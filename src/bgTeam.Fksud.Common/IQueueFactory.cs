﻿namespace bgTeam.Fksud.Common
{
    public interface IQueueFactory
    {
        QueueChannel<T> Create<T>(string uid);
    }
}
