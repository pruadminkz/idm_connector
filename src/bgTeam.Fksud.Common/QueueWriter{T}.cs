﻿namespace bgTeam.Fksud.Common
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public abstract class QueueWriter<T> : IDisposable
    {
        public abstract Task Produce(T value, CancellationToken cancellationToken = default);

        /// <inheritdoc/>
        public abstract void Dispose();
    }
}
