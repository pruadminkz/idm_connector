﻿namespace bgTeam.Fksud.Common
{
    using System.Threading.Tasks;
    using bgTeam.Fksud.Soap.Dto;

    public static class OpenTextHelper
    {
        public static Task Unwrap(this Task<OTAuthentication> task)
        {
            return task;
        }

        public static Task<TResult> Unwrap<TResult>(this Task<SoapResult<TResult>> task)
        {
            return task.ContinueWith(x => x.Result.Result);
        }
    }
}
