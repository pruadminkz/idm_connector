﻿namespace bgTeam.Fksud.Common
{
    using System.Threading.Tasks;

    public interface IUserSynchronizeSettingLocaleTypeService
    {
        Task SynchronizeLocaleType();
    }
}
