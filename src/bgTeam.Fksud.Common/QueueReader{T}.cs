﻿namespace bgTeam.Fksud.Common
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public abstract class QueueReader<T> : IDisposable
    {
        public abstract Task Commit(CancellationToken cancellationToken = default);

        public abstract Task<T> Consume(CancellationToken cancellationToken = default);

        /// <inheritdoc/>
        public abstract void Dispose();
    }
}
