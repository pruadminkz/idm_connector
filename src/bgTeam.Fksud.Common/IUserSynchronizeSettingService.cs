﻿namespace bgTeam.Fksud.Common
{
    using System.Threading.Tasks;

    public interface IUserSynchronizeSettingService
    {
        Task Synchronize();
    }
}
