﻿namespace bgTeam.Fksud.Common
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using bgTeam.Fksud.Domain.Dto;

    /// <summary>
    /// Сервис взаимодействия с CS.
    /// </summary>
    public interface IOpenTextService
    {
        /// <summary>
        /// Метод добавление группы в состав другой группы.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Параметры операции.</param>
        /// <returns>Результат выполнения операции.</returns>
        Task<OperationResultDto> AddWg2Wg(string token, AddWgToWgDto @params);

        /// <summary>
        /// Метод добавления учетной записи в группу.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Параметры операции.</param>
        /// <returns>Результат выполнения операции.</returns>
        Task<OperationResultDto> AddAccount2Wg(string token, AddAccountToWgDto @params);

        /// <summary>
        /// Метод создания рабочей группы.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Параметры операции.</param>
        /// <returns>Результат выполнения операции.</returns>
        Task<OperationResultDto> CreateWg(string token, WorkgroupDto @params);

        /// <summary>
        /// Метод разблокировки учетной записи.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="personnelNumber">Табельный номер пользователя.</param>
        /// <returns>Результат выполнения операции.</returns>
        Task<OperationResultDto> UnlockUser(string token, string personnelNumber);

        /// <summary>
        /// Метод разблокировки учетной записи по логину.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Представление объекта Account из системы.</param>
        /// <returns>Результат выполнения операции.</returns>
        Task<OperationResultDto> UnlockUserByLogin(string token, AccountDto @params);

        /// <summary>
        /// Метод удаления учетной записи из группы.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Параметры операции.</param>
        /// <returns>Результат выполнения операции.</returns>
        Task<OperationResultDto> RemoveAccountFromWg(string token, RemoveAccountFromWgDto @params);

        /// <summary>
        /// Метод получения информации об учетной записи.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="userName">Логин учетной записи.</param>
        /// <returns>Представление объекта Account из системы со всеми атрибутами и рабочими группами(workgroup).</returns>
        Task<AccountDto> GetAccount(string token, string userName);

        /// <summary>
        /// Метод изменения атрибутов учетной записи и набора рабочих групп и прав.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Представление объекта Account из системы со всеми атрибутами и рабочими группами.</param>
        /// <returns>Результат выполнения операции.</returns>
        Task<OperationResultDto> UpdateAccount(string token, AccountDto @params);

        /// <summary>
        /// Метод получения списка всех учетных записей.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <returns>Список учетных записей.</returns>
        Task<LookupAccountsDto> LookupAccounts(string token);

        /// <summary>
        /// Метод получения списка рабочих групп.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <returns>Список рабочих групп.</returns>
        Task<IEnumerable<WorkgroupDto>> LookupWorkgroups(string token);

        /// <summary>
        /// Обновление токена авторизации.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <returns>Обновленный токен авторизации в API.</returns>
        Task<string> RefreshToken(string token);

        /// <summary>
        /// Аутентификация в системе.
        /// </summary>
        /// <param name="userName">Имя пользователя.</param>
        /// <param name="password">Пароль.</param>
        /// <returns>Токен авторизации в API.</returns>
        Task<string> AuthenticateUser(string userName, string password);

        /// <summary>
        /// Метод получения Аудита о учетной записи.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="dateFrom">Дата с в запросе аудита.</param>
        /// <param name="dateTo">Дата по в запросе аудита.</param>
        /// <returns>Список учетных записей.</returns>
        Task<AuditDto> LookupAudit(string token, string dateFrom, string dateTo);

        /// <summary>
        /// Метод получения иформации в промежутке времени.
        /// </summary>
        /// <param name="credentials">Данные авторизации в API.</param>
        /// <param name="params">Параметры операции.</param>
        /// <returns>Csv file.</returns>
        Task<bool> TimeLapseInfo(SendingCsvFile credentials, SendingCsvFileJobDto @params);
    }
}
