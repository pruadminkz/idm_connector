﻿namespace bgTeam.Fksud.Common.Exceptions
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [ExcludeFromCodeCoverage]
    public class MemberNotFoundException : Exception
    {
        public MemberNotFoundException(string name)
            : base($"Member {name} not found.")
        {
        }
    }
}
