﻿namespace bgTeam.Fksud.Common.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using bgTeam.DataAccess;
    using bgTeam.Fksud.Common.Utilities;
    using bgTeam.Fksud.Domain.Entities;

    public class WerksSynchronizeService : IWerksSynchronizeService
    {
        private readonly IAppLogger _logger;
        //private readonly IMegaService _megafonService;        
        private readonly ICrudService _crudService;
        private readonly IRepository _repository;
        private readonly IConnectionFactory _factory;

        public WerksSynchronizeService(
            IAppLogger logger,
            //IMegaService megafonService,            
            ICrudService crudService,
            IRepository repository,
            IConnectionFactory factory)
        {
            _logger = logger;
            //_megafonService = megafonService;            
            _crudService = crudService;
            _repository = repository;
            _factory = factory;
        }

        public async Task Synchronize(string token, WerksMgf[] @params)
        {
            await MarkOldWerks();

            await InsertWerks(@params);

            await CopyReionalOfficeMarking();

            await CleanupWerks();
        }


        private async Task MarkOldWerks()
        {
            await _crudService.ExecuteAsync(
            @"UPDATE MFOCO_WERKS mws SET mws.TO_DELETE = 1");
            _logger.Info($"mfoco_werks marked for cleanup");
        }

        private async Task InsertWerks(WerksMgf[] wrkMgf)
        {
            _logger.Info($"Writing new Werks records");

            foreach (var item in wrkMgf)
            {
                using (var connection = _factory.Create())
                using (var tr = connection.BeginTransaction())
                {
                    try
                    {
                        await _crudService.ExecuteAsync(
                            @"INSERT INTO MFOCO_WERKS (
                                FILIAL_ID,
                                WERKS,
                                FILIAL,
                                LOCATION,
                                OLD_LOCATION,
                                ORG_ID,
                                PARENT_ID,
                                TO_DELETE)
                            VALUES (
                                :Filial_ID,
                                :Werks_code,
                                :Filial,
                                :Location,
                                :oldLocation,
                                :Organization_ID,
                                :Parent_ID,
                                0)",
                            new
                            {
                                Filial_ID = item.Filial_ID,
                                Werks_code = item.Werks_code,
                                Filial = item.Filial,
                                Location = item.Location,
                                oldLocation = item.oldLocation,
                                Organization_ID = item.Organization_ID,
                                Parent_ID = item.Parent_ID,
                            },
                            connection,
                            tr);

                        tr.Commit();

                        _logger.Info($"add werks row - {item.Werks_code}");
                    }
                    catch (AggregateException exp)
                    {
                        foreach (var e in exp.InnerExceptions)
                        {
                            _logger.Error(e);
                        }

                        _logger.Error(exp.GetBaseException());
                    }
                    catch (Exception exp)
                    {
                        _logger.Error(exp);

                        tr.Rollback();
                    }
                }
            }
        }


        private async Task CopyReionalOfficeMarking()
        {
            await _crudService.ExecuteAsync(
                @"UPDATE (SELECT * FROM MFOCO_WERKS WHERE TO_DELETE = 0) t1
                   SET (IS_REGIONAL_OFFICE) = (SELECT t2.IS_REGIONAL_OFFICE
								                FROM (SELECT * FROM MFOCO_WERKS WHERE TO_DELETE = 1) t2
								                WHERE t1.WERKS = t2.WERKS AND t1.TO_DELETE <> t2.TO_DELETE)
                 WHERE EXISTS (
                    SELECT 1
                      FROM (SELECT * FROM MFOCO_WERKS WHERE TO_DELETE = 1) t2
                     WHERE t1.WERKS = t2.WERKS AND t1.TO_DELETE <> t2.TO_DELETE)");
            _logger.Info($"IS_REGIONAL_OFFICE markings copied to new rows");
        }

        private async Task CleanupWerks()
        {
            await _crudService.ExecuteAsync(
            @"DELETE FROM MFOCO_WERKS mws WHERE mws.TO_DELETE = 1");
            _logger.Info($"mfoco_werks cleaned up");
        }
    }
}