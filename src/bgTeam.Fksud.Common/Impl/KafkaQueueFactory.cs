﻿namespace bgTeam.Fksud.Common.Impl
{
    using bgTeam.Fksud.Domain.Dto;
    using Microsoft.Extensions.Options;

    public class KafkaQueueFactory : IQueueFactory
    {
        private readonly IOptionsFactory<KafkaQueueOptions> _options;

        public KafkaQueueFactory(IOptionsFactory<KafkaQueueOptions> options)
        {
            _options = options;
        }

        /// <inheritdoc/>
        public QueueChannel<T> Create<T>(string uid)
        {
            return new KafkaQueueChannel<T>(Options.Create(_options.Create(uid)));
        }
    }
}
