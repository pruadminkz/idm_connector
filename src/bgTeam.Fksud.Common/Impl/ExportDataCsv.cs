﻿namespace bgTeam.Fksud.Common.Impl
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    public static class ExportDataCsv
    {
        public static void ExportCsv<T>(List<T> genericList, string fileName)
        {
            var sb = new StringBuilder();

            string path = @"C:\TEMP\";

            var finalPath = Path.Combine(path, fileName);
            var info = typeof(T).GetProperties();

            // Названия столбцов. Если файл уже существует, стобцы не перезаписываются.
            if (!File.Exists(finalPath))
            {
                var file = File.Create(finalPath);
                file.Close();

                var header = string.Join(";", info.Select(prop => prop.Name));
                sb.AppendLine(header);
                TextWriter sw = new StreamWriter(finalPath, true);
                sw.Write(sb.ToString());
                sw.Close();
            }

            // Заполнение таблицы.
            foreach (var obj in genericList)
            {
                sb = new StringBuilder();
                var line = string.Join(";", info.Select(prop => prop.GetValue(obj, null)));
                sb.AppendLine(line);
                TextWriter sw = new StreamWriter(finalPath, true);
                sw.Write(sb.ToString());
                sw.Close();
            }
        }
    }
}
