﻿namespace bgTeam.Fksud.Common.Impl
{
    using System.Threading.Tasks;
    using bgTeam.DataAccess;
    using bgTeam.Fksud.Domain.Entities;
    using Microsoft.Extensions.Logging;
    using bgTeam.Fksud.Soap.Services;
    using bgTeam.Fksud.Domain.Dto;
    using Microsoft.Extensions.Options;
    using bgTeam.Fksud.Soap.Dto;

    /// <inheritdoc/>
    public class UserSynchronizeSettingLocaleTypeService : IUserSynchronizeSettingLocaleTypeService
    {
        public const string DEFAULT_VALUE_LOCATE = "A<1,?,'localeType'='_ru_RU'>";

        private readonly ILogger<UserSynchronizeSettingLocaleTypeService> _logger;
        private readonly IRepository _repository;
        private readonly ICrudService _crudService;
        private readonly IOTMemberService _memberService;
        private readonly IAuthenticationService _authentication;
        private readonly IOptions<OpenTextApiCredentials> _credentials;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserSynchronizeSettingLocaleTypeService"/> class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="repository"></param>
        /// <param name="crudService"></param>
        /// <param name="memberService"></param>
        /// <param name="authentication"></param>
        /// <param name="credentials"></param>
        public UserSynchronizeSettingLocaleTypeService(
            ILogger<UserSynchronizeSettingLocaleTypeService> logger,
            IRepository repository,
            ICrudService crudService,
            IOTMemberService memberService,
            IAuthenticationService authentication,
            IOptions<OpenTextApiCredentials> credentials)
        {
            _logger = logger;
            _repository = repository;
            _crudService = crudService;
            _memberService = memberService;
            _authentication = authentication;
            _credentials = credentials;
        }

        /// <inheritdoc/>
        public async Task SynchronizeLocaleType()
        {
            _logger.LogInformation("Starting UserSettings update job");

            var authToken = await _authentication
                .AuthenticateUserAsync(_credentials.Value.UserName, _credentials.Value.Password);
            _logger.LogInformation("Authorized1: " + authToken.ToString());

            // добовляем General, если нету
            var settNew = await _repository.GetAllAsync<UserSettingTemp>(
                @"SELECT K.ID, K.NAME, KP.PREFSVALUE AS VALUE FROM KUAF K
                  LEFT JOIN KUAFPREFS KP ON PREFSID = K.ID AND PREFSKEYWORD = 'General'
                  WHERE TYPE = 0 AND K.DELETED = 0 AND KP.PREFSID IS NULL");

            foreach (var item in settNew)
            {
                await _crudService.ExecuteAsync("INSERT INTO KUAFPREFS(PREFSID ,PREFSKEYWORD ,PREFSVALUE) VALUES (:Id, 'General', :Value)", new { item.Id, Value = DEFAULT_VALUE_LOCATE });
                _logger.LogInformation($"Insert new General for - {item.Name}, {item.Id}");
            }
        }

    }
}