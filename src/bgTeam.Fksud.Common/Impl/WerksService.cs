﻿namespace bgTeam.Fksud.Common.Impl
{
    using System.Linq;
    using System.Threading.Tasks;
    using bgTeam.Fksud.Domain.Dto;
    using bgTeam.Fksud.Domain.Entities;

    /// <inheritdoc/>
    public class WerksService : IWerksService
    {
        private readonly IWerksSynchronizeService _werksSynchronizeService;       

        public WerksService(
            IWerksSynchronizeService werksSynchronizeService
            )
        {
            _werksSynchronizeService = werksSynchronizeService;
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> SyncWerks(string token, WerksDto[] @params)
        {
            var werks = @params.Select(x =>
            {
                return new WerksMgf
                {
                    Filial_ID = x.Filial_ID,
                    Werks_code = x.Werks,
                    Filial = x.Filial,
                    Location = x.Location,
                    oldLocation = x.oldLocation,
                    Organization_ID = x.Organization_ID,
                    Parent_ID = x.Parent_ID,
                };
            }).ToArray();

            await _werksSynchronizeService.Synchronize(token, werks);

            return OperationResultDto.Success;
        }
    }
}
