﻿namespace bgTeam.Fksud.Common.Impl
{
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.Fksud.Domain.Dto;
    using bgTeam.Fksud.Domain.Entities;

    /// <inheritdoc/>
    public class DepartamentsService : IDepartamentsService
    {
        private readonly IDepartamentSynchronizeService _departamentSynchronizeService;
        private readonly IAppLogger _logger;

        public DepartamentsService(
            IAppLogger logger,
            IDepartamentSynchronizeService departamentSynchronizeService)
        {
            _logger = logger;
            _departamentSynchronizeService = departamentSynchronizeService;
        }

        private static string ConvertIntArrayToString(int[] array)
        {
            // Concatenate all the elements into a StringBuilder.
            StringBuilder builder = new StringBuilder();
            foreach (int value in array)
            {
                builder.Append(value);
                builder.Append(", ");
            }
            return builder.ToString();
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> SyncDepartaments(string token, DepartamentDto[] @params)
        {
            var rootIds = @params
                //.Where(x => !@params.Any(y => y.Id == x.ParentId))
                .Where(x => x.Id == x.ParentId)
                .Select(x => x.ParentId)
                .Distinct()
                .ToArray();
            _logger.Info("rootIds: " + ConvertIntArrayToString(rootIds));

            var diff = @params.Select(x =>
            {
                int rootId = FindRootId(x);

                return new DivisionsMgf
                {
                    SYSTEM_ID = x.Id,
                    PARENT_ID = x.ParentId,
                    DIVISION_NAME = x.Name.Replace("'", "\""),
                    ROOT_DIVISION_ID = rootId,
                    Organization = GetOrganization(rootId),
                    GROUP_NAME = CreateGroupName(x, rootId),
                };
            }).ToArray();

            await _departamentSynchronizeService.Synchronize(token, diff);

            return OperationResultDto.Success;

            int FindRootId(DepartamentDto department)
            {
                if (!rootIds.Contains(department.Id))
                {
                    var next = @params.FirstOrDefault(x => x.Id == department.ParentId);

                    return next == null || next.Id == department.Id ? department.ParentId : FindRootId(next);
                }
                else
                {
                    return -1;
               }
            }

            string CreateGroupName(DepartamentDto department, int rootDep)
            {
                string group_name = string.Empty;
                _logger.Info("Generating GroupName for Department: " + department.Name + " with ID: " + department.Id.ToString() + " with ParentID: " + department.ParentId.ToString());
                if (!rootIds.Contains(department.Id))
                {
                    var str = GetAllStructName(department);

                    group_name = $"dv_{str.Replace("'", "\"")}_{department.Id}";
                }
                else
                {
                    group_name = $"dv_{department.Name.Replace("'", "\"")}_{department.Id}";
                }

                if (group_name.Length > 255)
                {
                    _logger.Info($"Group name too long: " + group_name);
                    var str = GetRootDepartmentName(department) + "_" + GetSecondLevelDepartmentName(department) + "_..._" + department.Name;
                    group_name = $"dv_{str.Replace("'", "\"")}_{department.Id}";
                    _logger.Debug($"Group name too long, truncated to {group_name}");
                }

                return group_name;
            }

            string GetAllStructName(DepartamentDto department)
            {
                if (!rootIds.Contains(department.Id))
                {
                    var parent = @params.FirstOrDefault(x => x.Id == department.ParentId);
                    _logger.Info("Current ID: " + department.Id);
                    _logger.Info("Current ParentID: " + parent.Id);
                    if (parent.Id != department.Id)
                    {
                        var str = GetAllStructName(parent);

                        return $"{str}_{department.Name}";
                    }
                }

                return department.Name;
            }

            string GetRootDepartmentName(DepartamentDto department)
            {
                _logger.Info("Inside GetRootDepartmentName() for " + department.Id.ToString() + " " + department.Name);
                if (!rootIds.Contains(department.Id))
                {
                    _logger.Info("department.Id is not inside rootIds. Getting parent Dep...");
                    var parent = FindDepartmentById(department.ParentId);
                    _logger.Info("Parent Dep is: " + parent.Id + " " + parent.Name);
                    var str = GetRootDepartmentName(parent);

                    _logger.Info("End 1 of GetRootDepartmentName()");
                    return str;
                }

                _logger.Info("End 2 of GetRootDepartmentName()");
                return department.Name;
            }

            string GetSecondLevelDepartmentName(DepartamentDto department)
            {
                _logger.Info("Inside GetSecondLevelDepartmentName()");
                if (!rootIds.Contains(department.ParentId))
                {
                    var parent = FindDepartmentById(department.ParentId);

                    var str = GetSecondLevelDepartmentName(parent);

                    _logger.Info("End 1 of GetSecondLevelDepartmentName()");
                    return str;
                }

                _logger.Info("End 2 of GetSecondLevelDepartmentName()");
                return department.Name;
            }

            DepartamentDto FindDepartmentById(int id)
            {
                _logger.Info("Inside FindDepartmentById()");
                return @params.FirstOrDefault(x => x.Id == id);
            }
        }

        private int GetOrganization(int id)
        {
            if (id == 50175191)
            {
                return 2; // МегаФон Ритейл
            }

            return 1;
        }

    }

}
