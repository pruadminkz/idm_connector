﻿namespace bgTeam.Fksud.Common.Impl
{
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using bgTeam.Fksud.Domain.Dto;
    using Confluent.Kafka;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class KafkaQueueWriter<T> : QueueWriter<T>
    {
        private readonly ProducerConfig _config;
        private readonly Producer<Null, T> _producer;
        private readonly IOptions<KafkaQueueOptions> _options;

        public KafkaQueueWriter(IOptions<KafkaQueueOptions> options)
        {
            _options = options;
            _config = new ProducerConfig
            {
                GroupId = options.Value.GroupId,
                BootstrapServers = options.Value.Hosts,
            };
            _producer = new Producer<Null, T>(_config, valueSerializer: Serializer);
        }

        /// <inheritdoc/>
        public override void Dispose()
        {
            try
            {
                _producer.Dispose();
            }
            catch { }
        }

        /// <inheritdoc/>
        public override async Task Produce(T value, CancellationToken cancellationToken = default)
        {
            await _producer.ProduceAsync(
                _options.Value.Topic,
                new Message<Null, T> { Value = value },
                cancellationToken);
        }

        private byte[] Serializer(string topic, T data)
        {
            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data));
        }
    }
}
