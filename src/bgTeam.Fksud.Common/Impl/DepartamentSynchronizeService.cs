﻿namespace bgTeam.Fksud.Common.Impl
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using bgTeam.DataAccess;
    using bgTeam.Fksud.Common.Utilities;
    using bgTeam.Fksud.Domain.Entities;

    public class DepartamentSynchronizeService : IDepartamentSynchronizeService
    {
        private readonly IAppLogger _logger;
        //private readonly IMegaService _megafonService;
        private readonly IOTMemberService _memberService;
        private readonly ICrudService _crudService;
        private readonly IRepository _repository;

        public DepartamentSynchronizeService(
            IAppLogger logger,
            //IMegaService megafonService,
            IOTMemberService memberService,
            ICrudService crudService,
            IRepository repository)
        {
            _logger = logger;
            //_megafonService = megafonService;
            _memberService = memberService;
            _crudService = crudService;
            _repository = repository;
        }

        public async Task Synchronize(string token, DivisionsMgf[] @params)
        {
            await ClearDivisions();

            var divisions = await _repository.GetAllAsync<Divisions>(
@"SELECT md.* FROM MFOCO_DIVISIONS md
INNER JOIN KUAF k ON k.ID = md.GROUP_DIVISION_ID
WHERE md.IS_DELETED = 0 AND k.DELETED = 0");

            await DeleteGroup(token, @params, divisions);

            await DeleteErrorGroups(token);

            await AddGroup(token, @params, divisions);

            await UpdateDivisions(@params);

            await UpdateGroups(token, @params);

            await FindErrorGroups();
        }


        private async Task ClearDivisions()
        {
            var divisions = await _repository.GetAllAsync<Divisions>(
@"SELECT md.* FROM MFOCO_DIVISIONS md
INNER JOIN KUAF k ON k.ID = md.GROUP_DIVISION_ID
WHERE k.TYPE != 1");

            foreach (var item in divisions)
            {
                await _crudService.ExecuteAsync("DELETE MFOCO_DIVISIONS WHERE SYSTEM_ID = :SysId", new { SysId = item.SYSTEM_ID });
                _logger.Info($"mfoco divisions delete - {item.SYSTEM_ID}, {item.DIVISION_NAME}");
            }
        }

        private Task DeleteGroup(string token, DivisionsMgf[] depMgf, IEnumerable<Divisions> divisions)
        {
            var diffList = divisions.Except(depMgf, new DivisionComparer()).ToList();

            if (!diffList.Any())
            {
                return Task.CompletedTask;
            }

            //foreach (var item in diffList)
            //{
            //    var dep = _megafonService.GetDepartmentInfo(item.SYSTEM_ID, false);

            //    if (dep != null)
            //    {
            //        _logger.Info($"divisions delete, but in MegaService - {dep.ID}, {dep.DepartmentName}");
            //    }
            //}

            return _memberService.DeleteMembers(token, diffList);
        }

        private Task AddGroup(string token, DivisionsMgf[] depMgf, IEnumerable<Divisions> divisions)
        {
            var diffList = depMgf.Except(divisions, new DivisionComparer()).ToList();

            if (!diffList.Any())
            {
                return Task.CompletedTask;
            }

            return _memberService.CreateGroups(token, diffList);
        }

        private async Task UpdateDivisions(DivisionsMgf[] depMgf)
        {
            var list = await _repository.GetAllAsync<Divisions>("SELECT SYSTEM_ID, DIVISION_NAME FROM MFOCO_DIVISIONS");

            var diffList = list.Except(depMgf, new DivisionNameComparer()).ToList();

            if (!diffList.Any())
            {
                return;
            }

            _logger.Info($"Divisions need update - {diffList.Count}");

            foreach (var item in diffList)
            {
                ////var val = depMgf.Where(x => x.Id == item.Id).FirstOrDefault();
                _logger.Info($"divisions need update name - {item.SysId}, {item.DivisionName}");
            }
        }

        private async Task UpdateGroups(string token, DivisionsMgf[] depMgf)
        {
            var kuafList = await _repository.GetAllAsync<KuafDivisions>(
@"SELECT d.SYSTEM_ID AS SYSID, k.ID AS KuafId, k.NAME
FROM OTCS.MFOCO_DIVISIONS d
JOIN KUAF k ON k.ID = d.GROUP_DIVISION_ID
WHERE d.GROUP_DIVISION_ID IS NOT NULL
AND d.IS_DELETED = 0 AND k.DELETED = 0 AND k.TYPE = 1");

            var diffList = kuafList.Except(depMgf, new EntityNameComparer()).ToList();

            if (!diffList.Any())
            {
                return;
            }

            foreach (var item in diffList)
            {
                var dep = depMgf.SingleOrDefault(x => x.SYSTEM_ID == item.SysId);
                if (dep != null)
                {
                    _logger.Debug($"need update group name for group - {item.Name}, new name - {dep.Name}");
                    item.Name = dep.Name;
                }
            }

            await _memberService.UpdateGroups(token, diffList);
        }

        private async Task FindErrorGroups()
        {
            var list = (await _repository.GetAllAsync<Kuaf>(
@"SELECT k.ID, k.NAME
FROM OTCS.MFOCO_DIVISIONS d
RIGHT JOIN KUAF k ON k.ID = d.GROUP_DIVISION_ID
WHERE k.TYPE = 1
  AND k.DELETED = 0
  AND k.NAME NOT LIKE 'cr_%'
  AND k.NAME NOT IN('TestGroup',
                     'DefaultGroup',
                     'otdsadmins',
                     'eLink',
                     'otadmins',
                     'otwfadmins',
                     'otrepusers')
  AND d.GROUP_DIVISION_ID IS NULL"))
                .OrderBy(x => x.Name);

            if (!list.Any())
            {
                return;
            }

            foreach (var item in list)
            {
                _logger.Debug($"Kuaf group - {item.SysId}, {item.Name}");
            }
        }

        private async Task DeleteErrorGroups(string token)
        {
            var list = await _repository.GetAllAsync<Kuaf>(
@"SELECT k.ID AS SYSID, k.Name
FROM KUAF k
LEFT JOIN MFOCO_DIVISIONS d ON k.ID = d.group_division_id
WHERE d.group_division_id IS NULL
  AND k.Name LIKE 'dv_%'
  AND k.deleted != 1
  AND k.TYPE = 1");

            if (!list.Any())
            {
                return;
            }

            await _memberService.DeleteGroups(token, list);
        }
    }
}
