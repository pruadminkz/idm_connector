﻿namespace bgTeam.Fksud.Common.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using bgTeam.Fksud.Domain.Dto;
    using Confluent.Kafka;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class KafkaQueueReader<T> : QueueReader<T>
    {
        private readonly TaskCompletionSource<bool> _initialized;
        private readonly ConsumerConfig _config;
        private readonly Consumer<Null, T> _consumer;
        private readonly IOptions<KafkaQueueOptions> _options;

        private CancellationTokenSource _eofSource;

        public KafkaQueueReader(IOptions<KafkaQueueOptions> options)
        {
            _options = options;
            _config = new ConsumerConfig
            {
                GroupId = options.Value.GroupId,
                BootstrapServers = options.Value.Hosts,
                EnableAutoCommit = false,
                EnableAutoOffsetStore = true,
                AutoOffsetReset = AutoOffsetResetType.Earliest,
            };
            _consumer = new Consumer<Null, T>(_config, valueDeserializer: Deserializer);
            _initialized = new TaskCompletionSource<bool>();
            _eofSource = new CancellationTokenSource();
            _consumer.OnPartitionEOF += (_, e) =>
            {
                _eofSource.Cancel();
                _eofSource = new CancellationTokenSource();
            };
            _consumer.OnPartitionsAssigned += (_, e) => _initialized.TrySetResult(true);
            _consumer.Subscribe(options.Value.Topic);
        }

        /// <inheritdoc/>
        public override void Dispose()
        {
            try
            {
                _consumer.Close();
                _consumer.Dispose();
            }
            catch { }
        }

        /// <inheritdoc/>
        public override Task Commit(CancellationToken cancellationToken = default)
        {
            return Task.Factory.StartNew(Function);

            List<TopicPartitionOffset> Function()
            {
                try
                {
                    return _consumer.Commit(cancellationToken);
                }
                catch (OperationCanceledException)
                {
                    return default;
                }
                catch (KafkaException)
                {
                    return default;
                }
            }
        }

        /// <inheritdoc/>
        public override async Task<T> Consume(CancellationToken cancellationToken = default)
        {
            //try
            //{
            if (_initialized.Task.IsCompleted)
            {
                await _initialized.Task;
            }

            return await Task.Factory.StartNew(Function);

            T Function()
            {
                var combinedSource = CancellationTokenSource.CreateLinkedTokenSource(_eofSource.Token, cancellationToken);
                try
                {
                    var result = _consumer.Consume(combinedSource.Token);
                    return result.Value;
                }
                catch (OperationCanceledException)
                {
                    return default;
                }
                catch (KafkaException)
                {
                    return default;
                }
            }
            //}
            //catch (KafkaException)
            //{
            //    return default;
            //}
        }

        private T Deserializer(string topic, ReadOnlySpan<byte> data, bool isNul)
        {
            return isNul ? default : JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(data));
        }
    }
}
