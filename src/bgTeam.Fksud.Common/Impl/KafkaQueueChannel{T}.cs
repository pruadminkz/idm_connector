﻿namespace bgTeam.Fksud.Common.Impl
{
    using bgTeam.Fksud.Domain.Dto;
    using Microsoft.Extensions.Options;

    public class KafkaQueueChannel<T> : QueueChannel<T>
    {
        public KafkaQueueChannel(IOptions<KafkaQueueOptions> options)
        {
            Reader = new KafkaQueueReader<T>(options);
            Writer = new KafkaQueueWriter<T>(options);
        }
    }
}
