﻿namespace bgTeam.Fksud.Common.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Threading.Tasks;
    using bgTeam.DataAccess;
    using bgTeam.Fksud.Domain.Dto;
    using bgTeam.Fksud.Domain.Entities;
    using bgTeam.Fksud.Soap.Dto;
    using bgTeam.Fksud.Soap.Services;
    using Microsoft.Extensions.Options;

    /// <inheritdoc/>
    public class OTMemberService : IOTMemberService
    {
        private readonly IAppLogger _logger;
        private readonly IOptions<OpenTextApiCredentials> _credentials;
        private readonly IMemberService _memberService;
        private readonly ICrudService _crudService;
        private readonly IAuthenticationService _authentication;

        //private readonly IAppSetings _appSetings;
        private readonly IConnectionFactory _factory;

        private readonly long[] _standartUsers = new long[] { 1000, 1001, 1002, 1003, 1004, 1005 };

        /// <summary>
        /// Initializes a new instance of the <see cref="OTMemberService"/> class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="credentials"></param>
        /// <param name="memberService"></param>
        /// <param name="authentication"></param>
        public OTMemberService(
            IAppLogger logger,
            IOptions<OpenTextApiCredentials> credentials,
            IMemberService memberService,
            ICrudService crudService,
            IAuthenticationService authentication,
            IConnectionFactory factory)
        {
            _logger = logger;
            _credentials = credentials;
            _memberService = memberService;
            _crudService = crudService;
            _authentication = authentication;
            _factory = factory;
        }

        /// <inheritdoc/>
        public async Task<bool> SetUserPrivilegesLoginAndPublicAccess(string login, OTAuthentication authToken)
        {
            
            //var authToken = await _authentication
            //    .AuthenticateUserAsync(_credentials.Value.UserName, _credentials.Value.Password);            

            var user = await _memberService.GetUserByLoginNameAsync(authToken, login).Unwrap();

            if (user.Privileges.LoginEnabled && user.Privileges.PublicAccessEnabled)
            {
                return false;
            }
            else
            {
                user.Privileges.LoginEnabled = true;
                user.Privileges.PublicAccessEnabled = true;
                var member = await _memberService.UpdateMemberAsync(authToken, user).Unwrap();
                var updUser = member as User;
                return (updUser?.Privileges.LoginEnabled ?? false) && (updUser?.Privileges.PublicAccessEnabled ?? false);
            }
        }

        public async Task DeleteGroups(string token, IEnumerable<Kuaf> list)
        {
            _logger.Info($"Delete groups - {list.Count()}");

            foreach (var item in list)
            {
                await _memberService.DeleteMemberAsync(token, item.SysId.Value).Unwrap();

                _logger.Info($"delete group - {item.Name} ({item.SysId})");
            }
        }

        public async Task DeleteMembers(string token, IEnumerable<IDivisions> diffList)
        {
            _logger.Info($"Delete divisions - {diffList.Count()}");

            // TODO : на время убираем удаления, для снятия статистики
            foreach (var item in diffList.OfType<Divisions>())
            {
                using (var connection = _factory.Create())
                using (var tr = connection.BeginTransaction())
                {
                    try
                    {
                        await _crudService.ExecuteAsync(
                            "UPDATE MFOCO_DIVISIONS SET IS_DELETED = 1, GROUP_DIVISION_ID = -1 WHERE SYSTEM_ID = :SysId", new { SysId = item.SYSTEM_ID }, connection, tr);
                        await _memberService.DeleteMemberAsync(token, item.GROUP_DIVISION_ID).Unwrap();

                        tr.Commit();

                        _logger.Info($"delete division - {item.GROUP_DIVISION_ID}, mgf div - {item.SYSTEM_ID}, ({item.DIVISION_NAME})");
                    }
                    catch (Exception exp)
                    {
                        tr.Rollback();
                        _logger.Error(exp);
                    }
                }
            }
        }

        public async Task UpdateGroups(string token, IEnumerable<IEntityName> diffList)
        {
            _logger.Info($"Update divisions - {diffList.Count()}");

            Parallel.ForEach(diffList, new ParallelOptions { MaxDegreeOfParallelism = (Environment.ProcessorCount >> 1) + 1 }, diff =>
            {
                var item = diff as KuafDivisions;

                try
                {
                    var mem = _memberService.GetMemberByIdAsync(token, item.KuafId).Unwrap().Result;

                    if (item.Name.Length <= 255)
                    {
                        mem.Name = item.Name;

                        _memberService.UpdateMemberAsync(token, mem).Wait();
                    }

                    _logger.Info($"update division - {item.SysId}, mgf div - {item.Name}");
                }
                catch (FaultException exp)
                {
                    _logger.Error(exp.Message);

                    // удаляем группу с таким же наименованием, и проверяем что это не та же группа
                    var gr = _memberService.GetGroupByNameAsync(token, item.Name).Unwrap().Result;
                    if (gr != null && item.KuafId != gr.ID)
                    {
                        _memberService.DeleteMemberAsync(token, gr.ID).Wait();
                        _logger.Info($"delete group - {gr.ID}, {gr.Name}");
                    }
                }
                catch (Exception exp)
                {
                    _logger.Error(exp);
                }
            });
        }

        public async Task CreateGroups(string token, IEnumerable<IDivisions> diffList)
        {
            _logger.Info($"Add divisions - {diffList.Count()}");

            foreach (var item1 in diffList)
            {
                using (var connection = _factory.Create())
                using (var tr = connection.BeginTransaction())
                {
                    long groupId = 0;
                    var item = item1 as DivisionsMgf;

                    try
                    {
                        groupId = await _memberService.CreateGroupAsync(token, item.GROUP_NAME, null).Unwrap();
                    }
                    catch (AggregateException exp)
                    {
                        foreach (var e in exp.InnerExceptions)
                        {
                            _logger.Error(e);
                        }

                        _logger.Error(exp.GetBaseException());
                    }
                    catch (FaultException exp)
                    {
                        _logger.Error($"{exp.Message}, for -  {item.GROUP_NAME}");

                        var gr = await _memberService.GetGroupByNameAsync(token, item.GROUP_NAME).Unwrap();

                        if (gr == null)
                        {
                            tr.Rollback();

                            continue;
                        }

                        groupId = gr.ID;
                    }

                    try
                    {
                        await _crudService.ExecuteAsync("DELETE MFOCO_DIVISIONS WHERE SYSTEM_ID = :SysId", new { SysId = item.SYSTEM_ID }, connection, tr);

                        await _crudService.ExecuteAsync(
                            @"INSERT INTO MFOCO_DIVISIONS (
                                SYSTEM_ID,
                                PARENT_ID,
                                SAP_ID,
                                DIVISION_NAME,
                                ROOT_DIVISION_ID,
                                GROUP_DIVISION_ID,
                                IS_REGIONAL_OFFICE,
                                IS_DELETED)
                            VALUES (
                                :SysId,
                                :ParentId,
                                :SapId,
                                :Name,
                                :RootId,
                                :GroupId,
                                :Org,
                                0)",
                            new
                            {
                                SysId = item.SYSTEM_ID,
                                ParentId = item.PARENT_ID,
                                SapId = item.SAP_ID,
                                Name = item.DIVISION_NAME,
                                RootId = item.ROOT_DIVISION_ID,
                                GroupId = groupId,
                                Org = GetOrganization(item.Organization, item.SYSTEM_ID, item.PARENT_ID, item.ROOT_DIVISION_ID),
                            }, connection, tr);

                        tr.Commit();

                        _logger.Info($"add division - {groupId}, {item.GROUP_NAME}");
                    }
                    catch (AggregateException exp)
                    {
                        foreach (var e in exp.InnerExceptions)
                        {
                            _logger.Error(e);
                        }

                        _logger.Error(exp.GetBaseException());
                    }
                    catch (Exception exp)
                    {
                        _logger.Error(exp);

                        tr.Rollback();

                        // удалить группу если успели её создать
                        if (groupId > 0)
                        {
                            await _memberService.DeleteMemberAsync(token, groupId).Unwrap();
                        }
                    }
                }
            }
        }

        private int GetOrganization(int organization, int systemId, int parentId, int rootId)
        {
            if (systemId == rootId || parentId == rootId)
            {
                return organization;
            }

            return -1;
        }

        public async Task AddListOfMembersToGroup(string token, Dictionary<long, List<long>> divisionsAndUsers)
        {
            _logger.Info($"divisions updates - {divisionsAndUsers.Count}");

            Parallel.ForEach(divisionsAndUsers, new ParallelOptions { MaxDegreeOfParallelism = (Environment.ProcessorCount >> 1) + 1 }, div =>
            {
                foreach (var item in div.Value)
                {
                    try
                    {
                        if (_memberService.GetMemberByIdAsync(token, item).Unwrap().Result is User mem)
                        {
                            if (div.Key > 0)
                            {
                                mem.DepartmentGroupID = div.Key;
                                _ = _memberService.UpdateMemberAsync(token, mem).Unwrap().Result;
                                _logger.Info($"user {item} set group {div.Key}, fax - {mem.Fax}");
                            }
                            else
                            {
                                _logger.Debug($"Not can be defined for a user group for user - {mem.Name}, { mem.ID} fax ({mem.Fax})");
                            }
                        }
                    }
                    catch (FaultException exp)
                    {
                        _logger.Error($"users {item}, {div.Key}");
                        _logger.Error(exp.Message);
                    }
                }
            });
        }

        public async Task DeleteListUsers(string token, IEnumerable<UserTemp> diffList)
        {
            foreach (var user in diffList)
            {
                // пропускаем стандартных пользователей
                if (_standartUsers.Contains(user.CSId))
                {
                    continue;
                }

                await _memberService.DeleteMemberAsync(token, user.CSId).Unwrap();
                _logger.Info($"delete user {user.CSId}, {user.Name}");
            }
        }
    }
}
