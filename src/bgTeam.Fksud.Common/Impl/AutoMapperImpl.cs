﻿namespace bgTeam.Fksud.Common.Impl
{
    using System.Linq;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using bgTeam.Fksud.Domain.Dto;
    using bgTeam.Fksud.Soap.Dto;

    /// <inheritdoc/>
    public class AutoMapperImpl : IDataMapper
    {
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoMapperImpl"/> class.
        /// </summary>
        public AutoMapperImpl()
        {
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, AccountDto>()
                    .ForMember(x => x.UserName, x => x.MapFrom(z => z.Name))
                    .ForMember(x => x.NtLogin, x => x.MapFrom(z => z.Name))
                    .ForMember(x => x.IsActive, x => x.MapFrom(z => !z.Deleted))
                    .ForMember(x => x.PatronymicName, x => x.MapFrom(z => z.MiddleName))
                    .ForMember(x => x.PersonnelNumber, x => x.MapFrom(z => z.Fax))
                    .ForMember(x => x.UserID, x => x.MapFrom(z => z.ID))
                    .ForMember(x => x.ObjectSID, x => x.MapFrom(z => z.Phone))
                    //.ForMember(x => x.WERKS, x => x.MapFrom(z => z.OfficeLocation));
                    .ForMember(x => x.OfficeLocation, x => x.MapFrom(z => z.OfficeLocation));


                cfg.CreateMap<AccountDto, User>()
                    .ForMember(x => x.Name, x => x.MapFrom(z => z.UserName))
                    .ForMember(x => x.MiddleName, x => x.MapFrom(z => z.PatronymicName))
                    .ForMember(x => x.Deleted, x => x.MapFrom(z => !z.IsActive))
                    .ForMember(x => x.ID, x => x.MapFrom(z => z.UserID))
                    .ForMember(x => x.Phone, x => x.MapFrom(z => z.ObjectSID))
                    .ForMember(x => x.Fax, x => x.MapFrom(z => z.PersonnelNumber))
                    //.ForMember(x => x.OfficeLocation, x => x.MapFrom(z => z.WERKS));
                    .ForMember(x => x.OfficeLocation, x => x.MapFrom(z => z.OfficeLocation));

                cfg.CreateMap<Group, WorkgroupDto>();

            }).CreateMapper();
        }

        /// <inheritdoc/>
        public TDest Map<TSrc, TDest>(TSrc src)
        {
            return _mapper.Map<TSrc, TDest>(src);
        }

        /// <inheritdoc/>
        public TDest Map<TSrc, TDest>(TDest dest, TSrc src)
        {
            return _mapper.Map(src, dest);
        }

        /// <inheritdoc/>
        public IQueryable<TDest> Project<TSrc, TDest>(IQueryable<TSrc> src)
        {
            return src.ProjectTo<TDest>(_mapper.ConfigurationProvider);
        }
    }
}
