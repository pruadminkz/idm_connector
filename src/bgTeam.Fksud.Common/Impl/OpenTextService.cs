﻿namespace bgTeam.Fksud.Common.Impl
{
    using bgTeam.DataAccess;
    using bgTeam.Fksud.Common.Exceptions;
    using bgTeam.Fksud.Domain.Dto;
    using bgTeam.Fksud.Domain.Entities;
    using bgTeam.Fksud.Soap.Dto;
    using bgTeam.Fksud.Soap.Enums;
    using bgTeam.Fksud.Soap.Services;
    using bgTeam.Fksud.Soap.Services.Impl;
    using Microsoft.Extensions.Options;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    /// <inheritdoc/>
    public class OpenTextService : IOpenTextService
    {
        private readonly IAppLogger _logger;
        private readonly IDataMapper _mapper;
        private readonly IRepository _repository;
        private readonly ICrudService _crudService;
        private readonly IMemberService _memberService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IOptions<OpenTextServiceOptions> _options;

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenTextService"/> class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="mapper">Маппер.</param>
        /// <param name="repository">Репозитарий.</param>
        /// <param name="crudService">CRUD.</param>
        /// <param name="memberService">Сервис управления пользователями и группами системы.</param>
        /// <param name="authenticationService">Сервис авторизации.</param>
        /// <param name="options">Настройки</param>
        public OpenTextService(
            IAppLogger logger,
            IDataMapper mapper,
            IRepository repository,
            ICrudService crudService,
            IMemberService memberService,
            IAuthenticationService authenticationService,
            IOptions<OpenTextServiceOptions> options)
        {
            _logger = logger;
            _mapper = mapper;
            _repository = repository;
            _crudService = crudService;
            _authenticationService = authenticationService;
            _options = options;
            _memberService = memberService;
        }

        private string RemoveSymbols(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                text = System.Text.RegularExpressions.Regex.Replace(text, @"[\n\t\r;]", " ");
                text = System.Text.RegularExpressions.Regex.Replace(text, @" +", " ");
                return text;
            }

            return string.Empty;
        }

        /// <inheritdoc/>
        public async Task<AuditDto> LookupAudit(string token, string dateFrom, string dateTo)
        {
            _logger.Info("Begin of LookupAudit.");
            var result = await RefreshToken(token);
            if (dateTo == null)
            {
                dateTo = DateTime.Now.ToString();
            }

            var eventsAuditsSql = await _repository.GetAllAsync<EventsAudit>(
                                @"select USER.NAME as Object, AUDITDATE as Datetime, PERF.NAME as Subject, AUDITSTR as Action
                                    , CASE
	                                    WHEN VAL1.NAME is not NULL THEN VAL1.NAME ELSE VALUE1
                                    END as OldValue
                                    , CASE
	                                    WHEN VAL2.NAME is not NULL THEN VAL2.NAME 	ELSE VALUE2
                                    END as NewValue
                                    from DAUDITNEWCORE A
                                    left join KUAF USER ON USER.ID = A.USERID
                                    left join KUAF PERF ON PERF.ID = A.PERFORMERID
									left join KUAF VAL1 ON CAST(VAL1.ID AS NVARCHAR) = CAST (A.VALUE1 AS NVARCHAR)
                                    left join KUAF VAL2 ON CAST(VAL2.ID AS NVARCHAR) = CAST (A.VALUE2 AS NVARCHAR)
                                    where A.USERID IN (SELECT ID FROM KUAF WHERE TYPE = 0) 
                                    AND AUDITDATE >= TO_TIMESTAMP (:dateFrom)
                                    AND AUDITDATE < TO_TIMESTAMP (:dateTo)", new { dateFrom, dateTo });

            string lastEvent = (await _repository.GetAsync<DateTime>("SELECT AUDITDATE as Datetime FROM DAUDITNEWCORE WHERE USERID IN (SELECT ID FROM KUAF WHERE TYPE = 0) AND AUDITSTR NOT IN ('Login', 'Logout') ORDER BY Datetime DESC LIMIT 1 ")).ToString("G");
            var audit = new AuditDto
            {
                Host = System.Net.Dns.GetHostName(),
                System = "OpenText",
                LastEvent = lastEvent,
                Events = (List<EventsAudit>)eventsAuditsSql,
            };

            return audit;
        }

        /// <inheritdoc/>
        public async Task<bool> TimeLapseInfo(SendingCsvFile credentials, SendingCsvFileJobDto @params)
        {
            _logger.Info("Begin of TimeLapseInfoFI.");

            DateTime dateTo = DateTime.Now;
            DateTime dateFrom = dateTo.AddDays(-7);

            var sql = @"SELECT * FROM ZT_SLA 
                                          WHERE WorkAudit_Date >= TO_TIMESTAMP('" + dateFrom +
                                          "') AND WorkAudit_Date < TO_TIMESTAMP('" + dateTo + "') " +
                                          "ORDER BY WorkAudit_Date";

            if (!string.IsNullOrEmpty(credentials.CustomSqlString))
            {
                sql = credentials.CustomSqlString;
            }

            _logger.Info($"Sending SQL: {sql}");

            var timeLapseInfoSql = await _repository.GetAllAsync<LapseInfo>(sql);

            var infoDto = new TimeLapseInfoDto
            {
                LapseInfo = timeLapseInfoSql.ToList(),
            };

            for (var i = 0; i < infoDto.LapseInfo.Count; i++)
            {
                infoDto.LapseInfo[i].Comment = RemoveSymbols(infoDto.LapseInfo[i].Comment);
                infoDto.LapseInfo[i].DocumentPackageType = RemoveSymbols(infoDto.LapseInfo[i].DocumentPackageType);
                infoDto.LapseInfo[i].ReturnReason = RemoveSymbols(infoDto.LapseInfo[i].ReturnReason);
                infoDto.LapseInfo[i].ProcessStep = RemoveSymbols(infoDto.LapseInfo[i].ProcessStep);
            }

            var fileName = @params.KPIBusinessEntityReportFile.FileName;

            _logger.Info("Exporting csv");

            ExportDataCsv.ExportCsv(infoDto.LapseInfo, fileName);

            var result = new HttpCommunicator(_logger);

            _logger.Info("Sending data to SAP");

            return result.MessageHandler(credentials, @params);
        }

        /// <inheritdoc/>
        public Task<string> RefreshToken(string token)
        {
            return _authenticationService.RefreshTokenAsync(token).Unwrap();
        }

        /// <inheritdoc/>
        public Task<string> AuthenticateUser(string userName, string password)
        {
            _logger.Info("Begin of AuthenticateUser.");
            _logger.Info("Processing user login: " + userName + ", pass: " + password);
            return _authenticationService.AuthenticateUserAsync(userName, password);
        }

        /// <inheritdoc/>
        public async Task<AccountDto> GetAccount(string token, string userName)
        {
            _logger.Info("Begin of GetAccount.");
            _logger.Info("Processing userName: " + userName);
            var result = await _memberService.GetUserByLoginNameAsync(token, userName).Unwrap();

            if (result == null)
            {
                throw new MemberNotFoundException(userName);
            }

            var account = _mapper.Map<User, AccountDto>(result);
            //var wg = await _memberService.ListMemberOfAsync(token, result.ID);
            account.Workgroups = (await _memberService.ListMemberOfAsync(token, result.ID).Unwrap())?.Select(x => x.Name)?.ToList();
            account.WorkgroupsID = (await _memberService.ListMemberOfAsync(token, result.ID).Unwrap())?.Select(x => x.ID)?.ToList();

            account.DepartmentGroupID = result.DepartmentGroupID;
            account.DepartmentGroup = (await _memberService.GetMemberByIdAsync(token, result.DepartmentGroupID).Unwrap())?.Name;
            _logger.Info("End of GetAccount.");
            return account;
        }

        /// <inheritdoc/>
        public async Task<LookupAccountsDto> LookupAccounts(string token)
        {
            _logger.Info("Begin of LookupAccounts");
            var result = await Lookup<User>(token);
            //_logger.Info("Количество пользователей result " + result.Count().ToString());

            var departmentGroupIds = result.Where(x => x.DepartmentGroupID > 0)
                .Select(x => x.DepartmentGroupID)
                .Distinct()
                .ToArray();

            var departmentGroups = departmentGroupIds.Length == 0
                ? Array.Empty<Member>()
                : await _memberService.GetMembersByIDAsync(token, departmentGroupIds).Unwrap();

            var partitionCount = _options.Value.DegreeOfParallelism <= 0
                    ? Environment.ProcessorCount
                    : _options.Value.DegreeOfParallelism;

            var accounts = (await Task.WhenAll(Partitioner.Create(result).GetPartitions(partitionCount).Select(async x =>
            {
                var list = new List<AccountDto>();

                using (x)
                {
                    while (x.MoveNext())
                    {
                        var item = x.Current;
                        var account = _mapper.Map<User, AccountDto>(item);

                        account.Workgroups = (await _memberService.ListMemberOfAsync(token, item.ID).Unwrap())?.Select(z => z.Name)?.ToList();
                        account.WorkgroupsID = (await _memberService.ListMemberOfAsync(token, item.ID).Unwrap())?.Select(x => x.ID)?.ToList();

                        account.DepartmentGroup = departmentGroups.FirstOrDefault(z => z.ID == item.DepartmentGroupID)?.Name;
                        account.DepartmentGroupID = item.DepartmentGroupID;

                        list.Add(account);
                    }
                }

                _logger.Info("Count of Accounts: " + list.Count.ToString());
                return list;
            }))).SelectMany(x => x).ToArray();
            _logger.Info("Begin of LookupAccounts.");
            return new LookupAccountsDto
            {
                Accounts = accounts,
            };
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<WorkgroupDto>> LookupWorkgroups(string token)
        {
            _logger.Info("Begin of LookupWorkgroups");
            var result = await Lookup<Group>(token);

            var leaderIds = result.Where(x => x.LeaderID > 0)
                .Select(x => x.LeaderID.Value)
                .Distinct()
                .ToArray();

            var leaders = leaderIds.Length == 0
                ? Array.Empty<Member>()
                : await _memberService.GetMembersByIDAsync(token, leaderIds).Unwrap();

            var workgroups = new List<WorkgroupDto>();
            long groupID;
            foreach (var item in result)
            {
                var workgroup = _mapper.Map<Group, WorkgroupDto>(item);

                workgroup.WorkGroupLeader = leaders?.FirstOrDefault(x => x.ID == item.LeaderID)?.Name;
                workgroup.WorkGroupLeaderID = leaders?.FirstOrDefault(x => x.ID == item.LeaderID)?.ID.ToString();
                groupID = item.ID;
                workgroup.GroupID = groupID;
                workgroup.ParentGroupID = (await _repository.GetAllAsync<long>("SELECT KC.ID FROM KUAFCHILDREN KC LEFT JOIN KUAF K ON K.ID = KC.ID WHERE KC.CHILDID = :groupID AND K.TYPE = 1", new { groupID }))?.ToList();
                workgroups.Add(workgroup);
            }

            _logger.Info("Count of Workgroups: " + workgroups.Count.ToString());
            _logger.Info("End of LookupWorkgroups.");
            return workgroups;
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> UnlockUser(string token, string personnelNumber)
        {
            _logger.Info("Begin of UnlockUser");
            _logger.Info("Processing personnelNumber: " + personnelNumber);
            var userId = await _repository.GetAsync<int?>(
                "SELECT ID FROM (SELECT TOP 1 ID FROM KUAF WHERE LOWER(FAX) = LOWER(:personnelNumber) AND DELETED = 1 ORDER BY ID DESC ) ",
                new { personnelNumber });

            if (userId == null)
            {
                throw new InvalidOperationException($"User with personnel number {personnelNumber} not found.");
            }

            await _crudService.ExecuteAsync(
                @"UPDATE KUAF SET NAME = CASE 
                                            WHEN SUBSTRING(NAME, 0, LOCATE(NAME, ' (Удалить) ') - 1) !=''
                                                THEN SUBSTRING(NAME, 0, LOCATE(NAME, ' (Удалить) ') - 1)
                                            WHEN SUBSTRING(NAME, 0, LOCATE(NAME, ' (Delete) ') - 1) !=''
                                                THEN SUBSTRING(NAME, 0, LOCATE(NAME, ' (Delete) ') - 1)
                                            ELSE NAME
                                         END, 
                                    USERPRIVILEGES = 2063, DELETED = 0 WHERE ID = :userId",
                new { userId });
            _logger.Info("End of UnlockUser" + OperationResultDto.Success.Result.ToString() + " errMsg: " + OperationResultDto.Success.Error.ToString());
            return OperationResultDto.Success;
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> UnlockUserByLogin(string token, AccountDto @params)
        {
            _logger.Info("Begin of UnlockUserByLogin.");
            //try { _logger.Info("Processing NtLogin: " + @params.NtLogin); } catch { _logger.Info("NtLogin not exists"); }
            //try { _logger.Info("Processing UserName: " + @params.UserName); } catch { _logger.Info("UserName not exists"); }
            try { _logger.Info("Processing UserID: " + @params.UserID); } catch { _logger.Info("UserID not exists"); }

            var userID = @params.UserID;
            /*
            var userName = @params.UserName;
            userName = string.Concat("%", userName.ToLower(), "%");
            var userId = await _repository.GetAsync<int?>(
                "SELECT ID FROM (SELECT ID FROM KUAF WHERE LOWER(NAME) like :userName AND DELETED = 1 ORDER BY ID DESC )",
                new { userName });*/

            var userId = await _repository.GetAsync<int?>(
                "SELECT ID FROM KUAF WHERE ID = :userID ORDER BY ID DESC ",
                new { userID });

            if (userId == null)
            {
                throw new InvalidOperationException($"User with UserID {userID} not found.");
            }

            userId = await _repository.GetAsync<int?>(
                "SELECT ID FROM KUAF WHERE ID = :userID AND DELETED = 1 ORDER BY ID DESC ",
                new { userID });

            if (userId == null)
            {
                throw new InvalidOperationException($"User with UserID {userID} not found among blocked users.");
            }

            await _crudService.ExecuteAsync(
                @"UPDATE KUAF SET NAME = CASE 
                                            WHEN SUBSTRING(NAME, 0, LOCATE(NAME, ' (Удалить) ') - 1) !=''
                                                THEN SUBSTRING(NAME, 0, LOCATE(NAME, ' (Удалить) ') - 1)
                                            WHEN SUBSTRING(NAME, 0, LOCATE(NAME, ' (Delete) ') - 1) !=''
                                                THEN SUBSTRING(NAME, 0, LOCATE(NAME, ' (Delete) ') - 1)
                                            ELSE NAME
                                         END, 
                                    USERPRIVILEGES = 2063, DELETED = 0 WHERE ID = :userId",
                new { userId });
            _logger.Info("End of UnlockUserByLogin" + OperationResultDto.Success.Result.ToString() + " errMsg: " + OperationResultDto.Success.Error.ToString());
            return OperationResultDto.Success;
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> UpdateAccount(string token, AccountDto @params)
        {
            _logger.Warning("Begin of UpdateAccount.");
            try { _logger.Warning("Processing NtLogin: " + @params.NtLogin); } catch { _logger.Warning("NtLogin not exists"); }
            try { _logger.Warning("Processing UserName: " + @params.UserName); } catch { _logger.Warning("UserName not exists"); }
            try { _logger.Warning("Processing UserID: " + @params.UserID); } catch { _logger.Warning("UserID not exists"); }
            //var user = await _memberService.GetUserByLoginNameAsync(token, @params.UserName).Unwrap();
            long userID = @params.UserID;
            string userName = await _repository.GetAsync<string>("SELECT NAME FROM KUAF WHERE ID = :userID AND DELETED = 0", new { userID });
            if (userName == @params.UserName)
            {
                var user = await _memberService.GetUserByLoginNameAsync(token, @params.UserName).Unwrap();

                if (user == null)
                {
                    _logger.Warning("Error. The login: " + @params.UserName + " not found in CS.");
                    throw new MemberNotFoundException(@params.UserName);
                }
                else if ((string.IsNullOrEmpty(_options.Value.NoSyncValue) && string.Equals(user.Phone, "do not sync", StringComparison.OrdinalIgnoreCase)) ||
                    (!string.IsNullOrEmpty(_options.Value.NoSyncValue) && string.Equals(user.Phone, _options.Value.NoSyncValue, StringComparison.OrdinalIgnoreCase)))
                {
                    _logger.Warning($"Skip {nameof(UpdateAccount)} for '{@params.UserName}', {nameof(user.Phone)} is {user.Phone}.");

                    return OperationResultDto.Success;
                }

                user = _mapper.Map(user, @params);

                var hasDepartmentGroup = !string.IsNullOrEmpty(@params.DepartmentGroup) && @params.DepartmentGroup.Length > 0;

                var departmentGroup = hasDepartmentGroup
                    ? await _memberService.GetGroupByNameAsync(token, @params.DepartmentGroup).Unwrap()
                    : null;

                if (hasDepartmentGroup && departmentGroup == null)
                {
                    throw new MemberNotFoundException(@params.DepartmentGroup);
                }

                user.DepartmentGroupID = departmentGroup?.ID ?? user.DepartmentGroupID;

                await _memberService.UpdateMemberAsync(token, user).Unwrap();
                /*
                var groups = await _memberService.ListMemberOfAsync(token, user.ID).Unwrap();

                // если у нас восстановленный после удаления пользователь, то нужно принудительно выдать ему группу подразделения.
                if (groups == null || !groups.Any())
                {
                    _logger.Info("UpdateAccount. We have user w/o any group. Adding him deaprtment group first.");
                    await _memberService.AddMemberToGroupAsync(token, departmentGroup.ID, user.ID).Unwrap();
                    groups = await _memberService.ListMemberOfAsync(token, user.ID).Unwrap();
                }

                //var groupsNames = groups.Select(z => z.Name).ToArray();
                var groupsID = groups.Select(z => z.ID).ToArray();

                var workgroups = @params.WorkgroupsID ?? new List<long>();

                departmentGroup = groups.First(x => x.ID == user.DepartmentGroupID);

                if (!workgroups.Contains(departmentGroup.ID))
                {
                    workgroups.Add(departmentGroup.ID);
                }

                //var aGroups = workgroups.Where(x => !groupsNames.Contains(x)).ToArray();

                var aGroups = workgroups.Where(x => !groupsID.Contains(x)).ToArray();
                var rGroups = groups.Where(x => !workgroups.Contains(x.ID)).Select(x => x.ID).ToArray();
                var aGroupsIds = new List<long>();

                foreach (var item in aGroups)
                {
                    //var group = await _memberService.GetGroupByNameAsync(token, item).Unwrap();

                    if (item.ToString() == null)
                    {
                        throw new MemberNotFoundException(item.ToString());
                    }

                    aGroupsIds.Add(item);
                }

                foreach (var item in rGroups)
                {
                    await _memberService.RemoveMemberFromGroupAsync(token, item, user.ID).Unwrap();
                }

                foreach (var item in aGroupsIds)
                {
                    await _memberService.AddMemberToGroupAsync(token, item, user.ID).Unwrap();
                }
                */
                _logger.Info("End of UpdateAccount." + " result: " + OperationResultDto.Success.Result.ToString() + " errMsg: " + OperationResultDto.Success.Error.ToString());
                return OperationResultDto.Success;
            }

            _logger.Info("End of UpdateAccount." + " result: " + OperationResultDto.BadRequestUpdateAccount.Result.ToString() + " errMsg: " + OperationResultDto.BadRequestUpdateAccount.Error.ToString());

            return OperationResultDto.BadRequestUpdateAccount;
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> AddWg2Wg(string token, AddWgToWgDto @params)
        {
            _logger.Warning("Begin of AddWg2Wg.");
            try { _logger.Warning("Processing TargetWorkgroupID: " + @params.TargetWorkgroup.GroupID); } catch { _logger.Warning("TargetWorkgroup.GroupID not exists"); }
            try { _logger.Warning("Processing SourceWorkgroupID: " + @params.SourceWorkgroup.GroupID); } catch { _logger.Warning("SourceWorkgroup.GroupID not exists"); }
            /*
            var source = await _memberService.GetGroupByNameAsync(token, @params.SourceWorkgroup.Name).Unwrap();

            if (source == null)
            {
                throw new MemberNotFoundException(@params.SourceWorkgroup.Name);
            }

            var target = await _memberService.GetGroupByNameAsync(token, @params.TargetWorkgroup.Name).Unwrap();

            if (target == null)
            {
                throw new MemberNotFoundException(@params.TargetWorkgroup.Name);
            }

            await _memberService.AddMemberToGroupAsync(token, target.ID, source.ID).Unwrap();
            */
            await _memberService.AddMemberToGroupAsync(token, @params.TargetWorkgroup.GroupID, @params.SourceWorkgroup.GroupID).Unwrap();
            _logger.Info("End of AddWg2Wg." + " result: " + OperationResultDto.Success.Result.ToString() + " errMsg: " + OperationResultDto.Success.Error.ToString());
            return OperationResultDto.Success;
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> AddAccount2Wg(string token, AddAccountToWgDto @params)
        {
            _logger.Warning("Begin of AddAccount2Wg.");
            try { _logger.Warning("Processing TargetWorkgroupID: " + @params.TargetWorkgroup.GroupID); } catch { _logger.Warning("TargetWorkgroup.GroupID not exists"); }
            try { _logger.Warning("Processing UserID: " + @params.Account.UserID); } catch { _logger.Warning("Account.UserID not exists"); }
            /*var account = await _memberService.GetUserByLoginNameAsync(token, @params.Account.UserName).Unwrap();
            if (account == null)
            {
                throw new MemberNotFoundException(@params.Account.UserName);
            }

            var target = await _memberService.GetGroupByNameAsync(token, @params.TargetWorkgroup.Name).Unwrap();

            if (target == null)
            {
                throw new MemberNotFoundException(@params.TargetWorkgroup.Name);
            }

            await _memberService.AddMemberToGroupAsync(token, target.ID, account.ID).Unwrap();*/

            await _memberService.AddMemberToGroupAsync(token, @params.TargetWorkgroup.GroupID, @params.Account.UserID).Unwrap();
            _logger.Info("End of AddAccount2Wg." + " result: " + OperationResultDto.Success.Result.ToString() + " errMsg: " + OperationResultDto.Success.Error.ToString());
            return OperationResultDto.Success;
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> CreateWg(string token, WorkgroupDto @params)
        {
            _logger.Info("Begin of CreateWg.");
            _logger.Info("Processing Name: " + @params.Name);
            var group = await _memberService.GetGroupByNameAsync(token, @params.Name.Replace("'", "\"")).Unwrap();

            if (group != null)
            {
                throw new InvalidOperationException($"Group {@params.Name} already exists.");
            }

            var leader = default(User);

            var leaderName = @params.WorkGroupLeader;

            if (!string.IsNullOrWhiteSpace(leaderName) && leaderName.Length > 0)
            {
                leader = await _memberService.GetUserByLoginNameAsync(token, leaderName).Unwrap();

                if (leader == null)
                {
                    throw new MemberNotFoundException(leaderName);
                }
            }

            await _memberService.CreateGroupAsync(token, @params.Name.Replace("'", "\""), leader?.ID).Unwrap();
            _logger.Info("End of CreateWg." + " result: " + OperationResultDto.Success.Result.ToString() + " errMsg: " + OperationResultDto.Success.Error.ToString());
            return OperationResultDto.Success;
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> RemoveAccountFromWg(string token, RemoveAccountFromWgDto @params)
        {
            _logger.Info("Begin of RemoveAccountFromWg.");
            try { _logger.Warning("Processing TargetWorkgroupID: " + @params.TargetWorkgroup.GroupID); } catch { _logger.Warning("TargetWorkgroup.GroupID not exists"); }
            try { _logger.Warning("Processing UserID: " + @params.Account.UserID); } catch { _logger.Warning("Account.UserID not exists"); }
            /*
            var account = await _memberService.GetUserByLoginNameAsync(token, @params.Account.UserName).Unwrap();

            if (account == null)
            {
                throw new MemberNotFoundException(@params.Account.UserName);
            }

            var target = await _memberService.GetGroupByNameAsync(token, @params.TargetWorkgroup.Name).Unwrap();

            if (target == null)
            {
                throw new MemberNotFoundException(@params.TargetWorkgroup.Name);
            }

            //if (target.LeaderID == account.ID)
            //{
            //    target.LeaderID = null;
            //    await _memberService.UpdateMemberAsync(token, target).Unwrap();
            //}

            await _memberService.RemoveMemberFromGroupAsync(token, target.ID, account.ID).Unwrap();*/

            await _memberService.RemoveMemberFromGroupAsync(token, @params.TargetWorkgroup.GroupID, @params.Account.UserID).Unwrap();
            _logger.Info("End of RemoveAccountFromWg." + OperationResultDto.Success.Result.ToString() + " errMsg: " + OperationResultDto.Success.Error.ToString());
            return OperationResultDto.Success;
        }

        private async Task<IEnumerable<TResult>> Lookup<TResult>(string token)
            where TResult : Member
        {
            var filter = SearchFilter.ANY;

            if (typeof(TResult) == typeof(User))
            {
                filter = SearchFilter.USER;
            }
            else if (typeof(TResult) == typeof(Group))
            {
                filter = SearchFilter.GROUP;
            }
            else if (typeof(TResult) == typeof(Domain))
            {
                filter = SearchFilter.DOMAIN;
            }

            _logger.Info("Вызываем SearchForMembersAsync");
            var result = await _memberService.SearchForMembersAsync(token, new MemberSearchOptions
            {
                Filter = filter,
                Column = SearchColumn.NAME,
                Search = string.Empty,
                Matching = SearchMatching.STARTSWITH,
                Scope = SearchScope.SYSTEM,
                PageSize = _options.Value.PageSize,
            }).Unwrap();

            _logger.Info("Отработал SearchForMembersAsync");

            var list = new List<TResult>();

            while (!(result?.FinalPage ?? true))
            {
                var searchResults = await _memberService.GetSearchResultsAsync(token, result).Unwrap();

                list.AddRange(searchResults.Members.OfType<TResult>());

                result = searchResults.PageHandle;
            }

            return list;
        }
    }
}
