﻿namespace bgTeam.Fksud.Common.Impl
{
    using System.Threading.Tasks;
    using bgTeam.DataAccess;
    using bgTeam.Fksud.Domain.Entities;
    using Microsoft.Extensions.Logging;    
    using bgTeam.Fksud.Soap.Services;    
    using bgTeam.Fksud.Domain.Dto;    
    using Microsoft.Extensions.Options;
    using bgTeam.Fksud.Soap.Dto;

    /// <inheritdoc/>
    public class UserSynchronizeSettingService : IUserSynchronizeSettingService
    {
        public const string DEFAULT_VALUE = "A<1,?,'viewType?'=1,'viewTypecfcontract'=1,'viewTypecfrelated'=1,'viewTypecfsubagreements'=1,'viewTypecfsubcf'=1,'viewTypedocuments'=1,'viewTypesubbinder'=1,?=A<1,?,'showHeader'=true,'showResubmissions'=true,'showTasks'=true,'showWorkflows'=true>,31066=A<1,?,'showHeader'=true>,31353=A<1,?,'showHeader'=true,'showResubmissions'=true,'showTasks'=true,'showWorkflows'=true>>";

        private readonly ILogger<UserSynchronizeSettingService> _logger;
        private readonly IRepository _repository;
        private readonly ICrudService _crudService;
        private readonly IOTMemberService _memberService;
        private readonly IAuthenticationService _authentication;
        private readonly IOptions<OpenTextApiCredentials> _credentials;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserSynchronizeSettingService"/> class.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="repository"></param>
        /// <param name="crudService"></param>
        /// <param name="memberService"></param>
        /// <param name="authentication"></param>
        /// <param name="credentials"></param>
        public UserSynchronizeSettingService(
            ILogger<UserSynchronizeSettingService> logger,
            IRepository repository,
            ICrudService crudService,
            IOTMemberService memberService,
            IAuthenticationService authentication,
            IOptions<OpenTextApiCredentials> credentials)
        {
            _logger = logger;
            _repository = repository;
            _crudService = crudService;
            _memberService = memberService;
            _authentication = authentication;
            _credentials = credentials;
        }

        /// <inheritdoc/>
        public async Task Synchronize()
        {
            _logger.LogInformation("Starting UserSettings update job");

            var authToken = await _authentication
                .AuthenticateUserAsync(_credentials.Value.UserName, _credentials.Value.Password);
            //_logger.LogInformation("UserName: " + _credentials.Value.UserName + " Password: " + _credentials.Value.Password);
            _logger.LogInformation("Authorized1: " + authToken.ToString());

            var sett = await _repository.GetAllAsync<UserSettingTemp>(
                @"SELECT K.ID, K.NAME, K.USERPRIVILEGES, K.SETTINGSNUM, KP.PREFSVALUE AS VALUE FROM KUAF K  
                    JOIN KUAFPREFS KP ON PREFSID = K.ID AND PREFSKEYWORD = 'cmbase'                
                  WHERE K.TYPE = 0 AND K.DELETED = 0");

            foreach (var item in sett)
            {
                var str = UserChangeSetting(item.Value);

                if (!str.Equals(item.Value))
                {
                    _logger.LogInformation(item.Name);
                    _logger.LogInformation(item.Value);
                    _logger.LogInformation(str);

                    await _crudService.ExecuteAsync(
                         @"UPDATE KUAFPREFS
                        SET PREFSVALUE = :Value
                        WHERE PREFSKEYWORD = 'cmbase'
                          AND PREFSID = :Id",
                         new { item.Id, Value = str });
                }

                try
                {
                   await SetUserPrivileges(item.Name, authToken);
                }
                catch (System.Exception ex)
                {
                    _logger.LogError(ex, "Error update user settings");
                    if (ex.Message.Contains("Your session has timed-out"))
                    {
                        _logger.LogError(ex, "Session has timed-out");
                        authToken = await _authentication
                            .AuthenticateUserAsync(_credentials.Value.UserName, _credentials.Value.Password);
                        await SetUserPrivileges(item.Name, authToken);
                    }
                }
            }

            // добовляем cmbase, если нету
            var settNew = await _repository.GetAllAsync<UserSettingTemp>(
                @"SELECT K.ID, K.NAME, KP.PREFSVALUE AS VALUE FROM KUAF K
                  LEFT JOIN KUAFPREFS KP ON PREFSID = K.ID AND PREFSKEYWORD = 'cmbase'
                  WHERE TYPE = 0 AND K.DELETED = 0 AND KP.PREFSID IS NULL");

            foreach (var item in settNew)
            {
                await _crudService.ExecuteAsync("INSERT INTO KUAFPREFS(PREFSID ,PREFSKEYWORD ,PREFSVALUE) VALUES (:Id, 'cmbase', :Value)", new { item.Id, Value = DEFAULT_VALUE });
                _logger.LogInformation($"Insert new cmbase for - {item.Name}, {item.Id}");

                await SetUserPrivileges(item.Name, authToken);
            }
        }

        private string UserChangeSetting(string value)
        {
            var mass = value.Split('A');

            mass[mass.Length - 2] = mass[mass.Length - 2]
                .Replace("'showHeader'=false", "'showHeader'=true");

            mass[mass.Length - 1] = mass[mass.Length - 1]
                .Replace("'showHeader'=false", "'showHeader'=true")
                .Replace("'showResubmissions'=false", "'showResubmissions'=true")
                .Replace("'showTasks'=false", "'showTasks'=true")
                .Replace("'showWorkflows'=false", "'showWorkflows'=true");

            return string.Join("A", mass);
        }

        private async Task SetUserPrivileges(string userName, string authToken)
        {
            _logger.LogInformation("SetUserPrivileges authToken: " + authToken.ToString());
            //try
            //{
                if (await _memberService.SetUserPrivilegesLoginAndPublicAccess(userName, authToken))
                {
                    _logger.LogInformation($"Update item {userName} privileges to loginEnable and publicAccessEnabled");
                }
            /*}
            catch (System.Exception ex)
            {
                _logger.LogError(ex, "Error update user settings");                
            }*/
        }
    }
}
