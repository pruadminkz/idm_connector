﻿namespace bgTeam.Fksud.Common.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using bgTeam.Fksud.Common.Exceptions;
    using bgTeam.Fksud.Domain.Dto;
    using bgTeam.Fksud.Soap.Services;
    using Microsoft.Extensions.Options;

    /// <summary>
    /// Decorator for <see cref="IOpenTextService"/>.
    /// </summary>
    public class OpenTextServiceDecorator : IOpenTextService
    {
        private readonly IAppLogger _logger;
        private readonly IOpenTextService _service;
        private readonly IQueueFactory _queueFactory;
        private readonly IMemberService _memberService;
        private readonly IOptions<OpenTextServiceDecoratorOptions> _options;

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenTextServiceDecorator"/> class.
        /// </summary>
        /// <param name="logger">Логгер.</param>
        /// <param name="service">Базовый сервис.</param>
        /// <param name="queueFactory">Фабрика очередей.</param>
        /// <param name="memberService">Сервис управления пользователями и группами системы.</param>
        /// <param name="options">Настройки.</param>
        public OpenTextServiceDecorator(
            IAppLogger logger,
            IOpenTextService service,
            IQueueFactory queueFactory,
            IMemberService memberService,
            IOptions<OpenTextServiceDecoratorOptions> options)
        {
            _logger = logger;
            _service = service;
            _queueFactory = queueFactory;
            _memberService = memberService;
            _options = options;
        }

        /// <inheritdoc/>
        public Task<AuditDto> LookupAudit(string token, string dateFrom, string dateTo)
        {
            return _service.LookupAudit(token, dateFrom, dateTo);
        }

        /// <inheritdoc/>
        public Task<bool> TimeLapseInfo(SendingCsvFile credentials, SendingCsvFileJobDto @params)
        {
            return _service.TimeLapseInfo(credentials, @params);
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> AddAccount2Wg(string token, AddAccountToWgDto @params)
        {
            try
            {
                return await _service.AddAccount2Wg(token, @params);
            }
            catch (MemberNotFoundException exp) when(_options.Value.UseQueue)
            {
                _logger.Error(exp);
                using (var queue = _queueFactory.Create<RetryMessage>(_options.Value.QueueSection))
                {
                    await queue.Writer.Produce(new RetryMessage
                    {
                        Method = nameof(AddAccount2Wg),
                        Params = @params,
                    });
                }
            }
            catch (Exception exp)
            {
                _logger.Warning(exp.Message.ToString());
                return new OperationResultDto
                {
                    Error = (exp is AggregateException aExp ? aExp.Flatten().GetBaseException() : exp).Message,
                    Result = false,
                };
            }

            return OperationResultDto.Success;
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> AddWg2Wg(string token, AddWgToWgDto @params)
        {
            try
            {
                return await _service.AddWg2Wg(token, @params);
            }
            catch (MemberNotFoundException exp) when (_options.Value.UseQueue)
            {
                _logger.Warning(exp.Message.ToString());
                using (var queue = _queueFactory.Create<RetryMessage>(_options.Value.QueueSection))
                {
                    await queue.Writer.Produce(new RetryMessage
                    {
                        Method = nameof(AddWg2Wg),
                        Params = @params,
                    });
                }
            }
            catch (Exception exp)
            {
                _logger.Warning(exp.Message.ToString());
                return new OperationResultDto
                {
                    Error = (exp is AggregateException aExp ? aExp.Flatten().GetBaseException() : exp).Message,
                    Result = false,
                };
            }

            return OperationResultDto.Success;
        }

        /// <inheritdoc/>
        public async Task<string> AuthenticateUser(string userName, string password)
        {
            var token = await _service.AuthenticateUser(userName, password);
            var user = await _memberService.GetUserByLoginNameAsync(token, userName).Unwrap();
            var workgroups = (await _memberService.ListMemberOfAsync(token, user.ID).Unwrap())
                .Select(x => x.Name).ToArray();

            if (!(_options.Value.RequredWorkgroups?.All(x => workgroups.Contains(x)) ?? true))
            {
                throw new InvalidOperationException($"Account '{user.Name}' don't have permissions to access connector.");
            }

            return token;
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> CreateWg(string token, WorkgroupDto @params)
        {
            try
            {
                return await _service.CreateWg(token, @params);
            }
            catch (MemberNotFoundException exp) when (_options.Value.UseQueue)
            {
                _logger.Info(exp.Message.ToString());
                using (var queue = _queueFactory.Create<RetryMessage>(_options.Value.QueueSection))
                {
                    await queue.Writer.Produce(new RetryMessage
                    {
                        Method = nameof(CreateWg),
                        Params = @params,
                    });
                }
            }
            catch (Exception exp)
            {
                _logger.Info(exp.Message.ToString());
                return new OperationResultDto
                {
                    Error = (exp is AggregateException aExp ? aExp.Flatten().GetBaseException() : exp).Message,
                    Result = false,
                };
            }

            return OperationResultDto.Success;
        }

        /// <inheritdoc/>
        public Task<AccountDto> GetAccount(string token, string userName)
        {
            return _service.GetAccount(token, userName);
        }

        /// <inheritdoc/>
        public Task<LookupAccountsDto> LookupAccounts(string token)
        {
            return _service.LookupAccounts(token);
        }

        /// <inheritdoc/>
        public Task<IEnumerable<WorkgroupDto>> LookupWorkgroups(string token)
        {
            return _service.LookupWorkgroups(token);
        }

        /// <inheritdoc/>
        public Task<string> RefreshToken(string token)
        {
            return _service.RefreshToken(token);
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> RemoveAccountFromWg(string token, RemoveAccountFromWgDto @params)
        {
            try
            {
                return await _service.RemoveAccountFromWg(token, @params);
            }
            catch (MemberNotFoundException exp) when (_options.Value.UseQueue)
            {
                _logger.Warning(exp.Message.ToString());
                using (var queue = _queueFactory.Create<RetryMessage>(_options.Value.QueueSection))
                {
                    await queue.Writer.Produce(new RetryMessage
                    {
                        Method = nameof(RemoveAccountFromWg),
                        Params = @params,
                    });
                }
            }
            catch (Exception exp)
            {
                _logger.Warning(exp.Message.ToString());
                return new OperationResultDto
                {
                    Error = (exp is AggregateException aExp ? aExp.Flatten().GetBaseException() : exp).Message,
                    Result = false,
                };
            }

            return OperationResultDto.Success;
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> UnlockUser(string token, string personnelNumber)
        {
            if (string.IsNullOrWhiteSpace(token) || token.Length == 0)
            {
                return OperationResultDto.Unauthorized;
            }

            try
            {
                var user = await _memberService.GetAuthenticatedUserAsync(token).Unwrap();

                if (user == null)
                {
                    return OperationResultDto.Unauthorized;
                }

                var workgroups = (await _memberService.ListMemberOfAsync(token, user.ID).Unwrap())
                    .Select(x => x.Name).ToArray();

                if (!(_options.Value.RequredWorkgroups?.All(x => workgroups.Contains(x)) ?? true))
                {
                    throw new InvalidOperationException($"Account '{user.Name}' don't have permissions to access connector.");
                }

                return await _service.UnlockUser(token, personnelNumber);
            }
            catch (Exception exp)
            {
                _logger.Error(exp);
                return new OperationResultDto
                {
                    Error = (exp is AggregateException aExp ? aExp.Flatten().GetBaseException() : exp).Message,
                    Result = false,
                };
            }
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> UnlockUserByLogin(string token, AccountDto @params)
        {
            if (string.IsNullOrWhiteSpace(token) || token.Length == 0)
            {
                return OperationResultDto.Unauthorized;
            }

            try
            {
                var user = await _memberService.GetAuthenticatedUserAsync(token).Unwrap();

                if (user == null)
                {
                    return OperationResultDto.Unauthorized;
                }

                var workgroups = (await _memberService.ListMemberOfAsync(token, user.ID).Unwrap())
                    .Select(x => x.Name).ToArray();

                if (!(_options.Value.RequredWorkgroups?.All(x => workgroups.Contains(x)) ?? true))
                {
                    throw new InvalidOperationException($"Account '{user.Name}' don't have permissions to access connector.");
                }

                return await _service.UnlockUserByLogin(token, @params);
            }
            catch (Exception exp)
            {
                _logger.Error(exp);
                return new OperationResultDto
                {
                    Error = (exp is AggregateException aExp ? aExp.Flatten().GetBaseException() : exp).Message,
                    Result = false,
                };
            }
        }

        /// <inheritdoc/>
        public async Task<OperationResultDto> UpdateAccount(string token, AccountDto @params)
        {
            try
            {
                return await _service.UpdateAccount(token, @params);
            }
            catch (MemberNotFoundException exp) when (_options.Value.UseQueue)
            {
                _logger.Error(exp);
                using (var queue = _queueFactory.Create<RetryMessage>(_options.Value.QueueSection))
                {
                    await queue.Writer.Produce(new RetryMessage
                    {
                        Method = nameof(UpdateAccount),
                        Params = @params,
                    });
                }
            }
            catch (Exception exp)
            {
                _logger.Error(exp);
                return new OperationResultDto
                {
                    Error = (exp is AggregateException aExp ? aExp.Flatten().GetBaseException() : exp).Message,
                    Result = false,
                };
            }

            return OperationResultDto.Success;
        }
    }
}
