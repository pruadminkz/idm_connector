﻿namespace bgTeam.Fksud.Common
{
    using System.Linq;

    /// <summary>
    /// Сервис преобразования данных.
    /// </summary>
    public interface IDataMapper
    {
        /// <summary>
        /// Преобразование данных.
        /// </summary>
        /// <typeparam name="TSrc">Входящий тип.</typeparam>
        /// <typeparam name="TDest">Исходящий тип.</typeparam>
        /// <param name="src">Входящие параметры.</param>
        /// <returns>Результат преобразования.</returns>
        TDest Map<TSrc, TDest>(TSrc src);

        /// <summary>
        /// Обновление данных.
        /// </summary>
        /// <typeparam name="TSrc">Входящий тип.</typeparam>
        /// <typeparam name="TDest">Исходящий тип.</typeparam>
        /// <param name="dest">Объект для обновления.</param>
        /// <param name="src">Входящие параметры.</param>
        /// <returns>Результат обновления.</returns>
        TDest Map<TSrc, TDest>(TDest dest, TSrc src);

        /// <summary>
        /// Проекция данных в запросе.
        /// </summary>
        /// <typeparam name="TSrc">Входящий тип.</typeparam>
        /// <typeparam name="TDest">Исходящий тип.</typeparam>
        /// <param name="src">Входящие запрос.</param>
        /// <returns>Преобразованный запрос.</returns>
        IQueryable<TDest> Project<TSrc, TDest>(IQueryable<TSrc> src);
    }
}
