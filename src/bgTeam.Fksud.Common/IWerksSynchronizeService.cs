﻿namespace bgTeam.Fksud.Common
{
    using System.Threading.Tasks;
    using bgTeam.Fksud.Domain.Entities;

    public interface IWerksSynchronizeService
    {
        /// <summary>
        /// Метод синхронизации справочника WERKS (территориальное деление).
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Представление объекта Werks (справочник территориального деления).</param>
        /// <returns>Результат выполнения операции.</returns>
        Task Synchronize(string token, WerksMgf[] @params);
    }
}
