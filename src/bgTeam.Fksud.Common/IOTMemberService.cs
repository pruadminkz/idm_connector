﻿namespace bgTeam.Fksud.Common
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using bgTeam.Fksud.Domain.Entities;
    using bgTeam.Fksud.Soap.Dto;

    public interface IOTMemberService
    {
        Task CreateGroups(string token, IEnumerable<IDivisions> diffList);

        Task UpdateGroups(string token, IEnumerable<IEntityName> diffList);

        Task DeleteMembers(string token, IEnumerable<IDivisions> diffList);

        Task AddListOfMembersToGroup(string token, Dictionary<long, List<long>> divisionsAndUsers);

        Task DeleteListUsers(string token, IEnumerable<UserTemp> diffList);

        Task DeleteGroups(string token, IEnumerable<Kuaf> list);

        Task<bool> SetUserPrivilegesLoginAndPublicAccess(string login, OTAuthentication authToken);
    }
}
