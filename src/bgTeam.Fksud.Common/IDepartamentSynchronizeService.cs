﻿namespace bgTeam.Fksud.Common
{
    using System.Threading.Tasks;
    using bgTeam.Fksud.Domain.Entities;

    public interface IDepartamentSynchronizeService
    {
        Task Synchronize(string token, DivisionsMgf[] @params);
    }
}
