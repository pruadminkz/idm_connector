﻿namespace bgTeam.Fksud.Common
{
    using System.Threading.Tasks;
    using bgTeam.Fksud.Domain.Dto;

    /// <summary>
    /// Сервис взаимодействия со справочником территориального деления.
    /// </summary>
    public interface IWerksService
    {
        /// <summary>
        /// Метод синхронизации справочника территориального деления.
        /// </summary>
        /// <param name="params">Параметры операции.</param>
        /// <returns>Результат выполнения операции.</returns>
        Task<OperationResultDto> SyncWerks(string token, WerksDto[] @params);
    }
}
