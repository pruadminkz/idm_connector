﻿namespace bgTeam.Fksud.Common
{
    using System;

    public abstract class QueueChannel<T> : IDisposable
    {
        public QueueReader<T> Reader { get; protected set; }

        public QueueWriter<T> Writer { get; protected set; }

        /// <inheritdoc/>
        public void Dispose()
        {
            Reader?.Dispose();
            Writer?.Dispose();
        }
    }
}
