﻿namespace bgTeam.Fksud.Common.Utilities
{
    using System.Collections.Generic;
    using bgTeam.Fksud.Domain.Entities;

    public class DivisionComparer : IEqualityComparer<IDivisions>
    {
        public int GetHashCode(IDivisions obj)
        {
            return obj == null ? 0 : obj.SYSTEM_ID.GetHashCode();
        }

        public bool Equals(IDivisions x, IDivisions y)
        {
            return x.SAP_ID == y.SAP_ID &&
                x.SYSTEM_ID == y.SYSTEM_ID &&
                x.PARENT_ID == y.PARENT_ID &&
                x.ROOT_DIVISION_ID == y.ROOT_DIVISION_ID;
        }
    }
}
