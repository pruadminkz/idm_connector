﻿namespace bgTeam.Fksud.Common.Utilities
{
    using System.Collections.Generic;
    using bgTeam.Fksud.Domain.Entities;

    public class DivisionNameComparer : IEqualityComparer<IEntityDivision>
    {
        public int GetHashCode(IEntityDivision obj)
        {
            return obj == null ? 0 : obj.SysId.GetHashCode();
        }

        public bool Equals(IEntityDivision x, IEntityDivision y)
        {
            return x.SysId == y.SysId &&
                x.DivisionName == y.DivisionName;
        }
    }
}
