﻿namespace bgTeam.Fksud.Common.Utilities
{
    using System.Collections.Generic;
    using bgTeam.Fksud.Domain.Entities;

    public class EntityNameComparer : IEqualityComparer<IEntityName>
    {
        public int GetHashCode(IEntityName obj)
        {
            return obj == null ? 0 : obj.SysId.GetHashCode();
        }

        public bool Equals(IEntityName x, IEntityName y)
        {
            return x.SysId == y.SysId &&
                x.Name == y.Name;
        }
    }
}
