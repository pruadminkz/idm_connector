﻿namespace bgTeam.Fksud.Common
{
    using System.Threading.Tasks;
    using bgTeam.Fksud.Domain.Dto;

    /// <summary>
    /// Сервис взаимодействия с департаментами.
    /// </summary>
    public interface IDepartamentsService
    {
        /// <summary>
        /// Метод синхронизации департаментов.
        /// </summary>
        /// <param name="params">Параметры операции.</param>
        /// <returns>Результат выполнения операции.</returns>
        Task<OperationResultDto> SyncDepartaments(string token, DepartamentDto[] @params);
    }
}
