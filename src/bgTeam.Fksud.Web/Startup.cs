namespace bgTeam.Fksud.Web
{
    using bgTeam.Fksud.WebApp;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.DependencyInjection;
    using Newtonsoft.Json.Serialization;
    using Swashbuckle.AspNetCore.Swagger;
    using System;
    using System.IO;

    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                });

            services.AddRouting();

            services.AddAuthentication();

            // Swagger
            services.AddSwaggerGen(x =>
            {
                x.SwaggerDoc("v1", new Info { Title = "Connector IDM API", Version = "v1" });

                foreach (var item in Directory.GetFiles(AppContext.BaseDirectory, $"bgTeam.Fksud.*.xml"))
                {
                    x.IncludeXmlComments(item, true);
                }
            });

            AppIocConfigure.Configure(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/home/error");
            }

            app.UseSwaggerUI(x =>
            {
                x.SwaggerEndpoint("v1/swagger.json", "bgTeam.Fksud API");
            });

            app.UseSwagger();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseMiddleware<AppMiddlewareException>();

            app.UseMvc(x =>
            {
                x.MapRoute(name: "default", template: "{controller=Home}/{action=Index}");
            });
        }
    }
}
