namespace bgTeam.Fksud.WebApp
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using bgTeam.Fksud.Domain.Dto;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;

    public class AppMiddlewareException
    {
        private readonly IAppLogger _logger;
        private readonly RequestDelegate _next;

        public AppMiddlewareException(IAppLogger logger, RequestDelegate next)
        {
            _logger = logger;
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (AggregateException exp)
            {
                _logger.Error(exp);
                await HandleExceptionAsync(context, exp.Flatten().GetBaseException(), HttpStatusCode.OK);
            }
            catch (Exception exp)
            {
                _logger.Error(exp);
                await HandleExceptionAsync(context, exp, HttpStatusCode.OK);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exp, HttpStatusCode code)
        {
            var result = JsonConvert.SerializeObject(new OperationResultDto
            {
                Result = false,
                Error = exp.Message,
            });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

            return context.Response.WriteAsync(result);
        }
    }
}
