namespace bgTeam.Fksud.WebApp
{
    using bgTeam.Core;
    using bgTeam.DataAccess;
    using System;
    using System.Collections.Generic;
    using System.Text;

    internal class AppSettings : IConnectionSetting
    {
        public string ConnectionString { get; set; }

        public AppSettings(IAppConfiguration config)
        {
            ConnectionString = config.GetConnectionString("MAINDB");
        }
    }
}
