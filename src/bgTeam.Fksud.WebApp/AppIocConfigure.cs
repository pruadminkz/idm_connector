namespace bgTeam.Fksud.WebApp
{
    using bgTeam;
    using bgTeam.Core;
    using bgTeam.Core.Impl;
    using bgTeam.DataAccess;
    using bgTeam.DataAccess.Impl.Dapper;
    using bgTeam.DataAccess.Impl.SAPHANA;
    using bgTeam.Fksud.Common;
    using bgTeam.Fksud.Common.Impl;
    using bgTeam.Fksud.Domain.Dto;
    using bgTeam.Fksud.Soap.Dto;
    using bgTeam.Fksud.Soap.Services;
    using bgTeam.Fksud.Soap.Services.Impl;
    using bgTeam.Fksud.Story;
    using bgTeam.Impl;
    using bgTeam.Impl.Serilog;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class AppIocConfigure
    {
        class ConnectionStringProxy : IConnectionSetting
        {
            public string ConnectionString
            {
                get;
                set;
            }

            public ConnectionStringProxy(IConfiguration configuration)
            {
                ConnectionString = configuration.GetConnectionString("OTCSDB");
            }
        }

        public static void Configure(IServiceCollection services)
        {
            services.Scan(scan => scan
                     .FromAssemblyOf<IStoryLibrary>()
                     .AddClasses(classes => classes.AssignableTo(typeof(IStory<,>)))
                     .AsImplementedInterfaces()
                     .WithTransientLifetime());

            var config = new AppConfigurationDefault();

            var otsSection = config.GetSection(nameof(OpenTextService));
            var otdsSection = config.GetSection(nameof(OpenTextServiceDecorator));

            var queueSectionName = otdsSection[nameof(OpenTextServiceDecoratorOptions.QueueSection)];

            services
                .AddSingleton<IAppConfiguration>(config)
                .AddSingleton<IDataMapper, AutoMapperImpl>()
                .AddSingleton<IAppLogger, AppLoggerSerilog>()
                .AddSingleton<IAppLoggerConfig, AppLoggerSerilogConfig>()
                .AddSingleton<IAuthenticationService, AuthenticationService>()
                .AddSingleton<IMemberService, MemberService>()
                .AddSingleton<IStoryFactory, StoryFactory>()
                .AddSingleton<IStoryBuilder, StoryBuilder>()
                .AddSingleton<IOpenTextService, OpenTextService>()
                .AddSingleton<IWerksService, WerksService>()
                .AddSingleton<IDepartamentsService, DepartamentsService>()
                .AddSingleton<IOTMemberService, OTMemberService>()
                .AddSingleton<IWerksSynchronizeService, WerksSynchronizeService>()
                .AddSingleton<IDepartamentSynchronizeService, DepartamentSynchronizeService>()
                .AddSingleton<ISqlDialect, SqlDialectDapper>()
                .AddSingleton<IConnectionSetting, ConnectionStringProxy>()
                .AddSingleton<IConnectionFactory, ConnectionFactorySapHana>()
                .AddSingleton<IRepository, RepositoryDapper>()
                .AddSingleton<ICrudService, CrudServiceDapper>()
                .Configure<SoapGateways>(otsSection)
                .Configure<OpenTextServiceOptions>(otsSection)
                .Configure<OpenTextServiceDecoratorOptions>(otdsSection)
                .Configure<KafkaQueueOptions>(queueSectionName, otdsSection.GetSection(queueSectionName))
                .AddSingleton<IQueueFactory, KafkaQueueFactory>()
                .Decorate<IOpenTextService, OpenTextServiceDecorator>();
        }
    }
}
