﻿namespace bgTeam.Fksud.WebApp.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using bgTeam.Fksud.Common;
    using bgTeam.Fksud.Domain.Dto;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Svedo Api.
    /// </summary>
    [Route("[controller]/[action]")]
    public class SvedoController : Controller
    {
        private readonly IOpenTextService _service;

        /// <summary>
        /// Initializes a new instance of the <see cref="SvedoController"/> class.
        /// </summary>
        /// <param name="service">Сервис взаимодействия с CS.</param>
        public SvedoController(IOpenTextService service)
        {
            _service = service;
        }

        /// <summary>
        /// Метод получения информации об учетной записи.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="username">Логин учетной записи.</param>
        /// <returns>Представление объекта Account из системы со всеми атрибутами и рабочими группами(workgroup).</returns>
        [HttpGet]
        public Task<AccountDto> GetAccount(string token, string username)
        {
            return _service.GetAccount(token, username);
        }

        /// <summary>
        /// Метод разблокировки учетной записи.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="personnelNumber">Табельный номер.</param>
        /// <returns>Результат выполнения операции.</returns>
        [HttpGet]
        public Task<OperationResultDto> UnlockUser(string token, string personnelNumber)
        {
            return _service.UnlockUser(token, personnelNumber);
        }

        /// <summary>
        /// Метод разблокировки учетной записи по логину.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="account">Представление объекта Account из системы.</param>
        /// <returns>Результат выполнения операции.</returns>
        [HttpPost]
        public Task<OperationResultDto> UnlockUserByLogin(string token, [FromBody]AccountDto account)
        {
            return _service.UnlockUserByLogin(token, account);
        }

        /// <summary>
        /// Метод изменения атрибутов учетной записи и набора рабочих групп и прав.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="account">Представление объекта Account из системы со всеми атрибутами и рабочими группами.</param>
        /// <returns>Результат выполнения операции.</returns>
        [HttpPost]
        public Task<OperationResultDto> UpdateAccount(string token, [FromBody]AccountDto account)
        {
            return _service.UpdateAccount(token, account);
        }

        /// <summary>
        /// Метод получения списка всех учетных записей.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <returns>Список учетных записей.</returns>
        [HttpGet]
        public Task<LookupAccountsDto> LookupAccounts(string token)
        {
            return _service.LookupAccounts(token);
        }

        /// <summary>
        /// Метод получения списка рабочих групп.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <returns>Список рабочих групп.</returns>
        [HttpGet]
        public Task<IEnumerable<WorkgroupDto>> LookupWorkgroups(string token)
        {
            return _service.LookupWorkgroups(token);
        }

        /// <summary>
        /// Метод добавление группы в состав другой группы.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Параметры операции.</param>
        /// <returns>Результат выполнения операции.</returns>
        [HttpPost("/[controller]/AddWG2WG")]
        public Task<OperationResultDto> AddWg2Wg(string token, [FromBody]AddWgToWgDto @params)
        {
            return _service.AddWg2Wg(token, @params);
        }

        /// <summary>
        /// Метод добавления учетной записи в группу.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Параметры операции.</param>
        /// <returns>Результат выполнения операции.</returns>
        [HttpPost("/[controller]/AddAccount2WG")]
        public Task<OperationResultDto> AddAccount2Wg(string token, [FromBody]AddAccountToWgDto @params)
        {
            return _service.AddAccount2Wg(token, @params);
        }

        /// <summary>
        /// Метод удаления учетной записи из группы.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Параметры операции.</param>
        /// <returns>Результат выполнения операции.</returns>
        [HttpPost("/[controller]/RemoveAccountFromWG")]
        public Task<OperationResultDto> RemoveAccountFromWg(string token, [FromBody]RemoveAccountFromWgDto @params)
        {
            return _service.RemoveAccountFromWg(token, @params);
        }

        /// <summary>
        /// Метод создания рабочей группы.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Параметры операции.</param>
        /// <returns>Результат выполнения операции.</returns>
        [HttpPost("/[controller]/CreateWG")]
        public Task<OperationResultDto> CreateWg(string token, [FromBody]WorkgroupDto @params)
        {
            return _service.CreateWg(token, @params);
        }

        /// <summary>
        /// Аутентификация в системе.
        /// </summary>
        /// <param name="params">Параметры авторизации.</param>
        /// <returns>Токен авторизации в API.</returns>
        [HttpPost]
        public Task<string> AuthenticateUser([FromBody]AuthorizationDto @params)
        {
            return _service.AuthenticateUser(@params.UserName, @params.Password);
        }

        /// <summary>
        /// Обновление токена авторизации.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <returns>Обновленный токен авторизации в API.</returns>
        [HttpGet]
        public Task<string> RefreshToken(string token)
        {
            return _service.RefreshToken(token);
        }

        /// <summary>
        /// Метод получения аудита.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="dateFrom">Дата с в запросе аудита.</param>
        /// <param name="dateTo">Дата по в запросе аудита.</param>
        /// <returns>Список рабочих групп.</returns>
        [HttpGet]
        public Task<AuditDto> UsersAudit(string token, string dateFrom, string dateTo)
        {
            return _service.LookupAudit(token, dateFrom, dateTo);
        }

        /// <summary>
        /// Метод получения информации в промежуток времени.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Параметры авторизации.</param>
        /// <returns>Список рабочих групп.</returns>
        [HttpPost]
        public Task<bool> GetTimeLapseInfoFI(string token, [FromBody] SendingCsvFileJobDto @params)
        {
            return _service.TimeLapseInfoFI(token, @params);
        }

        /// <summary>
        /// Метод получения информации в промежуток времени.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Параметры авторизации.</param>
        /// <returns>Список рабочих групп.</returns>
        [HttpPost]
        public Task<bool> GetTimeLapseInfoHR(string token, [FromBody] SendingCsvFileJobDto @params)
        {
            return _service.TimeLapseInfoHR(token, @params);
        }
    }
}
