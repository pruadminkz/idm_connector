﻿namespace bgTeam.Fksud.WebApp.Controllers
{
    using System.Threading.Tasks;
    using bgTeam.Fksud.Common;
    using bgTeam.Fksud.Domain.Dto;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Departament Controller.
    /// </summary>
    [Route("[controller]/[action]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DepartamentController : Controller
    {
        private readonly IDepartamentsService _service;

        /// <summary>
        /// Initializes a new instance of the <see cref="DepartamentController"/> class.
        /// </summary>
        /// <param name="service">Сервис взаимодействия с департаментами.</param>
        public DepartamentController(IDepartamentsService service)
        {
            _service = service;
        }

        /// <summary>
        /// Метод синхронизации департаментов.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Список департаментов.</param>
        /// <returns>Результат выполнения операции.</returns>
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<OperationResultDto> SyncDepartaments(string token, [FromBody]DepartamentDto[] @params)
        {
            return _service.SyncDepartaments(token, @params);
        }
    }
}
