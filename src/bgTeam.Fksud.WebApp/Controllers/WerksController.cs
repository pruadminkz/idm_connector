﻿namespace bgTeam.Fksud.WebApp.Controllers
{
    using System.Threading.Tasks;
    using bgTeam.Fksud.Common;
    using bgTeam.Fksud.Domain.Dto;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Werks Controller.
    /// </summary>
    [Route("[controller]/[action]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class WerksController : Controller
    {
        private readonly IWerksService _service;

        /// <summary>
        /// Initializes a new instance of the <see cref="WerksController"/> class.
        /// </summary>
        /// <param name="service">Сервис взаимодействия со справочником регионального деления.</param>
        public WerksController(IWerksService service)
        {
            _service = service;
        }

        /// <summary>
        /// Метод синхронизации справочника регионального деления.
        /// </summary>
        /// <param name="token">Токен авторизации в API.</param>
        /// <param name="params">Список кодов Wekrs.</param>
        /// <returns>Результат выполнения операции.</returns>
        [HttpPost("/[controller]/SyncWerks")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public Task<OperationResultDto> SyncWerks(string token, [FromBody]WerksDto[] @params)
        {
            return _service.SyncWerks(token, @params);
        }
    }
}
