namespace bgTeam.Fksud.WebApp.Controllers
{
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Стандартный контроллер.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Метод по-умолчанию.
        /// </summary>
        /// <returns>Версия.</returns>
        [HttpGet]
        public string Index()
        {
            return "Connector IDM API v1";
        }
    }
}
