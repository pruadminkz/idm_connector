﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CreateUserInDomainResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class CreateUserInDomainResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long CreateUserInDomainResult;

        public CreateUserInDomainResponse()
        {
        }

        public CreateUserInDomainResponse(OTAuthentication OTAuthentication, long CreateUserInDomainResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.CreateUserInDomainResult = CreateUserInDomainResult;
        }
    }
}
