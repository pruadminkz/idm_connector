﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetMemberByIdResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetMemberByIdResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public Member GetMemberByIdResult;

        public GetMemberByIdResponse()
        {
        }

        public GetMemberByIdResponse(OTAuthentication OTAuthentication, Member GetMemberByIdResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.GetMemberByIdResult = GetMemberByIdResult;
        }
    }
}
