﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "UpdateMember", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class UpdateMemberRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public Member member;

        public UpdateMemberRequest()
        {
        }

        public UpdateMemberRequest(OTAuthentication OTAuthentication, Member member)
        {
            this.OTAuthentication = OTAuthentication;
            this.member = member;
        }
    }
}
