﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "SearchForMembersResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class SearchForMembersResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public PageHandle SearchForMembersResult;

        public SearchForMembersResponse()
        {
        }

        public SearchForMembersResponse(OTAuthentication OTAuthentication, PageHandle SearchForMembersResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.SearchForMembersResult = SearchForMembersResult;
        }
    }
}
