﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetUserByLoginNameInDomain", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetUserByLoginNameInDomainRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public string loginName;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 1)]
        public long domainID;

        public GetUserByLoginNameInDomainRequest()
        {
        }

        public GetUserByLoginNameInDomainRequest(OTAuthentication OTAuthentication, string loginName, long domainID)
        {
            this.OTAuthentication = OTAuthentication;
            this.loginName = loginName;
            this.domainID = domainID;
        }
    }
}
