﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CreateExternalUser", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class CreateExternalUserRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public User user;

        public CreateExternalUserRequest()
        {
        }

        public CreateExternalUserRequest(OTAuthentication OTAuthentication, User user)
        {
            this.OTAuthentication = OTAuthentication;
            this.user = user;
        }
    }
}
