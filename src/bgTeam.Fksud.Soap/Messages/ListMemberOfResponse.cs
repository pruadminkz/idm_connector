﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using System.Xml.Serialization;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "ListMemberOfResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class ListMemberOfResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        [XmlElement("ListMemberOfResult", IsNullable = true)]
        public Group[] ListMemberOfResult;

        public ListMemberOfResponse()
        {
        }

        public ListMemberOfResponse(OTAuthentication OTAuthentication, Group[] ListMemberOfResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.ListMemberOfResult = ListMemberOfResult;
        }
    }
}
