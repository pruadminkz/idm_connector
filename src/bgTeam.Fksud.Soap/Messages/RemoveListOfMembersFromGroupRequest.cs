﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using System.Xml.Serialization;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "RemoveListOfMembersFromGroup", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class RemoveListOfMembersFromGroupRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long groupID;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 1)]
        [XmlElement("memberIDs")]
        public long[] memberIDs;

        public RemoveListOfMembersFromGroupRequest()
        {
        }

        public RemoveListOfMembersFromGroupRequest(OTAuthentication OTAuthentication, long groupID, long[] memberIDs)
        {
            this.OTAuthentication = OTAuthentication;
            this.groupID = groupID;
            this.memberIDs = memberIDs;
        }
    }
}
