﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CreateUserResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class CreateUserResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long CreateUserResult;

        public CreateUserResponse()
        {
        }

        public CreateUserResponse(OTAuthentication OTAuthentication, long CreateUserResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.CreateUserResult = CreateUserResult;
        }
    }
}
