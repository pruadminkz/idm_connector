﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "ImpersonateUser", WrapperNamespace = "urn:Core.service.livelink.opentext.com", IsWrapped = true)]
    internal class ImpersonateUserRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:Core.service.livelink.opentext.com", Order = 0)]
        public string userName;

        public ImpersonateUserRequest()
        {
        }

        public ImpersonateUserRequest(OTAuthentication OTAuthentication, string userName)
        {
            this.OTAuthentication = OTAuthentication;
            this.userName = userName;
        }
    }
}
