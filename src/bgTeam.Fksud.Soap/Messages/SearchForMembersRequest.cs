﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "SearchForMembers", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class SearchForMembersRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public MemberSearchOptions options;

        public SearchForMembersRequest()
        {
        }

        public SearchForMembersRequest(OTAuthentication OTAuthentication, MemberSearchOptions options)
        {
            this.OTAuthentication = OTAuthentication;
            this.options = options;
        }
    }
}
