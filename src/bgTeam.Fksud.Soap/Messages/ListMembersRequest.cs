﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "ListMembers", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class ListMembersRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long groupID;

        public ListMembersRequest()
        {
        }

        public ListMembersRequest(OTAuthentication OTAuthentication, long groupID)
        {
            this.OTAuthentication = OTAuthentication;
            this.groupID = groupID;
        }
    }
}
