﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetSearchResultsResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetSearchResultsResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public MemberSearchResults GetSearchResultsResult;

        public GetSearchResultsResponse()
        {
        }

        public GetSearchResultsResponse(OTAuthentication OTAuthentication, MemberSearchResults GetSearchResultsResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.GetSearchResultsResult = GetSearchResultsResult;
        }
    }
}
