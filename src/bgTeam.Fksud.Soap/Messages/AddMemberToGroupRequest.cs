﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "AddMemberToGroup", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class AddMemberToGroupRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long groupID;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 1)]
        public long memberID;

        public AddMemberToGroupRequest()
        {
        }

        public AddMemberToGroupRequest(OTAuthentication OTAuthentication, long groupID, long memberID)
        {
            this.OTAuthentication = OTAuthentication;
            this.groupID = groupID;
            this.memberID = memberID;
        }
    }
}
