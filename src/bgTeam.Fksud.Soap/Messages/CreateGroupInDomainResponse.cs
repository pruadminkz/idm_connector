﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CreateGroupInDomainResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class CreateGroupInDomainResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long CreateGroupInDomainResult;

        public CreateGroupInDomainResponse()
        {
        }

        public CreateGroupInDomainResponse(OTAuthentication OTAuthentication, long CreateGroupInDomainResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.CreateGroupInDomainResult = CreateGroupInDomainResult;
        }
    }
}
