﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "UpdatePassword", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class UpdatePasswordRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long memberID;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 1)]
        public string newPassword;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 2)]
        public string oldPassword;

        public UpdatePasswordRequest()
        {
        }

        public UpdatePasswordRequest(OTAuthentication OTAuthentication, long memberID, string newPassword, string oldPassword)
        {
            this.OTAuthentication = OTAuthentication;
            this.memberID = memberID;
            this.newPassword = newPassword;
            this.oldPassword = oldPassword;
        }
    }
}
