﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using System.Xml.Serialization;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "ListRightsByIDResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class ListRightsByIDResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        [XmlElement("ListRightsByIDResult", IsNullable = true)]
        public MemberRight[] ListRightsByIDResult;

        public ListRightsByIDResponse()
        {
        }

        public ListRightsByIDResponse(OTAuthentication OTAuthentication, MemberRight[] ListRightsByIDResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.ListRightsByIDResult = ListRightsByIDResult;
        }
    }
}
