﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetUserByLoginNameResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetUserByLoginNameResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public User GetUserByLoginNameResult;

        public GetUserByLoginNameResponse()
        {
        }

        public GetUserByLoginNameResponse(OTAuthentication OTAuthentication, User GetUserByLoginNameResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.GetUserByLoginNameResult = GetUserByLoginNameResult;
        }
    }
}
