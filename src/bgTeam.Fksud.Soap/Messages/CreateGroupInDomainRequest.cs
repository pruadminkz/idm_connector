﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using System.Xml.Serialization;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CreateGroupInDomain", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class CreateGroupInDomainRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public string name;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 1)]
        [XmlElement(IsNullable = true)]
        public long? leaderID;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 2)]
        public long domainID;

        public CreateGroupInDomainRequest()
        {
        }

        public CreateGroupInDomainRequest(OTAuthentication OTAuthentication, string name, long? leaderID, long domainID)
        {
            this.OTAuthentication = OTAuthentication;
            this.name = name;
            this.leaderID = leaderID;
            this.domainID = domainID;
        }
    }
}
