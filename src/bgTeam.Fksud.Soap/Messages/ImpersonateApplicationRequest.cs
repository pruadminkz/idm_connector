﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "ImpersonateApplication", WrapperNamespace = "urn:Core.service.livelink.opentext.com", IsWrapped = true)]
    internal class ImpersonateApplicationRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:Core.service.livelink.opentext.com", Order = 0)]
        public string applicationID;

        public ImpersonateApplicationRequest()
        {
        }

        public ImpersonateApplicationRequest(OTAuthentication OTAuthentication, string applicationID)
        {
            this.OTAuthentication = OTAuthentication;
            this.applicationID = applicationID;
        }
    }
}
