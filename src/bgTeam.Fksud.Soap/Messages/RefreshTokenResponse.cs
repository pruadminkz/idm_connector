﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "RefreshTokenResponse", WrapperNamespace = "urn:Core.service.livelink.opentext.com", IsWrapped = true)]
    internal class RefreshTokenResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:Core.service.livelink.opentext.com", Order = 0)]
        public string RefreshTokenResult;

        public RefreshTokenResponse()
        {
        }

        public RefreshTokenResponse(OTAuthentication OTAuthentication, string RefreshTokenResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.RefreshTokenResult = RefreshTokenResult;
        }
    }
}
