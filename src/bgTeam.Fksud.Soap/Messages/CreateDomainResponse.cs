﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CreateDomainResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class CreateDomainResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long CreateDomainResult;

        public CreateDomainResponse()
        {
        }

        public CreateDomainResponse(OTAuthentication OTAuthentication, long CreateDomainResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.CreateDomainResult = CreateDomainResult;
        }
    }
}
