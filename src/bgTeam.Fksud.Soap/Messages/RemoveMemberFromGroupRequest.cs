﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "RemoveMemberFromGroup", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class RemoveMemberFromGroupRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long groupID;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 1)]
        public long memberID;

        public RemoveMemberFromGroupRequest()
        {
        }

        public RemoveMemberFromGroupRequest(OTAuthentication OTAuthentication, long groupID, long memberID)
        {
            this.OTAuthentication = OTAuthentication;
            this.groupID = groupID;
            this.memberID = memberID;
        }
    }
}
