﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetGroupByNameResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetGroupByNameResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public Group GetGroupByNameResult;

        public GetGroupByNameResponse()
        {
        }

        public GetGroupByNameResponse(OTAuthentication OTAuthentication, Group GetGroupByNameResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.GetGroupByNameResult = GetGroupByNameResult;
        }
    }
}
