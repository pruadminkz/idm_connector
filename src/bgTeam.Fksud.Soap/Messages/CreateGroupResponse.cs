﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CreateGroupResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class CreateGroupResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long CreateGroupResult;

        public CreateGroupResponse()
        {
        }

        public CreateGroupResponse(OTAuthentication OTAuthentication, long CreateGroupResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.CreateGroupResult = CreateGroupResult;
        }
    }
}
