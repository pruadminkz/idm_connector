﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CombineApplicationToken", WrapperNamespace = "urn:Core.service.livelink.opentext.com", IsWrapped = true)]
    internal class CombineApplicationTokenRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:Core.service.livelink.opentext.com", Order = 0)]
        public string applicationToken;

        public CombineApplicationTokenRequest()
        {
        }

        public CombineApplicationTokenRequest(OTAuthentication OTAuthentication, string applicationToken)
        {
            this.OTAuthentication = OTAuthentication;
            this.applicationToken = applicationToken;
        }
    }
}
