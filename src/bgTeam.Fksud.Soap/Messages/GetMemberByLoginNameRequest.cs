﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetMemberByLoginName", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetMemberByLoginNameRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public string loginName;

        public GetMemberByLoginNameRequest()
        {
        }

        public GetMemberByLoginNameRequest(OTAuthentication OTAuthentication, string loginName)
        {
            this.OTAuthentication = OTAuthentication;
            this.loginName = loginName;
        }
    }
}
