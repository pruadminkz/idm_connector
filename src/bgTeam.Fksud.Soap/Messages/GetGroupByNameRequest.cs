﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetGroupByName", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetGroupByNameRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public string name;

        public GetGroupByNameRequest()
        {
        }

        public GetGroupByNameRequest(OTAuthentication OTAuthentication, string name)
        {
            this.OTAuthentication = OTAuthentication;
            this.name = name;
        }
    }
}
