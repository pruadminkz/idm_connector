﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using System.Xml.Serialization;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CreateGroup", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class CreateGroupRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public string name;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 1)]
        [XmlElement(IsNullable = true)]
        public long? leaderID;

        public CreateGroupRequest()
        {
        }

        public CreateGroupRequest(OTAuthentication OTAuthentication, string name, long? leaderID)
        {
            this.OTAuthentication = OTAuthentication;
            this.name = name;
            this.leaderID = leaderID;
        }
    }
}
