﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetMetadataLanguage", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetMetadataLanguageRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        public GetMetadataLanguageRequest()
        {
        }

        public GetMetadataLanguageRequest(OTAuthentication OTAuthentication)
        {
            this.OTAuthentication = OTAuthentication;
        }
    }
}
