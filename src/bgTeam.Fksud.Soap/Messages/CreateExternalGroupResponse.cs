﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CreateExternalGroupResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class CreateExternalGroupResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long CreateExternalGroupResult;

        public CreateExternalGroupResponse()
        {
        }

        public CreateExternalGroupResponse(OTAuthentication OTAuthentication, long CreateExternalGroupResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.CreateExternalGroupResult = CreateExternalGroupResult;
        }
    }
}
