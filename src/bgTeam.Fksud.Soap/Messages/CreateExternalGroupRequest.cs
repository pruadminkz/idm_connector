﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using System.Xml.Serialization;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CreateExternalGroup", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class CreateExternalGroupRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public string name;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 1)]
        [XmlElement(IsNullable = true)]
        public long? leaderID;

        public CreateExternalGroupRequest()
        {
        }

        public CreateExternalGroupRequest(OTAuthentication OTAuthentication, string name, long? leaderID)
        {
            this.OTAuthentication = OTAuthentication;
            this.name = name;
            this.leaderID = leaderID;
        }
    }
}
