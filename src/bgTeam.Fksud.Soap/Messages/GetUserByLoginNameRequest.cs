﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetUserByLoginName", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetUserByLoginNameRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public string loginName;

        public GetUserByLoginNameRequest()
        {
        }

        public GetUserByLoginNameRequest(OTAuthentication OTAuthentication, string loginName)
        {
            this.OTAuthentication = OTAuthentication;
            this.loginName = loginName;
        }
    }
}
