﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetSessionExpirationDate", WrapperNamespace = "urn:Core.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetSessionExpirationDateRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        public GetSessionExpirationDateRequest()
        {
        }

        public GetSessionExpirationDateRequest(OTAuthentication OTAuthentication)
        {
            this.OTAuthentication = OTAuthentication;
        }
    }
}
