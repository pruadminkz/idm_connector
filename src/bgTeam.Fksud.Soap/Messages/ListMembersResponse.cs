﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using System.Xml.Serialization;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "ListMembersResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class ListMembersResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        [XmlElement("ListMembersResult", IsNullable = true)]
        public Member[] ListMembersResult;

        public ListMembersResponse()
        {
        }

        public ListMembersResponse(OTAuthentication OTAuthentication, Member[] ListMembersResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.ListMembersResult = ListMembersResult;
        }
    }
}
