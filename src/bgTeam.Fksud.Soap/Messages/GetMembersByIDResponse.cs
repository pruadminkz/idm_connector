﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using System.Xml.Serialization;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetMembersByIDResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetMembersByIDResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        [XmlElement("GetMembersByIDResult", IsNullable = true)]
        public Member[] GetMembersByIDResult;

        public GetMembersByIDResponse()
        {
        }

        public GetMembersByIDResponse(OTAuthentication OTAuthentication, Member[] GetMembersByIDResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.GetMembersByIDResult = GetMembersByIDResult;
        }
    }
}
