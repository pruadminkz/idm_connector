﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "SetMetadataLanguage", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class SetMetadataLanguageRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public string languageCode;

        public SetMetadataLanguageRequest()
        {
        }

        public SetMetadataLanguageRequest(OTAuthentication OTAuthentication, string languageCode)
        {
            this.OTAuthentication = OTAuthentication;
            this.languageCode = languageCode;
        }
    }
}
