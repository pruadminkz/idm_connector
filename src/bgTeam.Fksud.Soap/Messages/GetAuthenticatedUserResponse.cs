﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetAuthenticatedUserResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetAuthenticatedUserResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public User GetAuthenticatedUserResult;

        public GetAuthenticatedUserResponse()
        {
        }

        public GetAuthenticatedUserResponse(OTAuthentication OTAuthentication, User GetAuthenticatedUserResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.GetAuthenticatedUserResult = GetAuthenticatedUserResult;
        }
    }
}
