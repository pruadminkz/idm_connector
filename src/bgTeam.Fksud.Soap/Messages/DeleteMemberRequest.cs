﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "DeleteMember", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class DeleteMemberRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long memberID;

        public DeleteMemberRequest()
        {
        }

        public DeleteMemberRequest(OTAuthentication OTAuthentication, long memberID)
        {
            this.OTAuthentication = OTAuthentication;
            this.memberID = memberID;
        }
    }
}
