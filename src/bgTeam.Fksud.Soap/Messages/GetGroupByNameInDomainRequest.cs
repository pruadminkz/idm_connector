﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetGroupByNameInDomain", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetGroupByNameInDomainRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public string name;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 1)]
        public long domainID;

        public GetGroupByNameInDomainRequest()
        {
        }

        public GetGroupByNameInDomainRequest(OTAuthentication OTAuthentication, string name, long domainID)
        {
            this.OTAuthentication = OTAuthentication;
            this.name = name;
            this.domainID = domainID;
        }
    }
}
