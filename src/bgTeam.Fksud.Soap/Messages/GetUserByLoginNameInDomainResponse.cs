﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetUserByLoginNameInDomainResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetUserByLoginNameInDomainResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public User GetUserByLoginNameInDomainResult;

        public GetUserByLoginNameInDomainResponse()
        {
        }

        public GetUserByLoginNameInDomainResponse(OTAuthentication OTAuthentication, User GetUserByLoginNameInDomainResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.GetUserByLoginNameInDomainResult = GetUserByLoginNameInDomainResult;
        }
    }
}
