﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CreateUserInDomain", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class CreateUserInDomainRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public User user;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 1)]
        public long domainID;

        public CreateUserInDomainRequest()
        {
        }

        public CreateUserInDomainRequest(OTAuthentication OTAuthentication, User user, long domainID)
        {
            this.OTAuthentication = OTAuthentication;
            this.user = user;
            this.domainID = domainID;
        }
    }
}
