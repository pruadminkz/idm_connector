﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetGroupByNameInDomainResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetGroupByNameInDomainResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public Group GetGroupByNameInDomainResult;

        public GetGroupByNameInDomainResponse()
        {
        }

        public GetGroupByNameInDomainResponse(OTAuthentication OTAuthentication, Group GetGroupByNameInDomainResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.GetGroupByNameInDomainResult = GetGroupByNameInDomainResult;
        }
    }
}
