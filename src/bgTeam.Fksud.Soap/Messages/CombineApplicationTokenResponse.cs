﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CombineApplicationTokenResponse", WrapperNamespace = "urn:Core.service.livelink.opentext.com", IsWrapped = true)]
    internal class CombineApplicationTokenResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:Core.service.livelink.opentext.com", Order = 0)]
        public string CombineApplicationTokenResult;

        public CombineApplicationTokenResponse()
        {
        }

        public CombineApplicationTokenResponse(OTAuthentication OTAuthentication, string CombineApplicationTokenResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.CombineApplicationTokenResult = CombineApplicationTokenResult;
        }
    }
}
