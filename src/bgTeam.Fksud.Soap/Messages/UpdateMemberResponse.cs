﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "UpdateMemberResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class UpdateMemberResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public Member UpdateMemberResult;

        public UpdateMemberResponse()
        {
        }

        public UpdateMemberResponse(OTAuthentication OTAuthentication, Member UpdateMemberResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.UpdateMemberResult = UpdateMemberResult;
        }
    }
}
