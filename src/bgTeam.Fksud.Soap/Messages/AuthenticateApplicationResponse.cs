﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "AuthenticateApplicationResponse", WrapperNamespace = "urn:Core.service.livelink.opentext.com", IsWrapped = true)]
    internal class AuthenticateApplicationResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:Core.service.livelink.opentext.com", Order = 0)]
        public string AuthenticateApplicationResult;

        public AuthenticateApplicationResponse()
        {
        }

        public AuthenticateApplicationResponse(OTAuthentication OTAuthentication, string AuthenticateApplicationResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.AuthenticateApplicationResult = AuthenticateApplicationResult;
        }
    }
}
