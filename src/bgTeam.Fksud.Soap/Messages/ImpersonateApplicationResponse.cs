﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "ImpersonateApplicationResponse", WrapperNamespace = "urn:Core.service.livelink.opentext.com", IsWrapped = true)]
    internal class ImpersonateApplicationResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:Core.service.livelink.opentext.com", Order = 0)]
        public string ImpersonateApplicationResult;

        public ImpersonateApplicationResponse()
        {
        }

        public ImpersonateApplicationResponse(OTAuthentication OTAuthentication, string ImpersonateApplicationResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.ImpersonateApplicationResult = ImpersonateApplicationResult;
        }
    }
}
