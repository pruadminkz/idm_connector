﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "ListMemberOf", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class ListMemberOfRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long memberID;

        public ListMemberOfRequest()
        {
        }

        public ListMemberOfRequest(OTAuthentication OTAuthentication, long memberID)
        {
            this.OTAuthentication = OTAuthentication;
            this.memberID = memberID;
        }
    }
}
