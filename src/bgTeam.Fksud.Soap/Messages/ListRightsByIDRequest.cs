﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "ListRightsByID", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class ListRightsByIDRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long id;

        public ListRightsByIDRequest()
        {
        }

        public ListRightsByIDRequest(OTAuthentication OTAuthentication, long id)
        {
            this.OTAuthentication = OTAuthentication;
            this.id = id;
        }
    }
}
