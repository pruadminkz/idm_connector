﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "AuthenticateApplication", WrapperNamespace = "urn:Core.service.livelink.opentext.com", IsWrapped = true)]
    internal class AuthenticateApplicationRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:Core.service.livelink.opentext.com", Order = 0)]
        public string applicationID;

        [MessageBodyMember(Namespace = "urn:Core.service.livelink.opentext.com", Order = 1)]
        public string password;

        public AuthenticateApplicationRequest()
        {
        }

        public AuthenticateApplicationRequest(OTAuthentication OTAuthentication, string applicationID, string password)
        {
            this.OTAuthentication = OTAuthentication;
            this.applicationID = applicationID;
            this.password = password;
        }
    }
}
