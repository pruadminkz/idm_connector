﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "RemoveListOfMembersFromGroupResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class RemoveListOfMembersFromGroupResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        public RemoveListOfMembersFromGroupResponse()
        {
        }

        public RemoveListOfMembersFromGroupResponse(OTAuthentication OTAuthentication)
        {
            this.OTAuthentication = OTAuthentication;
        }
    }
}
