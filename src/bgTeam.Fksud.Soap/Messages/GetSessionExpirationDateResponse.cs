﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System;
    using System.ServiceModel;
    using System.Xml.Serialization;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetSessionExpirationDateResponse", WrapperNamespace = "urn:Core.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetSessionExpirationDateResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [XmlElement(IsNullable = true)]
        [MessageBodyMember(Namespace = "urn:Core.service.livelink.opentext.com", Order = 0)]
        public DateTime? GetSessionExpirationDateResult;

        public GetSessionExpirationDateResponse()
        {
        }

        public GetSessionExpirationDateResponse(OTAuthentication OTAuthentication, DateTime? GetSessionExpirationDateResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.GetSessionExpirationDateResult = GetSessionExpirationDateResult;
        }
    }
}
