﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetSearchResults", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetSearchResultsRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public PageHandle pageHandle;

        public GetSearchResultsRequest()
        {
        }

        public GetSearchResultsRequest(OTAuthentication OTAuthentication, PageHandle pageHandle)
        {
            this.OTAuthentication = OTAuthentication;
            this.pageHandle = pageHandle;
        }
    }
}
