﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "RefreshToken", WrapperNamespace = "urn:Core.service.livelink.opentext.com", IsWrapped = true)]
    internal class RefreshTokenRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        public RefreshTokenRequest()
        {
        }

        public RefreshTokenRequest(OTAuthentication OTAuthentication)
        {
            this.OTAuthentication = OTAuthentication;
        }
    }
}
