﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CreateMemberResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class CreateMemberResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public Member CreateMemberResult;

        public CreateMemberResponse()
        {
        }

        public CreateMemberResponse(OTAuthentication OTAuthentication, Member CreateMemberResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.CreateMemberResult = CreateMemberResult;
        }
    }
}
