﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "ConvertExternalUserToUser", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class ConvertExternalUserToUserRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long userID;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 1)]
        public long groupID;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 2)]
        public MemberPrivileges memberPrivileges;

        public ConvertExternalUserToUserRequest()
        {
        }

        public ConvertExternalUserToUserRequest(OTAuthentication OTAuthentication, long userID, long groupID, MemberPrivileges memberPrivileges)
        {
            this.OTAuthentication = OTAuthentication;
            this.userID = userID;
            this.groupID = groupID;
            this.memberPrivileges = memberPrivileges;
        }
    }
}
