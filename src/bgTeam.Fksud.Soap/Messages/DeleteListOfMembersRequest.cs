﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using System.Xml.Serialization;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "DeleteListOfMembers", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class DeleteListOfMembersRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        [XmlElement("memberIDs")]
        public long[] memberIDs;

        public DeleteListOfMembersRequest()
        {
        }

        public DeleteListOfMembersRequest(OTAuthentication OTAuthentication, long[] memberIDs)
        {
            this.OTAuthentication = OTAuthentication;
            this.memberIDs = memberIDs;
        }
    }
}
