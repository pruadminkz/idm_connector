﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "CreateExternalUserResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class CreateExternalUserResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long CreateExternalUserResult;

        public CreateExternalUserResponse()
        {
        }

        public CreateExternalUserResponse(OTAuthentication OTAuthentication, long CreateExternalUserResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.CreateExternalUserResult = CreateExternalUserResult;
        }
    }
}
