﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetMemberByLoginNameResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetMemberByLoginNameResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public Member GetMemberByLoginNameResult;

        public GetMemberByLoginNameResponse()
        {
        }

        public GetMemberByLoginNameResponse(OTAuthentication OTAuthentication, Member GetMemberByLoginNameResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.GetMemberByLoginNameResult = GetMemberByLoginNameResult;
        }
    }
}
