﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetMemberById", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetMemberByIdRequest
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public long memberID;

        public GetMemberByIdRequest()
        {
        }

        public GetMemberByIdRequest(OTAuthentication OTAuthentication, long memberID)
        {
            this.OTAuthentication = OTAuthentication;
            this.memberID = memberID;
        }
    }
}
