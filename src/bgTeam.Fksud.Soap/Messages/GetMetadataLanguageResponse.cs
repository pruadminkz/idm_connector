﻿
namespace bgTeam.Fksud.Soap.Messages
{
    using System.ServiceModel;
    using bgTeam.Fksud.Soap.Dto;

    [MessageContract(WrapperName = "GetMetadataLanguageResponse", WrapperNamespace = "urn:MemberService.service.livelink.opentext.com", IsWrapped = true)]
    internal class GetMetadataLanguageResponse
    {
        [MessageHeader(Namespace = "urn:api.ecm.opentext.com")]
        public OTAuthentication OTAuthentication;

        [MessageBodyMember(Namespace = "urn:MemberService.service.livelink.opentext.com", Order = 0)]
        public string GetMetadataLanguageResult;

        public GetMetadataLanguageResponse()
        {
        }

        public GetMetadataLanguageResponse(OTAuthentication OTAuthentication, string GetMetadataLanguageResult)
        {
            this.OTAuthentication = OTAuthentication;
            this.GetMetadataLanguageResult = GetMetadataLanguageResult;
        }
    }
}
