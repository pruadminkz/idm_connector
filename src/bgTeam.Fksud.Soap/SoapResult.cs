﻿namespace bgTeam.Fksud.Soap
{
    using bgTeam.Fksud.Soap.Dto;

    public static class SoapResult
    {
        public static SoapResult<T> Create<T>(OTAuthentication oTAuthentication, T result)
        {
            return new SoapResult<T>(oTAuthentication, result);
        }
    }
}
