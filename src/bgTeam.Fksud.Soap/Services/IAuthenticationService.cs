﻿namespace bgTeam.Fksud.Soap.Services
{
    using System;
    using System.Threading.Tasks;
    using bgTeam.Fksud.Soap.Dto;

    public interface IAuthenticationService
    {
        Task<SoapResult<string>> AuthenticateApplicationAsync(OTAuthentication OTAuthentication, string applicationID, string password);

        Task<string> AuthenticateUserAsync(string userName, string userPassword);

        Task<string> AuthenticateUserWithApplicationTokenAsync(string userName, string userPassword, string applicationToken);

        Task<SoapResult<string>> CombineApplicationTokenAsync(OTAuthentication OTAuthentication, string applicationToken);

        Task<string> GetOTDSResourceIDAsync();

        Task<string> GetOTDSServerAsync();

        Task<SoapResult<DateTime?>> GetSessionExpirationDateAsync(OTAuthentication OTAuthentication);

        Task<SoapResult<string>> ImpersonateApplicationAsync(OTAuthentication OTAuthentication, string applicationID);

        Task<SoapResult<string>> ImpersonateUserAsync(OTAuthentication OTAuthentication, string userName);

        Task<SoapResult<string>> RefreshTokenAsync(OTAuthentication OTAuthentication);
    }
}
