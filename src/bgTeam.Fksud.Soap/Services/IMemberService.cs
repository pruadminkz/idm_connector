﻿
namespace bgTeam.Fksud.Soap.Services
{
    using System.Threading.Tasks;
    using bgTeam.Fksud.Soap.Dto;

    public interface IMemberService
    {
        Task<OTAuthentication> AddListOfMembersToGroupAsync(OTAuthentication OTAuthentication, long groupID, long[] memberIDs);

        Task<OTAuthentication> AddMemberToGroupAsync(OTAuthentication OTAuthentication, long groupID, long memberID);

        Task<OTAuthentication> ConvertExternalUserToUserAsync(OTAuthentication OTAuthentication, long userID, long groupID, MemberPrivileges memberPrivileges);

        Task<SoapResult<long>> CreateDomainAsync(OTAuthentication OTAuthentication, string name);

        Task<SoapResult<long>> CreateExternalGroupAsync(OTAuthentication OTAuthentication, string name, long? leaderID);

        Task<SoapResult<long>> CreateExternalUserAsync(OTAuthentication OTAuthentication, User user);

        Task<SoapResult<long>> CreateGroupAsync(OTAuthentication OTAuthentication, string name, long? leaderID);

        Task<SoapResult<long>> CreateGroupInDomainAsync(OTAuthentication OTAuthentication, string name, long? leaderID, long domainID);

        Task<SoapResult<Member>> CreateMemberAsync(OTAuthentication OTAuthentication, Member member);

        Task<SoapResult<long>> CreateUserAsync(OTAuthentication OTAuthentication, User user);

        Task<SoapResult<long>> CreateUserInDomainAsync(OTAuthentication OTAuthentication, User user, long domainID);

        Task<OTAuthentication> DeleteListOfMembersAsync(OTAuthentication OTAuthentication, long[] memberIDs);

        Task<OTAuthentication> DeleteMemberAsync(OTAuthentication OTAuthentication, long memberID);

        Task<SoapResult<User>> GetAuthenticatedUserAsync(OTAuthentication OTAuthentication);

        Task<SoapResult<Group>> GetGroupByNameAsync(OTAuthentication OTAuthentication, string name);

        Task<SoapResult<Group>> GetGroupByNameInDomainAsync(OTAuthentication OTAuthentication, string name, long domainID);

        Task<SoapResult<Member>> GetMemberByIdAsync(OTAuthentication OTAuthentication, long memberID);

        Task<SoapResult<Member>> GetMemberByLoginNameAsync(OTAuthentication OTAuthentication, string loginName);

        Task<SoapResult<Member[]>> GetMembersByIDAsync(OTAuthentication OTAuthentication, long[] memberIDs);

        Task<SoapResult<string>> GetMetadataLanguageAsync(OTAuthentication OTAuthentication);

        Task<SoapResult<MemberSearchResults>> GetSearchResultsAsync(OTAuthentication OTAuthentication, PageHandle pageHandle);

        Task<SoapResult<User>> GetUserByLoginNameAsync(OTAuthentication OTAuthentication, string loginName);

        Task<SoapResult<User>> GetUserByLoginNameInDomainAsync(OTAuthentication OTAuthentication, string loginName, long domainID);

        Task<SoapResult<Group[]>> ListMemberOfAsync(OTAuthentication OTAuthentication, long memberID);

        Task<SoapResult<Member[]>> ListMembersAsync(OTAuthentication OTAuthentication, long groupID);

        Task<SoapResult<MemberRight[]>> ListRightsByIDAsync(OTAuthentication OTAuthentication, long id);

        Task<OTAuthentication> RemoveListOfMembersFromGroupAsync(OTAuthentication OTAuthentication, long groupID, long[] memberIDs);

        Task<OTAuthentication> RemoveMemberFromGroupAsync(OTAuthentication OTAuthentication, long groupID, long memberID);

        Task<SoapResult<PageHandle>> SearchForMembersAsync(OTAuthentication OTAuthentication, MemberSearchOptions options);

        Task<OTAuthentication> SetMetadataLanguageAsync(OTAuthentication OTAuthentication, string languageCode);

        Task<SoapResult<Member>> UpdateMemberAsync(OTAuthentication OTAuthentication, Member member);

        Task<OTAuthentication> UpdatePasswordAsync(OTAuthentication OTAuthentication, long memberID, string newPassword, string oldPassword);
    }
}
