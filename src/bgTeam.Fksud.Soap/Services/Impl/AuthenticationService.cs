﻿namespace bgTeam.Fksud.Soap.Services.Impl
{
    using System;
    using System.ServiceModel;
    using System.Threading.Tasks;
    using System.Xml;
    using bgTeam.Fksud.Soap.Dto;
    using bgTeam.Fksud.Soap.Messages;
    using Microsoft.Extensions.Options;

    public class AuthenticationService : IAuthenticationService
    {
        private readonly AuthenticationClient _client;
        private readonly IOptions<SoapGateways> _options;

        public AuthenticationService(IOptions<SoapGateways> options)
        {
            _options = options;

            _client = new AuthenticationClient(
                new BasicHttpBinding
                {
                    MaxBufferSize = int.MaxValue,
                    ReaderQuotas = XmlDictionaryReaderQuotas.Max,
                    MaxReceivedMessageSize = int.MaxValue,
                    AllowCookies = true
                },
                new EndpointAddress(_options.Value.AuthenticationEndpoint));
        }

        public Task<SoapResult<string>> AuthenticateApplicationAsync(OTAuthentication OTAuthentication, string applicationID, string password)
        {
            var inValue = new AuthenticateApplicationRequest
            {
                OTAuthentication = OTAuthentication,
                applicationID = applicationID,
                password = password
            };
            return _client.AuthenticateApplicationAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.AuthenticateApplicationResult));
        }

        public Task<string> AuthenticateUserAsync(string userName, string userPassword)
        {
            return _client.AuthenticateUserAsync(userName, userPassword);
        }

        public Task<string> AuthenticateUserWithApplicationTokenAsync(string userName, string userPassword, string applicationToken)
        {
            return _client.AuthenticateUserWithApplicationTokenAsync(userName, userPassword, applicationToken);
        }

        public Task<SoapResult<string>> CombineApplicationTokenAsync(OTAuthentication OTAuthentication, string applicationToken)
        {
            var inValue = new CombineApplicationTokenRequest
            {
                OTAuthentication = OTAuthentication,
                applicationToken = applicationToken
            };
            return _client.CombineApplicationTokenAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.CombineApplicationTokenResult));
        }

        public Task<string> GetOTDSResourceIDAsync()
        {
            return _client.GetOTDSResourceIDAsync();
        }

        public Task<string> GetOTDSServerAsync()
        {
            return _client.GetOTDSServerAsync();
        }

        public Task<SoapResult<DateTime?>> GetSessionExpirationDateAsync(OTAuthentication OTAuthentication)
        {
            var inValue = new GetSessionExpirationDateRequest
            {
                OTAuthentication = OTAuthentication
            };
            return _client.GetSessionExpirationDateAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.GetSessionExpirationDateResult));
        }

        public Task<SoapResult<string>> ImpersonateApplicationAsync(OTAuthentication OTAuthentication, string applicationID)
        {
            var inValue = new ImpersonateApplicationRequest
            {
                OTAuthentication = OTAuthentication,
                applicationID = applicationID
            };
            return _client.ImpersonateApplicationAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.ImpersonateApplicationResult));
        }

        public Task<SoapResult<string>> ImpersonateUserAsync(OTAuthentication OTAuthentication, string userName)
        {
            var inValue = new ImpersonateUserRequest
            {
                OTAuthentication = OTAuthentication,
                userName = userName
            };
            return _client.ImpersonateUserAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.ImpersonateUserResult));
        }

        public Task<SoapResult<string>> RefreshTokenAsync(OTAuthentication OTAuthentication)
        {
            var inValue = new RefreshTokenRequest
            {
                OTAuthentication = OTAuthentication
            };
            return _client.RefreshTokenAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.RefreshTokenResult));
        }
    }
}
