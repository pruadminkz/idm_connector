﻿namespace bgTeam.Fksud.Soap.Services.Impl
{
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.Threading.Tasks;
    using bgTeam.Fksud.Soap.Messages;

    internal class MemberServiceClient : ClientBase<IMemberServiceClient>, IMemberServiceClient
    {
        public MemberServiceClient(Binding binding, EndpointAddress remoteAddress) :
                base(binding, remoteAddress)
        {
        }

        public Task<AddListOfMembersToGroupResponse> AddListOfMembersToGroupAsync(AddListOfMembersToGroupRequest request)
        {
            return Channel.AddListOfMembersToGroupAsync(request);
        }

        public Task<AddMemberToGroupResponse> AddMemberToGroupAsync(AddMemberToGroupRequest request)
        {
            return Channel.AddMemberToGroupAsync(request);
        }

        public Task<ConvertExternalUserToUserResponse> ConvertExternalUserToUserAsync(ConvertExternalUserToUserRequest request)
        {
            return Channel.ConvertExternalUserToUserAsync(request);
        }

        public Task<CreateDomainResponse> CreateDomainAsync(CreateDomainRequest request)
        {
            return Channel.CreateDomainAsync(request);
        }

        public Task<CreateExternalGroupResponse> CreateExternalGroupAsync(CreateExternalGroupRequest request)
        {
            return Channel.CreateExternalGroupAsync(request);
        }

        public Task<CreateExternalUserResponse> CreateExternalUserAsync(CreateExternalUserRequest request)
        {
            return Channel.CreateExternalUserAsync(request);
        }

        public Task<CreateGroupResponse> CreateGroupAsync(CreateGroupRequest request)
        {
            return Channel.CreateGroupAsync(request);
        }

        public Task<CreateGroupInDomainResponse> CreateGroupInDomainAsync(CreateGroupInDomainRequest request)
        {
            return Channel.CreateGroupInDomainAsync(request);
        }

        public Task<CreateMemberResponse> CreateMemberAsync(CreateMemberRequest request)
        {
            return Channel.CreateMemberAsync(request);
        }

        public Task<CreateUserResponse> CreateUserAsync(CreateUserRequest request)
        {
            return Channel.CreateUserAsync(request);
        }

        public Task<CreateUserInDomainResponse> CreateUserInDomainAsync(CreateUserInDomainRequest request)
        {
            return Channel.CreateUserInDomainAsync(request);
        }

        public Task<DeleteListOfMembersResponse> DeleteListOfMembersAsync(DeleteListOfMembersRequest request)
        {
            return Channel.DeleteListOfMembersAsync(request);
        }

        public Task<DeleteMemberResponse> DeleteMemberAsync(DeleteMemberRequest request)
        {
            return Channel.DeleteMemberAsync(request);
        }

        public Task<GetAuthenticatedUserResponse> GetAuthenticatedUserAsync(GetAuthenticatedUserRequest request)
        {
            return Channel.GetAuthenticatedUserAsync(request);
        }

        public Task<GetGroupByNameResponse> GetGroupByNameAsync(GetGroupByNameRequest request)
        {
            return Channel.GetGroupByNameAsync(request);
        }

        public Task<GetGroupByNameInDomainResponse> GetGroupByNameInDomainAsync(GetGroupByNameInDomainRequest request)
        {
            return Channel.GetGroupByNameInDomainAsync(request);
        }

        public Task<GetMemberByIdResponse> GetMemberByIdAsync(GetMemberByIdRequest request)
        {
            return Channel.GetMemberByIdAsync(request);
        }

        public Task<GetMemberByLoginNameResponse> GetMemberByLoginNameAsync(GetMemberByLoginNameRequest request)
        {
            return Channel.GetMemberByLoginNameAsync(request);
        }

        public Task<GetMembersByIDResponse> GetMembersByIDAsync(GetMembersByIDRequest request)
        {
            return Channel.GetMembersByIDAsync(request);
        }

        public Task<GetMetadataLanguageResponse> GetMetadataLanguageAsync(GetMetadataLanguageRequest request)
        {
            return Channel.GetMetadataLanguageAsync(request);
        }

        public Task<GetSearchResultsResponse> GetSearchResultsAsync(GetSearchResultsRequest request)
        {
            return Channel.GetSearchResultsAsync(request);
        }

        public Task<GetUserByLoginNameResponse> GetUserByLoginNameAsync(GetUserByLoginNameRequest request)
        {
            return Channel.GetUserByLoginNameAsync(request);
        }

        public Task<GetUserByLoginNameInDomainResponse> GetUserByLoginNameInDomainAsync(GetUserByLoginNameInDomainRequest request)
        {
            return Channel.GetUserByLoginNameInDomainAsync(request);
        }

        public Task<ListMemberOfResponse> ListMemberOfAsync(ListMemberOfRequest request)
        {
            return Channel.ListMemberOfAsync(request);
        }

        public Task<ListMembersResponse> ListMembersAsync(ListMembersRequest request)
        {
            return Channel.ListMembersAsync(request);
        }

        public Task<ListRightsByIDResponse> ListRightsByIDAsync(ListRightsByIDRequest request)
        {
            return Channel.ListRightsByIDAsync(request);
        }

        public Task<RemoveListOfMembersFromGroupResponse> RemoveListOfMembersFromGroupAsync(RemoveListOfMembersFromGroupRequest request)
        {
            return Channel.RemoveListOfMembersFromGroupAsync(request);
        }

        public Task<RemoveMemberFromGroupResponse> RemoveMemberFromGroupAsync(RemoveMemberFromGroupRequest request)
        {
            return Channel.RemoveMemberFromGroupAsync(request);
        }

        public Task<SearchForMembersResponse> SearchForMembersAsync(SearchForMembersRequest request)
        {
            return Channel.SearchForMembersAsync(request);
        }

        public Task<SetMetadataLanguageResponse> SetMetadataLanguageAsync(SetMetadataLanguageRequest request)
        {
            return Channel.SetMetadataLanguageAsync(request);
        }

        public Task<UpdateMemberResponse> UpdateMemberAsync(UpdateMemberRequest request)
        {
            return Channel.UpdateMemberAsync(request);
        }

        public Task<UpdatePasswordResponse> UpdatePasswordAsync(UpdatePasswordRequest request)
        {
            return Channel.UpdatePasswordAsync(request);
        }
    }
}
