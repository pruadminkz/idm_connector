﻿namespace bgTeam.Fksud.Soap.Services.Impl
{
    using System.ServiceModel;
    using System.Threading.Tasks;
    using System.Xml;
    using bgTeam.Fksud.Soap.Dto;
    using bgTeam.Fksud.Soap.Messages;
    using Microsoft.Extensions.Options;

    public class MemberService : IMemberService
    {
        private readonly IAppLogger _logger;
        private readonly MemberServiceClient _client;
        private readonly IOptions<SoapGateways> _options;

        public MemberService(IOptions<SoapGateways> options, IAppLogger logger)
        {
            _options = options;
            _logger = logger;
            _client = new MemberServiceClient(
                new BasicHttpBinding
                {
                    MaxBufferSize = int.MaxValue,
                    ReaderQuotas = XmlDictionaryReaderQuotas.Max,
                    MaxReceivedMessageSize = int.MaxValue,
                    AllowCookies = true
                },
                new EndpointAddress(_options.Value.MemberServiceEndpoint));
        }

        public Task<OTAuthentication> AddListOfMembersToGroupAsync(OTAuthentication OTAuthentication, long groupID, long[] memberIDs)
        {
            var inValue = new AddListOfMembersToGroupRequest
            {
                OTAuthentication = OTAuthentication,
                groupID = groupID,
                memberIDs = memberIDs
            };
            return _client.AddListOfMembersToGroupAsync(inValue).ContinueWith(x => x.Result.OTAuthentication);
        }

        public Task<OTAuthentication> AddMemberToGroupAsync(OTAuthentication OTAuthentication, long groupID, long memberID)
        {
            var inValue = new AddMemberToGroupRequest
            {
                OTAuthentication = OTAuthentication,
                groupID = groupID,
                memberID = memberID
            };
            return _client.AddMemberToGroupAsync(inValue).ContinueWith(x => x.Result.OTAuthentication);
        }

        public Task<OTAuthentication> ConvertExternalUserToUserAsync(OTAuthentication OTAuthentication, long userID, long groupID, MemberPrivileges memberPrivileges)
        {
            var inValue = new ConvertExternalUserToUserRequest
            {
                OTAuthentication = OTAuthentication,
                userID = userID,
                groupID = groupID,
                memberPrivileges = memberPrivileges
            };
            return _client.ConvertExternalUserToUserAsync(inValue).ContinueWith(x => x.Result.OTAuthentication);
        }

        public Task<SoapResult<long>> CreateDomainAsync(OTAuthentication OTAuthentication, string name)
        {
            var inValue = new CreateDomainRequest
            {
                OTAuthentication = OTAuthentication,
                name = name
            };
            return _client.CreateDomainAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.CreateDomainResult));
        }

        public Task<SoapResult<long>> CreateExternalGroupAsync(OTAuthentication OTAuthentication, string name, long? leaderID)
        {
            var inValue = new CreateExternalGroupRequest
            {
                OTAuthentication = OTAuthentication,
                name = name,
                leaderID = leaderID
            };
            return _client.CreateExternalGroupAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.CreateExternalGroupResult));
        }

        public Task<SoapResult<long>> CreateExternalUserAsync(OTAuthentication OTAuthentication, User user)
        {
            var inValue = new CreateExternalUserRequest
            {
                OTAuthentication = OTAuthentication,
                user = user
            };
            return _client.CreateExternalUserAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.CreateExternalUserResult));
        }

        public Task<SoapResult<long>> CreateGroupAsync(OTAuthentication OTAuthentication, string name, long? leaderID)
        {
            var inValue = new CreateGroupRequest
            {
                OTAuthentication = OTAuthentication,
                name = name,
                leaderID = leaderID
            };
            return _client.CreateGroupAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.CreateGroupResult));
        }

        public Task<SoapResult<long>> CreateGroupInDomainAsync(OTAuthentication OTAuthentication, string name, long? leaderID, long domainID)
        {
            var inValue = new CreateGroupInDomainRequest
            {
                OTAuthentication = OTAuthentication,
                name = name,
                leaderID = leaderID,
                domainID = domainID
            };
            return _client.CreateGroupInDomainAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.CreateGroupInDomainResult));
        }

        public Task<SoapResult<Member>> CreateMemberAsync(OTAuthentication OTAuthentication, Member member)
        {
            var inValue = new CreateMemberRequest
            {
                OTAuthentication = OTAuthentication,
                member = member
            };
            return _client.CreateMemberAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.CreateMemberResult));
        }

        public Task<SoapResult<long>> CreateUserAsync(OTAuthentication OTAuthentication, User user)
        {
            var inValue = new CreateUserRequest
            {
                OTAuthentication = OTAuthentication,
                user = user
            };
            return _client.CreateUserAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.CreateUserResult));
        }

        public Task<SoapResult<long>> CreateUserInDomainAsync(OTAuthentication OTAuthentication, User user, long domainID)
        {
            var inValue = new CreateUserInDomainRequest
            {
                OTAuthentication = OTAuthentication,
                user = user,
                domainID = domainID
            };
            return _client.CreateUserInDomainAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.CreateUserInDomainResult));
        }

        public Task<OTAuthentication> DeleteListOfMembersAsync(OTAuthentication OTAuthentication, long[] memberIDs)
        {
            var inValue = new DeleteListOfMembersRequest
            {
                OTAuthentication = OTAuthentication,
                memberIDs = memberIDs
            };
            return _client.DeleteListOfMembersAsync(inValue).ContinueWith(x => x.Result.OTAuthentication);
        }

        public Task<OTAuthentication> DeleteMemberAsync(OTAuthentication OTAuthentication, long memberID)
        {
            var inValue = new DeleteMemberRequest
            {
                OTAuthentication = OTAuthentication,
                memberID = memberID
            };
            return _client.DeleteMemberAsync(inValue).ContinueWith(x => x.Result.OTAuthentication);
        }

        public Task<SoapResult<User>> GetAuthenticatedUserAsync(OTAuthentication OTAuthentication)
        {
            var inValue = new GetAuthenticatedUserRequest
            {
                OTAuthentication = OTAuthentication
            };
            return _client.GetAuthenticatedUserAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.GetAuthenticatedUserResult));
        }

        public Task<SoapResult<Group>> GetGroupByNameAsync(OTAuthentication OTAuthentication, string name)
        {
            var inValue = new GetGroupByNameRequest
            {
                OTAuthentication = OTAuthentication,
                name = name
            };
            return _client.GetGroupByNameAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.GetGroupByNameResult));
        }

        public Task<SoapResult<Group>> GetGroupByNameInDomainAsync(OTAuthentication OTAuthentication, string name, long domainID)
        {
            var inValue = new GetGroupByNameInDomainRequest
            {
                OTAuthentication = OTAuthentication,
                name = name,
                domainID = domainID
            };
            return _client.GetGroupByNameInDomainAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.GetGroupByNameInDomainResult));
        }

        public Task<SoapResult<Member>> GetMemberByIdAsync(OTAuthentication OTAuthentication, long memberID)
        {
            var inValue = new GetMemberByIdRequest
            {
                OTAuthentication = OTAuthentication,
                memberID = memberID
            };
            return _client.GetMemberByIdAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.GetMemberByIdResult));
        }

        public Task<SoapResult<Member>> GetMemberByLoginNameAsync(OTAuthentication OTAuthentication, string loginName)
        {
            var inValue = new GetMemberByLoginNameRequest
            {
                OTAuthentication = OTAuthentication,
                loginName = loginName
            };
            return _client.GetMemberByLoginNameAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.GetMemberByLoginNameResult));
        }

        public Task<SoapResult<Member[]>> GetMembersByIDAsync(OTAuthentication OTAuthentication, long[] memberIDs)
        {
            var inValue = new GetMembersByIDRequest
            {
                OTAuthentication = OTAuthentication,
                memberIDs = memberIDs
            };
            return _client.GetMembersByIDAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.GetMembersByIDResult));
        }

        public Task<SoapResult<string>> GetMetadataLanguageAsync(OTAuthentication OTAuthentication)
        {
            var inValue = new GetMetadataLanguageRequest
            {
                OTAuthentication = OTAuthentication
            };
            return _client.GetMetadataLanguageAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.GetMetadataLanguageResult));
        }

        public Task<SoapResult<MemberSearchResults>> GetSearchResultsAsync(OTAuthentication OTAuthentication, PageHandle pageHandle)
        {
            var inValue = new GetSearchResultsRequest
            {
                OTAuthentication = OTAuthentication,
                pageHandle = pageHandle
            };
            return _client.GetSearchResultsAsync(inValue)                
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.GetSearchResultsResult));
        }

        public Task<SoapResult<User>> GetUserByLoginNameAsync(OTAuthentication OTAuthentication, string loginName)
        {
            var inValue = new GetUserByLoginNameRequest
            {
                OTAuthentication = OTAuthentication,
                loginName = loginName
            };
            return _client.GetUserByLoginNameAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.GetUserByLoginNameResult));
        }

        public Task<SoapResult<User>> GetUserByLoginNameInDomainAsync(OTAuthentication OTAuthentication, string loginName, long domainID)
        {
            var inValue = new GetUserByLoginNameInDomainRequest
            {
                OTAuthentication = OTAuthentication,
                loginName = loginName,
                domainID = domainID
            };
            return _client.GetUserByLoginNameInDomainAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.GetUserByLoginNameInDomainResult));
        }

        public Task<SoapResult<Group[]>> ListMemberOfAsync(OTAuthentication OTAuthentication, long memberID)
        {
            var inValue = new ListMemberOfRequest
            {
                OTAuthentication = OTAuthentication,
                memberID = memberID
            };
            return _client.ListMemberOfAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.ListMemberOfResult));
        }

        public Task<SoapResult<Member[]>> ListMembersAsync(OTAuthentication OTAuthentication, long groupID)
        {
            var inValue = new ListMembersRequest
            {
                OTAuthentication = OTAuthentication,
                groupID = groupID
            };
            return _client.ListMembersAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.ListMembersResult));
        }

        public Task<SoapResult<MemberRight[]>> ListRightsByIDAsync(OTAuthentication OTAuthentication, long id)
        {
            var inValue = new ListRightsByIDRequest
            {
                OTAuthentication = OTAuthentication,
                id = id
            };
            return _client.ListRightsByIDAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.ListRightsByIDResult));
        }

        public Task<OTAuthentication> RemoveListOfMembersFromGroupAsync(OTAuthentication OTAuthentication, long groupID, long[] memberIDs)
        {
            var inValue = new RemoveListOfMembersFromGroupRequest
            {
                OTAuthentication = OTAuthentication,
                groupID = groupID,
                memberIDs = memberIDs
            };
            return _client.RemoveListOfMembersFromGroupAsync(inValue).ContinueWith(x => x.Result.OTAuthentication);
        }

        public Task<OTAuthentication> RemoveMemberFromGroupAsync(OTAuthentication OTAuthentication, long groupID, long memberID)
        {
            var inValue = new RemoveMemberFromGroupRequest
            {
                OTAuthentication = OTAuthentication,
                groupID = groupID,
                memberID = memberID
            };
            return _client.RemoveMemberFromGroupAsync(inValue).ContinueWith(x => x.Result.OTAuthentication);
        }

        public Task<SoapResult<PageHandle>> SearchForMembersAsync(OTAuthentication OTAuthentication, MemberSearchOptions options)
        {
            var inValue = new SearchForMembersRequest
            {
                OTAuthentication = OTAuthentication,
                options = options
            };
            return _client.SearchForMembersAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.SearchForMembersResult));
        }

        public Task<OTAuthentication> SetMetadataLanguageAsync(OTAuthentication OTAuthentication, string languageCode)
        {
            var inValue = new SetMetadataLanguageRequest
            {
                OTAuthentication = OTAuthentication,
                languageCode = languageCode
            };
            return _client.SetMetadataLanguageAsync(inValue).ContinueWith(x => x.Result.OTAuthentication);
        }

        public Task<SoapResult<Member>> UpdateMemberAsync(OTAuthentication OTAuthentication, Member member)
        {
            var inValue = new UpdateMemberRequest
            {
                OTAuthentication = OTAuthentication,
                member = member
            };
            return _client.UpdateMemberAsync(inValue)
                .ContinueWith(x => SoapResult.Create(x.Result.OTAuthentication, x.Result.UpdateMemberResult));
        }

        public Task<OTAuthentication> UpdatePasswordAsync(OTAuthentication OTAuthentication, long memberID, string newPassword, string oldPassword)
        {
            var inValue = new UpdatePasswordRequest
            {
                OTAuthentication = OTAuthentication,
                memberID = memberID,
                newPassword = newPassword,
                oldPassword = oldPassword
            };
            return _client.UpdatePasswordAsync(inValue).ContinueWith(x => x.Result.OTAuthentication);
        }
    }
}
