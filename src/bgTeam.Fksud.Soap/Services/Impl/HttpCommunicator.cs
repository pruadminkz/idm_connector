﻿using bgTeam.Fksud.Domain.Dto;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace bgTeam.Fksud.Soap.Services.Impl
{
    public class HttpCommunicator
    {
        private readonly IAppLogger _logger;

        public HttpCommunicator(IAppLogger logger)
        {
            _logger = logger;
        }
        public bool MessageHandler(SendingCsvFile credentials, SendingCsvFileJobDto @params)
        {
            _logger.Info("Start sending a csv file");

            var encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(credentials.UserName + ":" + credentials.Password));
            var fileName = @"C:\TEMP\" + @params.KPIBusinessEntityReportFile.FileName;

            var fileBytes = File.ReadAllBytes(fileName);

            var handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = delegate { return true; };

            var httpClient = new HttpClient(handler);
            httpClient.Timeout = TimeSpan.FromSeconds(15);
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", encoded);

            var form = new MultipartFormDataContent();
            var file = Path.GetFileName(fileName);
            var convertFile = JsonConvert.SerializeObject(@params);

            form.Headers.Add("Content-Disposition", string.Format("attachment; fileName*=UTF-8''{0}", HttpUtility.UrlPathEncode(file).Replace(",", "%2C")));

            var body = new StringContent(convertFile);
            body.Headers.Add("Content-Disposition", "form-data; name=\"body\"");
            form.Add(body, "body");

            var fileContent = new ByteArrayContent(fileBytes, 0, fileBytes.Length);
            fileContent.Headers.Add("Content-Disposition", "form-data; name=\"file\"; filename=\"" + fileName + "\"");
            fileContent.Headers.Add("Content-Type", "text/csv");
            form.Add(fileContent, "file", file);

            string baseURL = credentials.Url;

            try
            {
                var response = httpClient.PostAsync(baseURL, form).Result;

                response.EnsureSuccessStatusCode();

                _logger.Info(response.StatusCode.ToString());
                httpClient.Dispose();
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw e;
            }
            finally
            {
                //File.Delete(fileName);
            }

            _logger.Info("File sent and deleted");

            return true;
        }
    }
}
