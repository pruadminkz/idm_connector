﻿namespace bgTeam.Fksud.Soap.Services.Impl
{
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.Threading.Tasks;
    using bgTeam.Fksud.Soap.Messages;

    internal class AuthenticationClient : ClientBase<IAuthenticationClient>, IAuthenticationClient
    {
        public AuthenticationClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        public Task<AuthenticateApplicationResponse> AuthenticateApplicationAsync(AuthenticateApplicationRequest request)
        {
            return Channel.AuthenticateApplicationAsync(request);
        }

        public Task<string> AuthenticateUserAsync(string userName, string userPassword)
        {
            return Channel.AuthenticateUserAsync(userName, userPassword);
        }

        public Task<string> AuthenticateUserWithApplicationTokenAsync(string userName, string userPassword, string applicationToken)
        {
            return Channel.AuthenticateUserWithApplicationTokenAsync(userName, userPassword, applicationToken);
        }

        public Task<CombineApplicationTokenResponse> CombineApplicationTokenAsync(CombineApplicationTokenRequest request)
        {
            return Channel.CombineApplicationTokenAsync(request);
        }

        public Task<string> GetOTDSResourceIDAsync()
        {
            return Channel.GetOTDSResourceIDAsync();
        }

        public Task<string> GetOTDSServerAsync()
        {
            return Channel.GetOTDSServerAsync();
        }

        public Task<GetSessionExpirationDateResponse> GetSessionExpirationDateAsync(GetSessionExpirationDateRequest request)
        {
            return Channel.GetSessionExpirationDateAsync(request);
        }

        public Task<ImpersonateApplicationResponse> ImpersonateApplicationAsync(ImpersonateApplicationRequest request)
        {
            return Channel.ImpersonateApplicationAsync(request);
        }

        public Task<ImpersonateUserResponse> ImpersonateUserAsync(ImpersonateUserRequest request)
        {
            return Channel.ImpersonateUserAsync(request);
        }

        public Task<RefreshTokenResponse> RefreshTokenAsync(RefreshTokenRequest request)
        {
            return Channel.RefreshTokenAsync(request);
        }

        public Task<string> ValidateUserAsync(string capToken)
        {
            return Channel.ValidateUserAsync(capToken);
        }
    }
}
