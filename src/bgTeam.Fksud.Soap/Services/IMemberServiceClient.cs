﻿namespace bgTeam.Fksud.Soap.Services
{
    using bgTeam.Fksud.Soap.Dto;
    using bgTeam.Fksud.Soap.Messages;
    using System.ServiceModel;
    using System.Threading.Tasks;

    [ServiceContract(Namespace = "urn:MemberService.service.livelink.opentext.com", ConfigurationName = "MemberService")]
    internal interface IMemberServiceClient
    {

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/AddListOfMembersToGroup", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/AddListOfMembersToGroupResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<AddListOfMembersToGroupResponse> AddListOfMembersToGroupAsync(AddListOfMembersToGroupRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/AddMemberToGroup", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/AddMemberToGroupResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<AddMemberToGroupResponse> AddMemberToGroupAsync(AddMemberToGroupRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/ConvertExternalUserToUser", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/ConvertExternalUserToUserResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<ConvertExternalUserToUserResponse> ConvertExternalUserToUserAsync(ConvertExternalUserToUserRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/CreateDomain", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/CreateDomainResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<CreateDomainResponse> CreateDomainAsync(CreateDomainRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/CreateExternalGroup", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/CreateExternalGroupResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<CreateExternalGroupResponse> CreateExternalGroupAsync(CreateExternalGroupRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/CreateExternalUser", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/CreateExternalUserResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<CreateExternalUserResponse> CreateExternalUserAsync(CreateExternalUserRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/CreateGroup", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/CreateGroupResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<CreateGroupResponse> CreateGroupAsync(CreateGroupRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/CreateGroupInDomain", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/CreateGroupInDomainResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<CreateGroupInDomainResponse> CreateGroupInDomainAsync(CreateGroupInDomainRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/CreateMember", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/CreateMemberResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<CreateMemberResponse> CreateMemberAsync(CreateMemberRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/CreateUser", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/CreateUserResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<CreateUserResponse> CreateUserAsync(CreateUserRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/CreateUserInDomain", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/CreateUserInDomainResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<CreateUserInDomainResponse> CreateUserInDomainAsync(CreateUserInDomainRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/DeleteListOfMembers", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/DeleteListOfMembersResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<DeleteListOfMembersResponse> DeleteListOfMembersAsync(DeleteListOfMembersRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/DeleteMember", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/DeleteMemberResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<DeleteMemberResponse> DeleteMemberAsync(DeleteMemberRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/GetAuthenticatedUser", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/GetAuthenticatedUserResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<GetAuthenticatedUserResponse> GetAuthenticatedUserAsync(GetAuthenticatedUserRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/GetGroupByName", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/GetGroupByNameResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<GetGroupByNameResponse> GetGroupByNameAsync(GetGroupByNameRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/GetGroupByNameInDomain", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/GetGroupByNameInDomainResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<GetGroupByNameInDomainResponse> GetGroupByNameInDomainAsync(GetGroupByNameInDomainRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/GetMemberById", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/GetMemberByIdResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<GetMemberByIdResponse> GetMemberByIdAsync(GetMemberByIdRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/GetMemberByLoginName", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/GetMemberByLoginNameResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<GetMemberByLoginNameResponse> GetMemberByLoginNameAsync(GetMemberByLoginNameRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/GetMembersByID", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/GetMembersByIDResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<GetMembersByIDResponse> GetMembersByIDAsync(GetMembersByIDRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/GetMetadataLanguage", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/GetMetadataLanguageResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<GetMetadataLanguageResponse> GetMetadataLanguageAsync(GetMetadataLanguageRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/GetSearchResults", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/GetSearchResultsResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<GetSearchResultsResponse> GetSearchResultsAsync(GetSearchResultsRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/GetUserByLoginName", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/GetUserByLoginNameResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<GetUserByLoginNameResponse> GetUserByLoginNameAsync(GetUserByLoginNameRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/GetUserByLoginNameInDomain", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/GetUserByLoginNameInDomainResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<GetUserByLoginNameInDomainResponse> GetUserByLoginNameInDomainAsync(GetUserByLoginNameInDomainRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/ListMemberOf", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/ListMemberOfResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<ListMemberOfResponse> ListMemberOfAsync(ListMemberOfRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/ListMembers", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/ListMembersResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<ListMembersResponse> ListMembersAsync(ListMembersRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/ListRightsByID", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/ListRightsByIDResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<ListRightsByIDResponse> ListRightsByIDAsync(ListRightsByIDRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/RemoveListOfMembersFromGroup", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/RemoveListOfMembersFromGroupResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<RemoveListOfMembersFromGroupResponse> RemoveListOfMembersFromGroupAsync(RemoveListOfMembersFromGroupRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/RemoveMemberFromGroup", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/RemoveMemberFromGroupResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<RemoveMemberFromGroupResponse> RemoveMemberFromGroupAsync(RemoveMemberFromGroupRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/SearchForMembers", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/SearchForMembersResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<SearchForMembersResponse> SearchForMembersAsync(SearchForMembersRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/SetMetadataLanguage", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/SetMetadataLanguageResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<SetMetadataLanguageResponse> SetMetadataLanguageAsync(SetMetadataLanguageRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/UpdateMember", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/UpdateMemberResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<UpdateMemberResponse> UpdateMemberAsync(UpdateMemberRequest request);

        [OperationContract(Action = "urn:MemberService.service.livelink.opentext.com/UpdatePassword", ReplyAction = "urn:MemberService.service.livelink.opentext.com/MemberService/UpdatePasswordResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        [ServiceKnownType(typeof(ServiceDataObject1))]
        [ServiceKnownType(typeof(ServiceDataObject))]
        Task<UpdatePasswordResponse> UpdatePasswordAsync(UpdatePasswordRequest request);
    }
}
