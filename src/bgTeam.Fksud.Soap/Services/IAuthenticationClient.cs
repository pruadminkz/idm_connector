﻿namespace bgTeam.Fksud.Soap.Services
{
    using System.ServiceModel;
    using System.Threading.Tasks;
    using bgTeam.Fksud.Soap.Messages;

    [ServiceContract(Namespace = "urn:Core.service.livelink.opentext.com", ConfigurationName = "Authentication")]
    internal interface IAuthenticationClient
    {
        [OperationContract(Action = "urn:Core.service.livelink.opentext.com/AuthenticateApplication", ReplyAction = "urn:Core.service.livelink.opentext.com/Authentication/AuthenticateApplicationResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<AuthenticateApplicationResponse> AuthenticateApplicationAsync(AuthenticateApplicationRequest request);

        [OperationContract(Action = "urn:Core.service.livelink.opentext.com/AuthenticateUser", ReplyAction = "urn:Core.service.livelink.opentext.com/Authentication/AuthenticateUserResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<string> AuthenticateUserAsync(string userName, string userPassword);

        [OperationContract(Action = "urn:Core.service.livelink.opentext.com/AuthenticateUserWithApplicationToken", ReplyAction = "urn:Core.service.livelink.opentext.com/Authentication/AuthenticateUserWithApplicationTokenResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<string> AuthenticateUserWithApplicationTokenAsync(string userName, string userPassword, string applicationToken);

        [OperationContract(Action = "urn:Core.service.livelink.opentext.com/CombineApplicationToken", ReplyAction = "urn:Core.service.livelink.opentext.com/Authentication/CombineApplicationTokenResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<CombineApplicationTokenResponse> CombineApplicationTokenAsync(CombineApplicationTokenRequest request);

        [OperationContract(Action = "urn:Core.service.livelink.opentext.com/GetOTDSResourceID", ReplyAction = "urn:Core.service.livelink.opentext.com/Authentication/GetOTDSResourceIDResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<string> GetOTDSResourceIDAsync();

        [OperationContract(Action = "urn:Core.service.livelink.opentext.com/GetOTDSServer", ReplyAction = "urn:Core.service.livelink.opentext.com/Authentication/GetOTDSServerResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<string> GetOTDSServerAsync();

        [OperationContract(Action = "urn:Core.service.livelink.opentext.com/GetSessionExpirationDate", ReplyAction = "urn:Core.service.livelink.opentext.com/Authentication/GetSessionExpirationDateResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<GetSessionExpirationDateResponse> GetSessionExpirationDateAsync(GetSessionExpirationDateRequest request);

        [OperationContract(Action = "urn:Core.service.livelink.opentext.com/ImpersonateApplication", ReplyAction = "urn:Core.service.livelink.opentext.com/Authentication/ImpersonateApplicationResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<ImpersonateApplicationResponse> ImpersonateApplicationAsync(ImpersonateApplicationRequest request);

        [OperationContract(Action = "urn:Core.service.livelink.opentext.com/ImpersonateUser", ReplyAction = "urn:Core.service.livelink.opentext.com/Authentication/ImpersonateUserResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<ImpersonateUserResponse> ImpersonateUserAsync(ImpersonateUserRequest request);

        [OperationContract(Action = "urn:Core.service.livelink.opentext.com/RefreshToken", ReplyAction = "urn:Core.service.livelink.opentext.com/Authentication/RefreshTokenResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<RefreshTokenResponse> RefreshTokenAsync(RefreshTokenRequest request);

        [OperationContract(Action = "urn:Core.service.livelink.opentext.com/ValidateUser", ReplyAction = "urn:Core.service.livelink.opentext.com/Authentication/ValidateUserResponse")]
        [XmlSerializerFormat(SupportFaults = true)]
        Task<string> ValidateUserAsync(string capToken);
    }
}
