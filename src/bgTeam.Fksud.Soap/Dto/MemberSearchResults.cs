﻿namespace bgTeam.Fksud.Soap.Dto
{
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlType(Namespace = "urn:MemberService.service.livelink.opentext.com")]
    public class MemberSearchResults : ServiceDataObject
    {
        /// <remarks/>
        [XmlElement("Members", Order = 0)]
        public Member[] Members { get; set; }

        /// <remarks/>
        [XmlElement(Order = 1)]
        public PageHandle PageHandle { get; set; }
    }
}
