﻿namespace bgTeam.Fksud.Soap.Dto
{
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlInclude(typeof(Group))]
    [XmlInclude(typeof(Domain))]
    [XmlInclude(typeof(User))]
    [XmlType(Namespace = "urn:MemberService.service.livelink.opentext.com")]
    public class Member : ServiceDataObject
    {
        /// <remarks/>
        [XmlElement(Order = 0)]
        public long CreatedBy { get; set; }

        /// <remarks/>
        [XmlElement(Order = 1)]
        public bool Deleted { get; set; }

        /// <remarks/>
        [XmlElement(Order = 2)]
        public string DisplayName { get; set; }

        /// <remarks/>
        [XmlElement(Order = 3)]
        public long ID { get; set; }

        /// <remarks/>
        [XmlElement(Order = 4)]
        public string Name { get; set; }

        /// <remarks/>
        [XmlElement(Order = 5)]
        public string Type { get; set; }
    }
}
