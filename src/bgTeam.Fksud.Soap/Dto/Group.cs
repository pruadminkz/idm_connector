﻿namespace bgTeam.Fksud.Soap.Dto
{
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlType(Namespace = "urn:MemberService.service.livelink.opentext.com")]
    public class Group : Member
    {
        /// <remarks/>
        [XmlElement(IsNullable = true, Order = 0)]
        public long? LeaderID { get; set; }
    }
}
