﻿namespace bgTeam.Fksud.Soap.Dto
{
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlType(Namespace = "urn:MemberService.service.livelink.opentext.com")]
    public class MemberPrivileges : ServiceDataObject
    {
        /// <remarks/>
        [XmlElement(Order = 0)]
        public bool CanAdministerSystem { get; set; }

        /// <remarks/>
        [XmlElement(Order = 1)]
        public bool CanAdministerUsers { get; set; }

        /// <remarks/>
        [XmlElement(Order = 2)]
        public bool CreateUpdateGroups { get; set; }

        /// <remarks/>
        [XmlElement(Order = 3)]
        public bool CreateUpdateUsers { get; set; }

        /// <remarks/>
        [XmlElement(Order = 4)]
        public bool LoginEnabled { get; set; }

        /// <remarks/>
        [XmlElement(Order = 5)]
        public bool PublicAccessEnabled { get; set; }
    }
}
