﻿namespace bgTeam.Fksud.Soap.Dto
{
    using System;

    public class SoapGateways
    {
        public Uri MegaWcfEndpoint { get; set; }

        public Uri AuthenticationEndpoint { get; set; }

        public Uri MemberServiceEndpoint { get; set; }
    }
}
