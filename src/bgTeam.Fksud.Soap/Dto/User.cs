﻿namespace bgTeam.Fksud.Soap.Dto
{
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlType(Namespace = "urn:MemberService.service.livelink.opentext.com")]
    public class User : Member
    {
        /// <remarks/>
        [XmlElement(Order = 0)]
        public long DepartmentGroupID { get; set; }

        /// <remarks/>
        [XmlElement(Order = 1)]
        public string Email { get; set; }

        /// <remarks/>
        [XmlElement(Order = 2)]
        public string Fax { get; set; }

        /// <remarks/>
        [XmlElement(Order = 3)]
        public string FirstName { get; set; }

        /// <remarks/>
        [XmlElement(Order = 4)]
        public string LastName { get; set; }

        /// <remarks/>
        [XmlElement(Order = 5)]
        public string MiddleName { get; set; }

        /// <remarks/>
        [XmlElement(Order = 6)]
        public string OfficeLocation { get; set; }

        /// <remarks/>
        [XmlElement(Order = 7)]
        public string Password { get; set; }

        /// <remarks/>
        [XmlElement(Order = 8)]
        public string Phone { get; set; }

        /// <remarks/>
        [XmlElement(Order = 9)]
        public MemberPrivileges Privileges { get; set; }

        /// <remarks/>
        [XmlElement(IsNullable = true, Order = 10)]
        public int? TimeZone { get; set; }

        /// <remarks/>
        [XmlElement(Order = 11)]
        public string Title { get; set; }
    }
}
