﻿namespace bgTeam.Fksud.Soap.Dto
{
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlType(Namespace = "urn:MemberService.service.livelink.opentext.com")]
    public class MemberRight : ServiceDataObject
    {
        /// <remarks/>
        [XmlElement(Order = 0)]
        public long ID { get; set; }

        /// <remarks/>
        [XmlElement(IsNullable = true, Order = 1)]
        public long? LeaderID { get; set; }

        /// <remarks/>
        [XmlElement(IsNullable = true, Order = 2)]
        public string Name { get; set; }

        /// <remarks/>
        [XmlElement(IsNullable = true, Order = 3)]
        public string Type { get; set; }
    }
}
