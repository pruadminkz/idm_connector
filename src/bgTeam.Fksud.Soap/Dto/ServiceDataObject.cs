﻿namespace bgTeam.Fksud.Soap.Dto
{
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlInclude(typeof(MemberSearchOptions))]
    [XmlInclude(typeof(MemberRight))]
    [XmlInclude(typeof(MemberSearchResults))]
    [XmlInclude(typeof(Member))]
    [XmlInclude(typeof(Group))]
    [XmlInclude(typeof(Domain))]
    [XmlInclude(typeof(User))]
    [XmlInclude(typeof(MemberPrivileges))]
    [XmlType(Namespace = "urn:MemberService.service.livelink.opentext.com")]
    public abstract class ServiceDataObject
    {
    }
}
