﻿namespace bgTeam.Fksud.Soap.Dto
{
    using bgTeam.Fksud.Soap.Enums;
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlType(Namespace = "urn:MemberService.service.livelink.opentext.com")]
    public class MemberSearchOptions : ServiceDataObject
    {
        /// <remarks/>
        [XmlElement(Order = 0)]
        public SearchColumn Column { get; set; }

        /// <remarks/>
        [XmlElement(Order = 1)]
        public long DomainID { get; set; }

        /// <remarks/>
        [XmlElement(Order = 2)]
        public SearchFilter Filter { get; set; }

        /// <remarks/>
        [XmlElement(Order = 3)]
        public long GroupID { get; set; }

        /// <remarks/>
        [XmlElement(Order = 4)]
        public SearchMatching Matching { get; set; }

        /// <remarks/>
        [XmlElement(Order = 5)]
        public int PageSize { get; set; }

        /// <remarks/>
        [XmlElement(Order = 6)]
        public SearchScope Scope { get; set; }

        /// <remarks/>
        [XmlElement(Order = 7)]
        public string Search { get; set; }
    }
}
