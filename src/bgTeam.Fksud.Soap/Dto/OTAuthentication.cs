﻿namespace bgTeam.Fksud.Soap.Dto
{
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlType(Namespace = "urn:api.ecm.opentext.com")]
    public class OTAuthentication
    {
        /// <remarks/>
        [XmlElement(Order = 0)]
        public string AuthenticationToken { get; set; }

        public static implicit operator OTAuthentication(string value)
        {
            return new OTAuthentication
            {
                AuthenticationToken = value
            };
        }
    }
}
