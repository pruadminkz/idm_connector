﻿namespace bgTeam.Fksud.Soap.Dto
{
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlInclude(typeof(PageHandle))]
    [XmlType(TypeName = "ServiceDataObject", Namespace = "urn:Core.service.livelink.opentext.com")]
    public abstract class ServiceDataObject1
    {
    }
}
