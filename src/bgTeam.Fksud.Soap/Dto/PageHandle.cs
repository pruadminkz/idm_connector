﻿namespace bgTeam.Fksud.Soap.Dto
{
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlType(Namespace = "urn:Core.service.livelink.opentext.com")]
    public class PageHandle : ServiceDataObject1
    {

        /// <remarks/>
        [XmlElement(Order = 0)]
        public bool FinalPage { get; set; }

        /// <remarks/>
        [XmlElement(IsNullable = true, Order = 1)]
        public int? NumberOfPages { get; set; }

        /// <remarks/>
        [XmlElement(IsNullable = true, Order = 2)]
        public int? PageHandleID { get; set; }

        /// <remarks/>
        [XmlElement(IsNullable = true, Order = 3)]
        public int? PageNumber { get; set; }

        /// <remarks/>
        [XmlElement(Order = 4)]
        public int PageSize { get; set; }
    }
}
