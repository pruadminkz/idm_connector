﻿namespace bgTeam.Fksud.Soap.Dto
{
    public class SoapResult<T>
    {
        public OTAuthentication OTAuthentication { get; set; }

        public T Result { get; set; }

        public SoapResult(OTAuthentication oTAuthentication, T result)
        {
            OTAuthentication = oTAuthentication;
            Result = result;
        }
    }
}
