﻿namespace bgTeam.Fksud.Soap.Enums
{
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlType(Namespace = "urn:MemberService.service.livelink.opentext.com")]
    public enum SearchMatching
    {

        /// <remarks/>
        CONTAINS,

        /// <remarks/>
        ENDSWITH,

        /// <remarks/>
        SOUNDSLIKE,

        /// <remarks/>
        STARTSWITH,
    }
}
