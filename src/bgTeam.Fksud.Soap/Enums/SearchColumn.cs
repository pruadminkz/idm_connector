﻿namespace bgTeam.Fksud.Soap.Enums
{
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlType(Namespace = "urn:MemberService.service.livelink.opentext.com")]
    public enum SearchColumn
    {

        /// <remarks/>
        FIRSTNAME,

        /// <remarks/>
        LASTNAME,

        /// <remarks/>
        MAILADDRESS,

        /// <remarks/>
        NAME,
    }
}
