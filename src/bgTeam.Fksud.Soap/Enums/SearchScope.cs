﻿namespace bgTeam.Fksud.Soap.Enums
{
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlType(Namespace = "urn:MemberService.service.livelink.opentext.com")]
    public enum SearchScope
    {

        /// <remarks/>
        GROUP,

        /// <remarks/>
        SYSTEM,
    }
}
