﻿namespace bgTeam.Fksud.Soap.Enums
{
    using System.Xml.Serialization;

    /// <remarks/>
    [XmlType(Namespace = "urn:MemberService.service.livelink.opentext.com")]
    public enum SearchFilter
    {

        /// <remarks/>
        ANY,

        /// <remarks/>
        DOMAIN,

        /// <remarks/>
        GROUP,

        /// <remarks/>
        USER,
    }
}
