﻿namespace bgTeam.Fksud.Domain.Entities
{
    public class Kuaf : IEntityName
    {
        public long? SysId { get; set; }

        public string Name { get; set; }
    }
}
