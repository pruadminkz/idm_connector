﻿namespace bgTeam.Fksud.Domain.Entities
{

    public class UserTemp
    {
        public string Id { get; set; }

        public long CSId { get; set; }

        public string Name { get; set; }
    }
}
