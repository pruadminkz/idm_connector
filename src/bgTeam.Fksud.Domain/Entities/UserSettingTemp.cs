﻿namespace bgTeam.Fksud.Domain.Entities
{
    public class UserSettingTemp
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public long? UserPrivileges { get; set; }

        public long? SettingsNum { get; set; }
    }
}
