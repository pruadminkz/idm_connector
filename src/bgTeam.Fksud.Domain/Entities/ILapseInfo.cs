﻿namespace bgTeam.Fksud.Domain.Entities
{
    using System;

    public interface ILapseInfo
    {
        string Division { get; }

        string Client { get; }

        DateTime ReportingPeriod { get; }

        string Employee { get; }

        int ID { get; }

        string Status { get; }

        string DocumentPackageType { get; }

        DateTime StartDate { get; }

        DateTime EndDate { get; }

        string ProcessStep { get; }

        DateTime ProcessStepStartDate { get; }

        DateTime ProcessStepPlanDate { get; }

        DateTime ProcessStepFactDate { get; }

        string ProcessStepStatus { get; }

        string Executor { get; }

        string TransferToArchive { get; }

        string SLA { get; }

        string ReturnReason { get; }

        string LinkedDocSAPS4 { get; }

        DateTime DateOfDocumentReceipt { get; }

        string FI_HRAttribute { get; }

        string Comment { get; }

        string LinkedPackages { get; }

        string IsPackageLinked { get; }

        DateTime DocumentPostingDate { get; }

        string DocNumSAPS4 { get; }

        int SYSTEM_ID { get; }

        DateTime WorkAudit_Date { get; }
    }
}