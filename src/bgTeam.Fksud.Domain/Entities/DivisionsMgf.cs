﻿namespace bgTeam.Fksud.Domain.Entities
{

    public class DivisionsMgf : IDivisions, IEntityName, IEntityDivision
    {
        public int SYSTEM_ID { get; set; }

        public int SAP_ID { get { return SYSTEM_ID; } }

        public int PARENT_ID { get; set; }

        public int ROOT_DIVISION_ID { get; set; }

        public string DIVISION_NAME { get; set; }

        public string GROUP_NAME { get; set; }

        public long? SysId { get { return SYSTEM_ID; } set { } }

        public string Name { get { return GROUP_NAME; } set { } }

        public string DivisionName { get { return DIVISION_NAME; } set { DIVISION_NAME = value; } }

        public int Organization { get; set; }
    }
}
