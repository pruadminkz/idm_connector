﻿namespace bgTeam.Fksud.Domain.Entities
{
    public interface IEntityDivision : IEntity
    {
        string DivisionName { get; set; }
    }
}
