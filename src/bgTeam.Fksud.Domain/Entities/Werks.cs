﻿namespace bgTeam.Fksud.Domain.Entities
{
    public class Werks : IWerks, IEntityWerks
    {
        public string Werks_code { get; set; }

        public string Filial { get; set; }

        public string Filial_ID { get; set; }

        public string Location { get; set; }

        public string oldLocation { get; set; }

        public string Organization_ID { get; set; }

        public string Parent_ID { get; set; }
    }
}
