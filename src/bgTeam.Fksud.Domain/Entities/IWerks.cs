﻿namespace bgTeam.Fksud.Domain.Entities
{
    public interface IWerks
    {
        string Werks_code { get; }

        string Filial { get; }

        string Filial_ID { get; }

        string Location { get; }

        string oldLocation { get; }

        string Organization_ID { get; }

        string Parent_ID { get; }

    }
}
