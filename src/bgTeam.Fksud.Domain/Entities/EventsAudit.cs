﻿namespace bgTeam.Fksud.Domain.Entities
{
    public class EventsAudit : IEventsAudit
    {
        public string OBJECT { get; set; }

        public string DATETIME { get; set; }

        public string SUBJECT { get; set; }

        public string ACTION { get; set; }

        public string OLDVALUE { get; set; }

        public string NEWVALUE { get; set; }
    }
}
