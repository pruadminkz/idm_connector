﻿namespace bgTeam.Fksud.Domain.Entities
{
    public interface IDivisions
    {
        int SAP_ID { get; }

        int SYSTEM_ID { get; }

        int PARENT_ID { get; }

        int ROOT_DIVISION_ID { get; }

        string DIVISION_NAME { get; set; }

        int Organization { get; }
    }
}
