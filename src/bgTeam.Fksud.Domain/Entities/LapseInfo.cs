﻿namespace bgTeam.Fksud.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class LapseInfo : ILapseInfo
    {
        /// <summary>
        /// Дивизион.
        /// </summary>
        public string Division { get; set; }

        /// <summary>
        /// Клиент(БЕ).
        /// </summary>
        public string Client { get; set; }

        /// <summary>
        /// Отчетная дата.
        /// </summary>
        public DateTime ReportingPeriod { get; set; }

        /// <summary>
        /// Сотрудник.
        /// </summary>
        public string Employee { get; set; }

        /// <summary>
        /// Номер.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Статус.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Тип пакета документов.
        /// </summary>
        public string DocumentPackageType { get; set; }

        /// <summary>
        /// Дата начала процесса.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Дата окон.процесса.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Шаг процесса.
        /// </summary>
        public string ProcessStep { get; set; }

        /// <summary>
        /// Дата начала шага процесса.
        /// </summary>
        public DateTime ProcessStepStartDate { get; set; }

        /// <summary>
        /// Плановая дата завершения шага процесса.
        /// </summary>
        public DateTime ProcessStepPlanDate { get; set; }

        /// <summary>
        /// Фактическая дата завершения шага процесса.
        /// </summary>
        public DateTime ProcessStepFactDate { get; set; }

        /// <summary>
        /// Статус шага.
        /// </summary>
        public string ProcessStepStatus { get; set; }

        /// <summary>
        /// Исполнитель шага процесса.
        /// </summary>
        public string Executor { get; set; }

        /// <summary>
        /// Признак передачи в архив БЕ.
        /// </summary>
        public string TransferToArchive { get; set; }

        /// <summary>
        /// Соблюдение SLA.
        /// </summary>
        public string SLA { get; set; }

        /// <summary>
        /// Причина возврата.
        /// </summary>
        public string ReturnReason { get; set; }

        /// <summary>
        /// Связанный объект в SAP.
        /// </summary>
        public string LinkedDocSAPS4 { get; set; }

        /// <summary>
        /// Дата поступления документа.
        /// </summary>
        public DateTime DateOfDocumentReceipt { get; set; }

        /// <summary>
        /// Признак FI/HR.
        /// </summary>
        public string FI_HRAttribute { get; set; }

        /// <summary>
        /// Комментарий.
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Связанные карточки.
        /// </summary>
        public string LinkedPackages { get; set; }

        /// <summary>
        /// Признак пакетного связывания.
        /// </summary>
        public string IsPackageLinked { get; set; }

        /// <summary>
        /// Дата размещения документов.
        /// </summary>
        public DateTime DocumentPostingDate { get; set; }

        /// <summary>
        /// Номер документа в SAP.
        /// </summary>
        public string DocNumSAPS4 { get; set; }

        /// <summary>
        /// Уникальный ID.
        /// </summary>
        public int SYSTEM_ID { get; set; }

        /// <summary>
        /// Дата аудита.
        /// </summary>
        public DateTime WorkAudit_Date { get; set; }
    }
}
