﻿namespace bgTeam.Fksud.Domain.Entities
{

    public class KuafDivisions : IEntityName
    {
        public long? SysId { get; set; }

        public int KuafId { get; set; }

        public string Name { get; set; }
    }
}
