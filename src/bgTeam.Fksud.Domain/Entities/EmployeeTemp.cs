﻿namespace bgTeam.Fksud.Domain.Entities
{

    public class EmployeeTemp
    {
        public long ID { get; set; }

        public long GROUPID { get; set; }

        public long DIVISION_ID { get; set; }

        public string NAME { get; set; }
    }
}
