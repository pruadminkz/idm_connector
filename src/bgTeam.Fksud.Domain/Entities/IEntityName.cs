﻿namespace bgTeam.Fksud.Domain.Entities
{
    public interface IEntityName : IEntity
    {
        string Name { get; set; }
    }
}
