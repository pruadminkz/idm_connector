﻿namespace bgTeam.Fksud.Domain.Entities
{
    public interface IEventsAudit
    {
        string OBJECT { get; }

        string DATETIME { get; }

        string SUBJECT { get; }

        string ACTION { get; }

        string OLDVALUE { get; }

        string NEWVALUE { get; }
    }
}