﻿namespace bgTeam.Fksud.Domain.Entities
{
    public class Divisions : IDivisions, IEntityDivision
    {
        public int SYSTEM_ID { get; set; }

        public int SAP_ID { get; set; }

        public int PARENT_ID { get; set; }

        public int ROOT_DIVISION_ID { get; set; }

        public int GROUP_DIVISION_ID { get; set; }

        public string DIVISION_NAME { get; set; }

        public long? SysId { get { return SYSTEM_ID; } set { SYSTEM_ID = (int)value; } }

        public string DivisionName { get { return DIVISION_NAME; } set { DIVISION_NAME = value; } }

        public int Organization { get; internal set; }
    }
}
