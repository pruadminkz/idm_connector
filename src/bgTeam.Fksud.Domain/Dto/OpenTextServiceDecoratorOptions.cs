﻿namespace bgTeam.Fksud.Domain.Dto
{
    public class OpenTextServiceDecoratorOptions
    {
        public bool UseQueue { get; set; }

        public string QueueSection { get; set; }

        public string[] RequredWorkgroups { get; set; }
    }
}
