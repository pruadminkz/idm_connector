﻿namespace bgTeam.Fksud.Domain.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    /// <summary>
    /// Параметры для авторизации в api.
    /// </summary>
    public class AuthorizationDto
    {
        /// <summary>
        /// Имя пользователя.
        /// </summary>
        [Required]
        [JsonProperty("userName")]
        public string UserName { get; set; }

        /// <summary>
        /// Рабочая группа в которую будет добавлен аккаунт.
        /// </summary>
        [Required]
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
