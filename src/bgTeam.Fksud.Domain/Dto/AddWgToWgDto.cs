﻿namespace bgTeam.Fksud.Domain.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    /// <summary>
    /// Параметры для связки рабочей группы с другой рабочей группой.
    /// </summary>
    public class AddWgToWgDto
    {
        /// <summary>
        /// Рабочая группа, которая будет включена в другую.
        /// </summary>
        [Required]
        [JsonProperty("srcWorkgroup")]
        public WorkgroupDto SourceWorkgroup { get; set; }

        /// <summary>
        /// Рабочая группа в которую будет включена первая группа.
        /// </summary>
        [Required]
        [JsonProperty("targWorkgroup")]
        public WorkgroupDto TargetWorkgroup { get; set; }

        /// <summary>
        /// Счетчик попыток повторной обработки сообщения.
        /// </summary>
        public int RetryCount { get; set; } = 0;
    }
}
