﻿namespace bgTeam.Fksud.Domain.Dto
{
    public class SendingCsvFile
    {
        /// <summary>
        /// Имя пользователя.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Путь отправления файла.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// SQL-запрос.
        /// </summary>
        public string CustomSqlString { get; set; }
    }
}
