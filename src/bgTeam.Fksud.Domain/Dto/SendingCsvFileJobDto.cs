﻿namespace bgTeam.Fksud.Domain.Dto
{
    using Newtonsoft.Json;
    using System.ComponentModel.DataAnnotations;

    public class SendingCsvFileJobDto
    {
        /// <summary>
        /// Информация промежутка времени.
        /// </summary>
        [Required]
        [JsonProperty("MessageHeader")]
        public MessageHeader MessageHeader { get; set; }

        /// <summary>
        /// Информация промежутка времени.
        /// </summary>
        [Required]
        [JsonProperty("KPIBusinessEntityReportFile")]
        public KPIBusinessEntityReportFile KPIBusinessEntityReportFile { get; set; }
    }
}
