﻿namespace bgTeam.Fksud.Domain.Dto
{
    using System;
    using System.Collections.Generic;
    using bgTeam.Fksud.Domain.Entities;

    /// <summary>
    /// Результат запроса аудита.
    /// </summary>
    public class AuditDto
    {
        /// <summary>
        /// имя сервера.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Название системы.
        /// </summary>
        public string System { get; set; }

        /// <summary>
        /// Последнее событие.
        /// </summary>
        public string LastEvent { get; set; }

        /// <summary>
        /// Аудит.
        /// </summary>
        public List<EventsAudit> Events { get; set; }
    }
}