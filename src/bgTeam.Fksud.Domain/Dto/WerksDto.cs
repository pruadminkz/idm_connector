﻿namespace bgTeam.Fksud.Domain.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    /// <summary>
    /// Территориальное деление.
    /// </summary>
    public class WerksDto
    {
        /// <summary>
        /// "Старый" код филиала (для обратной совместимости со структурой данных СВЭДО.
        /// </summary>
        [Required]
        public string Filial_ID { get; set; }

        /// <summary>
        /// Код территориального отделения из SAP HCM.
        /// </summary>
        [Required]
        public string Werks { get; set; }

        /// <summary>
        /// Наименование филиала.
        /// </summary>
        [Required]
        public string Filial { get; set; }

        /// <summary>
        /// Наименование регионального отделения.
        /// </summary>
        public string Location { get; set; }
                
        /// <summary>
        /// Наименование регионального отделения.
        /// </summary>
        public string oldLocation { get; set; }

        /// <summary>
        /// Код организации.
        /// </summary>
        public string Organization_ID { get; set; }

        /// <summary>
        /// Код родительского филиала (совпадает с кодом организации).
        /// </summary>
        public string Parent_ID { get; set; }
    }
}
