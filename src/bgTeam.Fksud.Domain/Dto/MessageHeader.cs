﻿namespace bgTeam.Fksud.Domain.Dto
{
    using Newtonsoft.Json;
    using System;
    using System.ComponentModel.DataAnnotations;

    public class MessageHeader
    {
        public MessageHeader(string creationDateTime, string id)
        {
            CreationDateTime = creationDateTime;
            ID = id;
        }

        /// <summary>
        /// Дата создания файла.
        /// </summary>
        [Required]
        [JsonProperty("CreationDateTime")]
        public string CreationDateTime { get; set; }

        /// <summary>
        /// ID.
        /// </summary>
        [Required]
        [JsonProperty("ID")]
        public string ID { get; set; }
    }
}
