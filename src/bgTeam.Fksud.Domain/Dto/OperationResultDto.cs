﻿namespace bgTeam.Fksud.Domain.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    /// <summary>
    /// Унифицированный результат запроса.
    /// </summary>
    public class OperationResultDto
    {
        /// <summary>
        /// Операция выполнена успешно.
        /// </summary>
        public static readonly OperationResultDto Success = new OperationResultDto
        {
            Result = true,
            Error = string.Empty,
        };

        /// <summary>
        /// Нет прав на выполнение операции.
        /// </summary>
        public static readonly OperationResultDto Unauthorized = new OperationResultDto
        {
            Error = "Unauthorized",
            Result = false,
        };

        /// <summary>
        /// Указан неверный логин для запроса UpdateAccount.
        /// </summary>
        public static readonly OperationResultDto BadRequestUpdateAccount = new OperationResultDto
        {
            Error = "The Username does not match the current value.",
            Result = false,
        };

        /// <summary>
        /// Флаг успешности операции.
        /// </summary>
        [Required]
        [JsonProperty("result")]
        public bool Result { get; set; }

        /// <summary>
        /// Сообщение об ошибке.
        /// </summary>
        [JsonProperty("errMsg")]
        public string Error { get; set; }
    }
}
