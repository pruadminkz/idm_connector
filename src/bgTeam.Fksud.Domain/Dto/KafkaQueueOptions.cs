﻿namespace bgTeam.Fksud.Domain.Dto
{
    public class KafkaQueueOptions
    {
        public string Hosts { get; set; }

        public string Topic { get; set; }

        public string GroupId { get; set; }
    }
}
