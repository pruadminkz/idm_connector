﻿namespace bgTeam.Fksud.Domain.Dto
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Аудит.
    /// </summary>
    public class EventsAuditDto
    {
        /// <summary>
        /// Пользователь.
        /// </summary>
        public string Object { get; set; }

        /// <summary>
        /// CONCAT(Дата модификации, Время модификации).
        /// </summary>
        public string Datetime { get; set; }

        /// <summary>
        /// Изменил.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Тип документа изменения.
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// Старое значение.
        /// </summary>
        public string OldValue { get; set; }

        /// <summary>
        /// Новое значение.
        /// </summary>
        public string NewValue { get; set; }

        /// <summary>
        /// Счетчик попыток повторной обработки сообщения.
        /// </summary>
        public int RetryCount { get; set; } = 0;
    }
}
