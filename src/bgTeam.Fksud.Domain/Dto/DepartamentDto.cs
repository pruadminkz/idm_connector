﻿namespace bgTeam.Fksud.Domain.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    /// <summary>
    /// Департамент.
    /// </summary>
    public class DepartamentDto
    {
        /// <summary>
        /// Идентификатор департамента.
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Название департамента.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Идентификатор старшего департамента.
        /// </summary>
        public int ParentId { get; set; }
    }
}
