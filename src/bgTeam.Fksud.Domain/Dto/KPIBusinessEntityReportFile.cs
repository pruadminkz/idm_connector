﻿namespace bgTeam.Fksud.Domain.Dto
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Text;

    public class KPIBusinessEntityReportFile
    {
        public KPIBusinessEntityReportFile(string reportTypeName, string filename)
        {
            ReportTypeName = reportTypeName;
            FileName = filename;
        }

        /// <summary>
        /// ReportTypeName.
        /// </summary>
        [Required]
        [JsonProperty("ReportTypeName")]
        public string ReportTypeName { get; set; }

        /// <summary>
        /// FileName.
        /// </summary>
        [Required]
        [JsonProperty("FileName")]
        public string FileName { get; set; }
    }
}
