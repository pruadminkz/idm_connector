﻿namespace bgTeam.Fksud.Domain.Dto
{
    using System;

    /// <summary>
    /// Результат запроса аккаунтов.
    /// </summary>
    public class LookupAccountsDto
    {
        /// <summary>
        /// Список учетных записей СВЭДО со всеми атрибутами, статусом и списком групп по каждой учетной записи.
        /// </summary>
        public AccountDto[] Accounts { get; set; }

        /// <summary>
        /// Дата создания каждой учетной записи в массиве.
        /// </summary>
        public DateTime[] CreateDate { get; set; }

        /// <summary>
        /// Дата блокировки каждой учетной записи в массиве(если применимо).
        /// </summary>
        public DateTime[] LockDate { get; set; }
    }
}
