﻿namespace bgTeam.Fksud.Domain.Dto
{
    /// <summary>
    /// Данные для авторизации api OpenText.
    /// </summary>
    public class OpenTextApiCredentials
    {
        /// <summary>
        /// Логин.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password { get; set; }
    }
}
