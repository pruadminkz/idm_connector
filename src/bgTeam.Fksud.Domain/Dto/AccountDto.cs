﻿namespace bgTeam.Fksud.Domain.Dto
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    /// <summary>
    /// Аккаунт.
    /// </summary>
    public class AccountDto
    {
        /// <summary>
        /// Логин СВЭДО(уникальный).
        /// </summary>
        [Required]
        [JsonProperty("Username")]
        public string UserName { get; set; }

        /// <summary>
        /// Имя.
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Отчество.
        /// </summary>
        public string PatronymicName { get; set; }

        /// <summary>
        /// E-mail.
        /// </summary>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// NT-логин.
        /// </summary>
        [Required]
        public string NtLogin { get; set; }

        /// <summary>
        /// Заголовок.
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// Телефон - .
        /// </summary>
        public string ObjectSID { get; set; }

        /// <summary>
        /// Комментарий.
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Департамент.
        /// </summary>
        public string DepartmentGroup { get; set; }

        /// <summary>
        /// ID Департамент.
        /// </summary>
        public long DepartmentGroupID { get; set; }

        /// <summary>
        /// Табельный номер.
        /// </summary>
        [Required]
        public string PersonnelNumber { get; set; }

        /// <summary>
        /// Статус.
        /// </summary>
        [Required]
        public bool IsActive { get; set; }

        /// <summary>
        /// Территориальное расположение отдела.
        /// </summary>
        [Required]
        public string OfficeLocation { get; set; }

        /// <summary>
        /// Идентификационный номер пользователя в ОТ.
        /// </summary>
        [Required]
        public long UserID { get; set; }

        /// <summary>
        /// Рабочие группы.
        /// </summary>
        public List<long> WorkgroupsID { get; set; }

        /// <summary>
        /// ID рабочих групп.
        /// </summary>
        public List<string> Workgroups { get; set; }

        /// <summary>
        /// Счетчик попыток повторной обработки сообщения.
        /// </summary>
        public int RetryCount { get; set; } = 0;
    }
}
