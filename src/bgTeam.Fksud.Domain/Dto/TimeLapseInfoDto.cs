﻿namespace bgTeam.Fksud.Domain.Dto
{
    using bgTeam.Fksud.Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// Результат запроса информации в промежутке времени.
    /// </summary>
    public class TimeLapseInfoDto
    {
        /// <summary>
        /// Информация промежутка времени.
        /// </summary>
        public List<LapseInfo> LapseInfo { get; set; }
    }
}
