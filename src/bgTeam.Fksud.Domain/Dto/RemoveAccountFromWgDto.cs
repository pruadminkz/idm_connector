﻿namespace bgTeam.Fksud.Domain.Dto
{
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    /// <summary>
    /// Параметры для удаления связи аккаунта с рабочей группой.
    /// </summary>
    public class RemoveAccountFromWgDto
    {
        /// <summary>
        /// Представление объекта Account из системы со всеми атрибутами и рабочими группами.
        /// </summary>
        [Required]
        [JsonProperty("Account")]
        public AccountDto Account { get; set; }

        /// <summary>
        /// Рабочая группа из которой будет удален аккаунт.
        /// </summary>
        [Required]
        [JsonProperty("targWorkgroup")]
        public WorkgroupDto TargetWorkgroup { get; set; }

        /// <summary>
        /// Счетчик попыток повторной обработки сообщения.
        /// </summary>
        public int RetryCount { get; set; } = 0;
    }
}
