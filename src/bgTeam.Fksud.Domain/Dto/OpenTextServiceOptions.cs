﻿namespace bgTeam.Fksud.Domain.Dto
{
    public class OpenTextServiceOptions
    {
        public string NoSyncValue { get; set; }

        public int PageSize { get; set; }

        public int DegreeOfParallelism { get; set; }
    }
}
