﻿namespace bgTeam.Fksud.Domain.Dto
{
    public class RetryMessage
    {
        public string Method { get; set; }

        public object Params { get; set; }
    }
}
