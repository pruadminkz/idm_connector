﻿namespace bgTeam.Fksud.Domain.Dto
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Рабочая группа.
    /// </summary>
    public class WorkgroupDto
    {
        /// <summary>
        /// Имя рабочей группы(уникальное).
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Описание.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ответственный в рабочей группе(логин).
        /// </summary>
        public string WorkGroupLeader { get; set; }

        /// <summary>
        /// ID ответственного в рабочей группе(логин).
        /// </summary>
        public string WorkGroupLeaderID { get; set; }

        /// <summary>
        /// Идентификатор рабочей группы в ОТ.
        /// </summary>
        public long GroupID { get; set; }

        /// <summary>
        /// Идентификатор родителя рабочей группы в ОТ.
        /// </summary>
        public List<long> ParentGroupID { get; set; }

        /// <summary>
        /// Счетчик попыток повторной обработки сообщения.
        /// </summary>
        public int RetryCount { get; set; } = 0;
    }
}
