﻿namespace bgTeam.Fksud.SchedulerApp.Impl
{
    using System;
    using Quartz;
    using Quartz.Spi;

    public class JobFactory : IJobFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public JobFactory(IServiceProvider serviceProvider)
            => _serviceProvider = serviceProvider;

        /// <inheritdoc/>
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
            => _serviceProvider.GetService(bundle.JobDetail.JobType) as IJob;

        /// <inheritdoc/>
        public void ReturnJob(IJob job)
        {
            // Method intentionally left empty.
        }
    }
}
