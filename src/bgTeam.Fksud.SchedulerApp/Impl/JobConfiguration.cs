﻿namespace bgTeam.Fksud.SchedulerApp.Impl
{
    public class JobConfiguration
    {
        public string Cron { get; set; }

        public bool Enabled { get; set; }

        public int RetryJobCount { get; set; } = 20;
    }
}
