﻿namespace bgTeam.Fksud.SchedulerApp.Impl
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using bgTeam.Fksud.Common;
    using bgTeam.Fksud.Common.Exceptions;
    using bgTeam.Fksud.Domain.Dto;
    using bgTeam.Fksud.Soap.Services;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json.Linq;
    using Quartz;

    [DisallowConcurrentExecution]
    public class RetryQueueJob : IJob
    {
        private readonly IQueueFactory _queueFactory;
        private readonly IOpenTextService _openTextService;
        private readonly IAppLogger _logger;
        private readonly IAuthenticationService _authentication;
        private readonly IOptions<OpenTextApiCredentials> _credentials;

        public RetryQueueJob(
            IAppLogger logger,
            IOptions<OpenTextApiCredentials> credentials,
            IQueueFactory queueFactory,
            IOpenTextService openTextService,
            IAuthenticationService authentication)
        {
            _logger = logger;
            _credentials = credentials;
            _queueFactory = queueFactory;
            _openTextService = openTextService;
            _authentication = authentication;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            _logger.Info($"Starting {nameof(RetryQueueJob)}.");
            using (var queue = _queueFactory.Create<RetryMessage>(nameof(KafkaQueueOptions)))
            {
                var authToken = await _authentication.AuthenticateUserAsync(_credentials.Value.UserName, _credentials.Value.Password);

                var toProduce = new List<RetryMessage>();

                while (true)
                {
                    var message = await queue.Reader.Consume();
                    int messageRetryCount = 0;

                    if (message == null)
                    {
                        break;
                    }
                    try
                    {
                        switch (message.Method)
                        {
                            case nameof(IOpenTextService.AddWg2Wg) when message.Params is JToken token:
                                {
                                    var @params = token.ToObject<AddWgToWgDto>();
                                    @params.RetryCount++;
                                    message.Params = @params;
                                    messageRetryCount = @params.RetryCount;
                                    await _openTextService.AddWg2Wg(authToken, @params);
                                }

                                break;
                            case nameof(IOpenTextService.UpdateAccount) when message.Params is JToken token:
                                {
                                    var @params = token.ToObject<AccountDto>();
                                    @params.RetryCount++;
                                    message.Params = @params;
                                    messageRetryCount = @params.RetryCount;
                                    await _openTextService.UpdateAccount(authToken, @params);
                                }

                                break;
                            case nameof(IOpenTextService.RemoveAccountFromWg) when message.Params is JToken token:
                                {
                                    var @params = token.ToObject<RemoveAccountFromWgDto>();
                                    @params.RetryCount++;
                                    message.Params = @params;
                                    messageRetryCount = @params.RetryCount;
                                    await _openTextService.RemoveAccountFromWg(authToken, @params);
                                }

                                break;
                            case nameof(IOpenTextService.AddAccount2Wg) when message.Params is JToken token:
                                {
                                    var @params = token.ToObject<AddAccountToWgDto>();
                                    @params.RetryCount++;
                                    message.Params = @params;
                                    messageRetryCount = @params.RetryCount;
                                    await _openTextService.AddAccount2Wg(authToken, @params);
                                }

                                break;

                            case nameof(IOpenTextService.CreateWg) when message.Params is JToken token:
                                {
                                    var @params = token.ToObject<WorkgroupDto>();
                                    @params.RetryCount++;
                                    message.Params = @params;
                                    messageRetryCount = @params.RetryCount;
                                    await _openTextService.CreateWg(authToken, @params);
                                }

                                break;
                        }
                    }
                    catch (MemberNotFoundException exp)
                    {
                        _logger.Info("Member not found. Exception: " + exp.Message);
                        switch (message.Method)
                        {
                            case nameof(IOpenTextService.UpdateAccount) when message.Params is JToken token:
                                {
                                    var @params = token.ToObject<AccountDto>();
                                    _logger.Info("UpdateAccount Username param: " + @params.UserName);
                                }
                           break;
                        }
                        _logger.Info($"Messages {message.Method}({message.Params}) requeued.");
                        if (messageRetryCount <= 20)
                        {
                            toProduce.Add(new RetryMessage
                            {
                                Method = message.Method,
                                Params = message.Params,
                            });
                        }
                    }
                }

                _logger.Info($"All messages processed.");

                await queue.Reader.Commit();

                foreach (var item in toProduce)
                {
                    await queue.Writer.Produce(item);
                }
            }
        }
    }
}
