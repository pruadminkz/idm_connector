﻿namespace bgTeam.Fksud.SchedulerApp.Impl
{
    using bgTeam.Fksud.Common;
    using bgTeam.Fksud.Domain.Dto;
    using bgTeam.Fksud.Soap.Services;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json.Linq;
    using Quartz;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    [DisallowConcurrentExecution]
    public class SendingCsvFileJob : IJob
    {
        private readonly IQueueFactory _queueFactory;
        private readonly IOpenTextService _openTextService;
        private readonly IAppLogger _logger;
        private readonly IAuthenticationService _authentication;
        private readonly IOptions<SendingCsvFile> _credentialOptions;

        public SendingCsvFileJob(
            IAppLogger logger,
            IQueueFactory queueFactory,
            IOpenTextService openTextService,
            IAuthenticationService authentication,
            IOptions<SendingCsvFile> credentialOptions)
        {
            _logger = logger;
            _credentialOptions = credentialOptions;
            _queueFactory = queueFactory;
            _openTextService = openTextService;
            _authentication = authentication;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            _logger.Info($"Starting {nameof(SendingCsvFileJob)}.");

            try
            {
                var @params = new SendingCsvFileJobDto();
                @params.MessageHeader = new MessageHeader(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssK"), DateTime.Now.ToString("yyyyMMddHH"));
                @params.KPIBusinessEntityReportFile = new KPIBusinessEntityReportFile("DocumentsPackage", "OT_DocumentsPackage_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".csv");

                _logger.Info($"TimeLapseInfo");
                await _openTextService.TimeLapseInfo(_credentialOptions.Value, @params);
            }
            catch (Exception ex)
            {
                _logger.Error("Data not changed. Exception:" + ex);
            }
        }
    }
}
