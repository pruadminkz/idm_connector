﻿namespace bgTeam.Fksud.SchedulerApp.Impl
{
    using System.Threading.Tasks;
    using bgTeam.Fksud.Common;
    using Quartz;

    [DisallowConcurrentExecution]
    public class UpdateUserSettingsJob : IJob
    {
        private readonly IUserSynchronizeSettingService _userSynchronizeSettingService;

        public UpdateUserSettingsJob(IUserSynchronizeSettingService userSynchronizeSettingService)
        {
            _userSynchronizeSettingService = userSynchronizeSettingService;
        }

        public Task Execute(IJobExecutionContext context)
        {
            return _userSynchronizeSettingService.Synchronize();
        }
    }
}
