﻿namespace bgTeam.Fksud.SchedulerApp.Impl
{
    using System.Threading.Tasks;
    using bgTeam.Fksud.Common;
    using Quartz;

    [DisallowConcurrentExecution]
    public class UpdateUserSettingsLocaleTypeJob : IJob
    {
        private readonly IUserSynchronizeSettingLocaleTypeService _userSynchronizeSettingLocaleTypeService;

        public UpdateUserSettingsLocaleTypeJob(IUserSynchronizeSettingLocaleTypeService userSynchronizeSettingLocaleTypeService)
        {
            _userSynchronizeSettingLocaleTypeService = userSynchronizeSettingLocaleTypeService;
        }

        public Task Execute(IJobExecutionContext context)
        {
            return _userSynchronizeSettingLocaleTypeService.SynchronizeLocaleType();
        }
    }
}
