﻿namespace bgTeam.Fksud.SchedulerApp.Impl
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Hosting;
    using Quartz;
    using Quartz.Spi;

    public class QuartzService : IHostedService
    {
        private readonly IAppLogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IJobFactory _jobFactory;
        private readonly ISchedulerFactory _schedulerFactory;

        private IScheduler _scheduler;

        public QuartzService(IAppLogger logger, IConfiguration configuration, IJobFactory jobFactory, ISchedulerFactory schedulerFactory)
        {
            _logger = logger;
            _configuration = configuration;
            _jobFactory = jobFactory;
            _schedulerFactory = schedulerFactory;
        }

        /// <inheritdoc/>
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.Info($"{nameof(QuartzService)} starting.");

            _scheduler = await _schedulerFactory.GetScheduler(cancellationToken);

            _scheduler.JobFactory = _jobFactory;

            _logger.Info("Scheduling job SendingCsvFileJob.");
            await ScheduleJob<SendingCsvFileJob>(cancellationToken);

            await ScheduleJob<RetryQueueJob>(cancellationToken);

            await ScheduleJob<UpdateUserSettingsJob>(cancellationToken);

            await ScheduleJob<UpdateUserSettingsLocaleTypeJob>(cancellationToken);

            _logger.Info("Starting scheduler...");

            try
            {
                await _scheduler.Start(cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
        }

        /// <inheritdoc/>
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.Info($"{nameof(QuartzService)} stopping.");
            return _scheduler.Shutdown(cancellationToken);
        }

        private Task ScheduleJob<T>(CancellationToken cancellationToken)
            where T : IJob
        {
            var name = typeof(T).Name;

            var configuration = _configuration
                .GetSection(name)
                .Get<JobConfiguration>();

            if (configuration.Enabled)
            {
                var jobDetail = JobBuilder.Create<T>()
                    .WithIdentity(name)
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .ForJob(jobDetail)
                    .WithCronSchedule(configuration.Cron)
                    .WithIdentity(name)
                    .StartNow()
                    .Build();

                return _scheduler.ScheduleJob(jobDetail, trigger, cancellationToken);
            }
            else
            {
                return Task.CompletedTask;
            }
        }
    }
}
