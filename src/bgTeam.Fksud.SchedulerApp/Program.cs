﻿namespace bgTeam.Fksud.SchedulerApp
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using bgTeam.Core;
    using bgTeam.Core.Impl;
    using bgTeam.DataAccess;
    using bgTeam.DataAccess.Impl.Dapper;
    using bgTeam.DataAccess.Impl.SAPHANA;
    using bgTeam.Fksud.Common;
    using bgTeam.Fksud.Common.Impl;
    using bgTeam.Fksud.Domain.Dto;
    using bgTeam.Fksud.SchedulerApp.Impl;
    using bgTeam.Fksud.Soap.Dto;
    using bgTeam.Fksud.Soap.Services;
    using bgTeam.Fksud.Soap.Services.Impl;
    using bgTeam.Impl;
    using bgTeam.Impl.Serilog;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Quartz;
    using Quartz.Impl;
    using Quartz.Spi;
    using Serilog;

    class Program
    {
        class SettingsProxy
        {
            public string AppEnvironment { get; set; }

            public string AppConfigsPath { get; set; }

            public string AppConfigsAdditional { get; set; }
        }

        class ConnectionStringProxy : IConnectionSetting
        {
            public string ConnectionString
            {
                get;
                set;
            }

            public ConnectionStringProxy(IConfiguration configuration)
            {
                ConnectionString = configuration.GetConnectionString("OTCSDB");
            }
        }

        static Task Main(string[] args)
        {
            var isService = !(Debugger.IsAttached || args.Contains("--console"));

            var builder = BuildHost(args);

            return isService
                ? builder.RunWinServiceAsync()
                : builder.RunConsoleAsync();
        }

        private static IHostBuilder BuildHost(string[] args)
            => new HostBuilder()
            .ConfigureHostConfiguration(x =>
            {
                x.SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
                .AddCommandLine(args)
                .AddJsonFile("hostsettings.json", optional: true)
                .AddJsonFile("appsettings.json", optional: true)
                .AddEnvironmentVariables();
            })
            .ConfigureAppConfiguration((context, x) =>
            {
                x.SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
                .AddJsonFile("appsettings.json", optional: true);

                var hostSettings = context.Configuration.Get<SettingsProxy>();

                if (!string.IsNullOrEmpty(hostSettings.AppEnvironment))
                {
                    x.AddJsonFile($"appsettings.{hostSettings.AppEnvironment}.json", optional: true);

                    foreach (var item in hostSettings.AppConfigsAdditional?.Split(',') ?? Array.Empty<string>())
                    {
                        var path = Path.Combine(hostSettings.AppConfigsPath ?? string.Empty, $"{item}.{hostSettings.AppEnvironment}.json");
                        x.AddJsonFile(path, optional: true);
                    }
                }

                x.AddCommandLine(args).AddEnvironmentVariables();
            })
            .ConfigureServices((context, x) =>
            {
                var otsSection = context.Configuration.GetSection(nameof(OpenTextService));
                var kqoSection = context.Configuration.GetSection(nameof(KafkaQueueOptions));
                var sending = context.Configuration.GetSection(nameof(SendingCsvFile));

                x.AddSingleton<IAppLogger, AppLoggerSerilog>()
                    .AddSingleton<IAppLoggerConfig, AppLoggerSerilogConfig>()
                    .AddSingleton<IDataMapper, AutoMapperImpl>()
                    .AddSingleton<IAuthenticationService, AuthenticationService>()
                    .AddSingleton<IMemberService, MemberService>()
                    .AddSingleton<IOpenTextService, OpenTextService>()
                    .AddSingleton<IJobFactory, JobFactory>()
                    .AddSingleton<ISchedulerFactory, StdSchedulerFactory>()
                    .AddSingleton<ISqlDialect, SqlDialectDapper>()
                    .AddSingleton<IConnectionSetting, ConnectionStringProxy>()
                    .AddSingleton<IConnectionFactory, ConnectionFactorySapHana>()
                    .AddSingleton<IRepository, RepositoryDapper>()
                    .AddSingleton<ICrudService, CrudServiceDapper>()
                    .AddSingleton<IOTMemberService, OTMemberService>()
                    .AddSingleton<IUserSynchronizeSettingService, UserSynchronizeSettingService>()
                    .AddSingleton<IUserSynchronizeSettingLocaleTypeService, UserSynchronizeSettingLocaleTypeService>()
                    .AddTransient<SendingCsvFileJob>()
                    .AddTransient<RetryQueueJob>()
                    .AddTransient<UpdateUserSettingsJob>()
                    .AddTransient<UpdateUserSettingsLocaleTypeJob>()
                    .Configure<SoapGateways>(otsSection)
                    .Configure<OpenTextServiceOptions>(otsSection)
                    .Configure<OpenTextApiCredentials>(otsSection)
                    .Configure<SendingCsvFile>(sending)
                    .Configure<KafkaQueueOptions>(nameof(KafkaQueueOptions), kqoSection)
                    .AddSingleton<IQueueFactory, KafkaQueueFactory>()
                    .AddHostedService<QuartzService>();
            });
    }
}
